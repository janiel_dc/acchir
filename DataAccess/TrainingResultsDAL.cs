﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;


namespace DataAccess
{
    public class TrainingResultsDAL
    {

        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertNewTrainingResult(TrainingResultBL trainingResultBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingResultBL._trainingId),
                new SqlParameter("@MemberId", trainingResultBL._memberId),
                new SqlParameter("@LastModifiedUserId", trainingResultBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_TrainingResults", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getTrainingDetailsByTrainingId(TrainingResultBL trainingResultBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingResultBL._trainingId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingMembersByTrainingId", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getAttendance(int memberId, int trainingId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberId),
                new SqlParameter("@TrainingId", trainingId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_Attendance", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getExistingTrainingDetails(TrainingResultBL trainingResultBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingResultBL._trainingId),
                new SqlParameter("@MemberId", trainingResultBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingDetailsByTrainingIdAndMemberID", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getUnaccreditedList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UnaccreditedSalesMemberList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getUnhiredList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UnhiredSalesMemberList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getOrganicPersonnelList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_OrganicPersonnelList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPositionList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPositionList");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateTrainingStatus(TrainingResultBL trainingResultBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingResultBL._trainingId),
                new SqlParameter("@MemberId", trainingResultBL._memberId),
                new SqlParameter("@isAttendedDay1", trainingResultBL._isAttendedDay1),
                new SqlParameter("@isAttendedDay2", trainingResultBL._isAttendedDay2),
                new SqlParameter("@ExamScore", trainingResultBL._examScore),
                new SqlParameter("@LastModifiedUserId", trainingResultBL._lastModifiedUserId),
                new SqlParameter("@isPassed", trainingResultBL._isPassed)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_TrainingResult", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateAttendance(TrainingResultBL trainingResultBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingResultBL._trainingId),
                new SqlParameter("@MemberId", trainingResultBL._memberId),
                new SqlParameter("@isAttendedDay1", trainingResultBL._isAttendedDay1),
                new SqlParameter("@isAttendedDay2", trainingResultBL._isAttendedDay2),
                new SqlParameter("@isPassed", trainingResultBL._isPassed)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Attendance", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteMember(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId),
                new SqlParameter("@LastModifiedUserId", salesPersonnelBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Member", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion

    }
}
