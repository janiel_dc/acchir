﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class TrainingDAL
    {
        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertTraining(TrainingBL trainingBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingNameOrRemarks", trainingBL._trainingNameOrRemarks),
                new SqlParameter("@TrainingStartDate", trainingBL._trainingStartDate),
                new SqlParameter("@OfficerInCharge", trainingBL._officerInCharge),
                new SqlParameter("@Facilitator", trainingBL._facilitator),
                new SqlParameter("@Venue", trainingBL._venue),
                new SqlParameter("@CreatedUserId", trainingBL._createdUserId),
                new SqlParameter("@LastModifiedUserId", trainingBL._lastModifiedUserId),
                new SqlParameter("@isActive", trainingBL._isActive),
                new SqlParameter("@trainingExamTotalNumber", trainingBL._trainingExamTotalNumber)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_NewTraining", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getActiveTrainingList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_ActiveTrainingList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getTrainingDetailsById(TrainingBL trainingBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingBL._trainingId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingDetailsById", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getTrainingList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPositionList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPositionList");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateTrainingDetails(TrainingBL trainingBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingBL._trainingId),
                new SqlParameter("@TrainingNameOrRemarks", trainingBL._trainingNameOrRemarks),
                new SqlParameter("@TrainingStartDate", trainingBL._trainingStartDate),
                new SqlParameter("@OfficerInCharge", trainingBL._officerInCharge),
                new SqlParameter("@Facilitator", trainingBL._facilitator),
                new SqlParameter("@Venue", trainingBL._venue),
                new SqlParameter("@LastModifiedUserId", trainingBL._lastModifiedUserId),
                new SqlParameter("@isActive", trainingBL._isActive),
                new SqlParameter("@TrainingExamTotalNumber", trainingBL._trainingExamTotalNumber)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Training", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteTraining(TrainingBL trainingBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingBL._trainingId),
                new SqlParameter("@LastModifiedUserId", trainingBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Training", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion
    }
}
