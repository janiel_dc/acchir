﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class UserDAL
    {
        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertNewUser(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@FirstName", userBL._firstName),
                new SqlParameter("@MiddleName", userBL._middleName),
                new SqlParameter("@LastName", userBL._lastName),
                new SqlParameter("@Gender", userBL._gender),
                new SqlParameter("@BirthDate", userBL._birthDate),
                new SqlParameter("@Address1", userBL._address1),
                new SqlParameter("@Address2", userBL._address2),
                new SqlParameter("@ContactNo1", userBL._contactNo1),
                new SqlParameter("@ContactNo2", userBL._contactNo2),
                new SqlParameter("@EmailAddress", userBL._emailAddress),
                new SqlParameter("@UserName", userBL._userName),
                new SqlParameter("@Password", userBL._password),
                new SqlParameter("@ChallengeQuestion", userBL._challengeQuestion),
                new SqlParameter("@ChallengeAnswer", userBL._challengeAnswer),
                new SqlParameter("@IsActive", userBL._isActive),
                new SqlParameter("@LastModifiedUserId", userBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_NewUser", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getUserDetailsByUserName(UserBL usersBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserName", usersBL._userName)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UserListByUserName", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getAdminLists()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_AdminLists");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getUserDetailsById(UserBL usersBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserId", usersBL._userId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UserDetailsById", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet userAuthentication(UserBL usersBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserName", usersBL._userName),
                new SqlParameter("@Password", usersBL._password)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UserAuthentication", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSecretQuestion(string username)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@Username", username)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_ChallengeQuestion", param);

            }
            catch
            {
                throw;
            }
        } 

        #endregion

        #region "UPDATE"

        public static void updateUserGeneralInfo(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserId", userBL._userId),
                new SqlParameter("@FirstName", userBL._firstName),
                new SqlParameter("@MiddleName", userBL._middleName),
                new SqlParameter("@LastName", userBL._lastName),
                new SqlParameter("@BirthDate", userBL._birthDate),
                new SqlParameter("@Address1", userBL._address1),
                new SqlParameter("@Address2", userBL._address2),
                new SqlParameter("@ContactNo1", userBL._contactNo1),
                new SqlParameter("@ContactNo2", userBL._contactNo2),
                new SqlParameter("@EmailAddress", userBL._emailAddress),
                new SqlParameter("@Password", userBL._password),
                new SqlParameter("@ChallengeQuestion", userBL._challengeQuestion),
                new SqlParameter("@ChallengeAnswer", userBL._challengeAnswer),
                new SqlParameter("@IsActive", userBL._isActive),
                new SqlParameter("@LastModifiedUserId", userBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_UserGeneralInfo", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateUserAccountInfo(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@Password", userBL._password),
                new SqlParameter("@ChallengeQuestion", userBL._challengeQuestion),
                new SqlParameter("@ChallengeAnswer", userBL._challengeAnswer),
                new SqlParameter("@UserId", userBL._userId),
                new SqlParameter("@LastModifiedUserId", userBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_UserAccountInfo", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateUserPersonalInfo(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@FirstName", userBL._firstName),
                new SqlParameter("@MiddleName", userBL._middleName),
                new SqlParameter("@LastName", userBL._lastName),
                new SqlParameter("@Address1", userBL._address1),
                new SqlParameter("@Address2", userBL._address2),
                new SqlParameter("@ContactNo1", userBL._contactNo1),
                new SqlParameter("@ContactNo2", userBL._contactNo2),
                new SqlParameter("@EmailAddress", userBL._emailAddress),
                new SqlParameter("@UserId", userBL._userId),
                new SqlParameter("@LastModifiedUserId", userBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_UserPersonalInfo", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updatePassword(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserId", userBL._userId),
                new SqlParameter("@Password", userBL._password)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Password", param);

            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteUser(UserBL userBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@UserId", userBL._userId),
                new SqlParameter("@LastModifiedUserId", userBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_User", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion
    }
}
