﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class RevalidaResultsDAL
    {
        #region "MEMBERS"

        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertRevalidaResult(RevalidaResultsBL revalidaResultsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaId", revalidaResultsBL._revalidaId),
                new SqlParameter("@MemberId", revalidaResultsBL._memberId),
                new SqlParameter("@LastModifiedUserId", revalidaResultsBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_RevalidaResult", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getRevalidaDetailsByRevalidaDate(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaDate", revalidaBL._revalidaDate)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaResultFinishedListByDate", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getRevalidaDetailsByRevalidaIdAndMemberId(RevalidaResultsBL revalidaResultsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaId", revalidaResultsBL._revalidaId),
                new SqlParameter("@MemberId", revalidaResultsBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaDetailsByRevalidaIdAndMemberId",param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getRevalidaInProgressList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaInProgressList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getTrainingPassersList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingPassers");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getRevalidaByDate(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaDate", revalidaBL._revalidaDate)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaResultsByDate",param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateRevalidaResultDetails(RevalidaResultsBL revalidaResultsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaResultId", revalidaResultsBL._revalidaResultId),
                new SqlParameter("@isSubmittedRevalidaForm", revalidaResultsBL._isSubmittedRevalidaForm),
                new SqlParameter("@isPassedInRevalida", revalidaResultsBL._isPassedInRevalida),
                new SqlParameter("@LastModifiedUserId", revalidaResultsBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_RevalidaResult", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteTraining(TrainingBL trainingBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@TrainingId", trainingBL._trainingId),
                new SqlParameter("@LastModifiedUserId", trainingBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Training", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion
    }
}
