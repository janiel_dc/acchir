﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class SalesPersonnelDAL
    {
        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertNewMember(SalesPersonnelBL memberBL, SalesDocumentsBL salesDocumentBL, SalesPersonnelHierarchyBL salesPersonnelHierarchyBL)

        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SalesPositionTypeId", memberBL._salesPositionTypeId),
                new SqlParameter("@MemberTypeId", memberBL._memberTypeId),
                new SqlParameter("@FirstName", memberBL._firstName),
                new SqlParameter("@MiddleName", memberBL._middleName),
                new SqlParameter("@LastName", memberBL._lastName),
                new SqlParameter("@NickName", memberBL._nickName),
                new SqlParameter("@Gender", memberBL._gender),
                new SqlParameter("@CivilStatus", memberBL._civilStatus),
                new SqlParameter("@Religion", memberBL._religion),
                new SqlParameter("@Citizenship", memberBL._citizenship),
                new SqlParameter("@BirthDate", memberBL._birthDate),
                new SqlParameter("@BirthPlace", memberBL._birthPlace),
                new SqlParameter("@ContactNo1", memberBL._contactNo1),
                new SqlParameter("@ContactNo2", memberBL._contactNo2),
                new SqlParameter("@EmailAddress", memberBL._emailAddress),
                new SqlParameter("@AddressNo", memberBL._addressNo),
                new SqlParameter("@AddressStreet", memberBL._addressStreet),
                new SqlParameter("@AddressVillageOrSubdivision", memberBL._addressVillageOrSubdivision),
                new SqlParameter("@AddressBarangayOrDistrict", memberBL._addressBarangayOrDistrict),
                new SqlParameter("@AddressTownOrCity", memberBL._addressTownOrCity),
                new SqlParameter("@CreatedUserId", memberBL._createdUserId),
                new SqlParameter("@LastModifiedUserId", memberBL._lastModifiedUserId),
                new SqlParameter("@IsActive", memberBL._isActive),
                new SqlParameter("@2x2Photo", salesDocumentBL._2x2Photo),
                new SqlParameter("@GovernmentID", salesDocumentBL._governmentID),
                new SqlParameter("@TINNo", salesDocumentBL._tINNo),
                new SqlParameter("@ApplicationAndCOA", salesDocumentBL._applicationAndCOA),
                new SqlParameter("@SignatureOfEndorsers", salesDocumentBL._signatureOfEndorsers),
                new SqlParameter("@SupervisorMemberId", salesPersonnelHierarchyBL._supervisorMemberId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_NewMember", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getSalesForEndorsementToAccounting(int memberId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberId)};
                return SqlHelper.ExecuteDataset(cn, "[DAL_GET_EndorsementToAccounting]", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesForEndorsement(int memberId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberId)};
                return SqlHelper.ExecuteDataset(cn, "[DAL_GET_ForEndorsement]", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesCompleteRequirements(int memberId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_CompleteRequirements", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getHigherSalesPersonnelBySalesPositionId(int salesPositionId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SalesPositionId", salesPositionId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_HigherSalesPersonnelBySalesPositionId", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet searchOrganicDocuments(string searchString)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SearchString", searchString)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SearchUnaccreditedSalesMemberList", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet searchSalesDocuments(string searchString)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SearchString", searchString)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SearchUnaccreditedSalesMemberList", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet searchSalesAttendance(string searchString)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SearchString", searchString)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SearchUnaccreditedSalesMemberListForAttendance", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesAttendance()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_UnaccreditedSalesMemberListForAttendance");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesTrainingPassers(int memberId)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingPassersAndRequirements", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet salesDocumentsByMemberId(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesDocumentsByMemberId", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPersonnelListById(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPersonnelById", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPersonnelList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPersonnelList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPositionList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPositionList");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateAccredit(SalesDocumentsBL salesDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesDocumentsBL._memberId),
                new SqlParameter("@EndorsedBy", salesDocumentsBL._endorsedBy),
                new SqlParameter("@ReceivedBy", salesDocumentsBL._receivedBy)};
                SqlHelper.ExecuteNonQuery(cn, "[DAL_UPDATE_AccreditSales]", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateMSA(SalesDocumentsBL salesDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesDocumentsBL._memberId),
                new SqlParameter("@ForMSA", salesDocumentsBL._forMSA),
                new SqlParameter("@SigningMSA", salesDocumentsBL._signingMSA),
                new SqlParameter("@ATMAppplication", salesDocumentsBL._ATMAppplication),
                new SqlParameter("@BankProcessingFee", salesDocumentsBL._bankProcessingFee),
                new SqlParameter("@1x1IdPicForATM", salesDocumentsBL._1x1IdPicForATM),
                new SqlParameter("@CopyOfValidId", salesDocumentsBL._copyOfValidId),
                new SqlParameter("@IDApplication", salesDocumentsBL._IDApplication),
                new SqlParameter("@1x1IdPicForId", salesDocumentsBL._1x1IdPicForId),
                new SqlParameter("@EnrollmentToBiometrics", salesDocumentsBL._enrollmentToBiometrics)};
                SqlHelper.ExecuteNonQuery(cn, "[DAL_UPDATE_MSA]", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateEndorsement(SalesDocumentsBL salesDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesDocumentsBL._memberId),
                new SqlParameter("@MSAVP", salesDocumentsBL._MSAbyVip),
                new SqlParameter("@Endorsement", salesDocumentsBL._forBroker)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Endorsement", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateRequirements(SalesDocumentsBL salesDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesDocumentsBL._memberId),
                new SqlParameter("@NBI", salesDocumentsBL._NBI),
                new SqlParameter("@Diploma", salesDocumentsBL._diploma)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Requirements", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateSalesDocuments(SalesDocumentsBL salesDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@2x2Pic", salesDocumentsBL._2x2Photo),
                new SqlParameter("@GovernmentID", salesDocumentsBL._governmentID),
                new SqlParameter("@TINNo", salesDocumentsBL._tINNo),
                new SqlParameter("@ApplicationAndCOA", salesDocumentsBL._applicationAndCOA),
                new SqlParameter("@SignatureOfEndorsers", salesDocumentsBL._signatureOfEndorsers),
                new SqlParameter("@BasicOrientationSeminar", salesDocumentsBL._basicOrientationSeminar),
                new SqlParameter("@DateOfOrientation", salesDocumentsBL._dateOfOrientation),
                new SqlParameter("@TrainingOfficer", salesDocumentsBL._trainingOfficer),
                new SqlParameter("@LastModifiedUserId", salesDocumentsBL._lastModifiedUserId),
                new SqlParameter("@SalesDocumentsId", salesDocumentsBL._salesDocumentsId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_SalesDocuments", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateMemberDetails(SalesPersonnelBL salesPersonnelBL, SalesPersonnelHierarchyBL salesPersonnelHierarchyBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId),
                new SqlParameter("@SalesPositionTypeId", salesPersonnelBL._salesPositionTypeId),
                new SqlParameter("@FirstName", salesPersonnelBL._firstName),
                new SqlParameter("@MiddleName", salesPersonnelBL._middleName),
                new SqlParameter("@LastName", salesPersonnelBL._lastName),
                new SqlParameter("@NickName", salesPersonnelBL._nickName),
                new SqlParameter("@Gender", salesPersonnelBL._gender),
                new SqlParameter("@CivilStatus", salesPersonnelBL._civilStatus),
                new SqlParameter("@Religion", salesPersonnelBL._religion),
                new SqlParameter("@Citizenship", salesPersonnelBL._citizenship),
                new SqlParameter("@BirthDate", salesPersonnelBL._birthDate),
                new SqlParameter("@BirthPlace", salesPersonnelBL._birthPlace),
                new SqlParameter("@ContactNo1", salesPersonnelBL._contactNo1),
                new SqlParameter("@ContactNo2", salesPersonnelBL._contactNo2),
                new SqlParameter("@EmailAddress", salesPersonnelBL._emailAddress),
                new SqlParameter("@AddressNo", salesPersonnelBL._addressNo),
                new SqlParameter("@AddressStreet", salesPersonnelBL._addressStreet),
                new SqlParameter("@AddressVillageOrSubdivision", salesPersonnelBL._addressVillageOrSubdivision),
                new SqlParameter("@AddressBarangayOrDistrict", salesPersonnelBL._addressBarangayOrDistrict),
                new SqlParameter("@AddressTownOrCity", salesPersonnelBL._addressTownOrCity),
                new SqlParameter("@LastModifiedUserId", salesPersonnelBL._lastModifiedUserId),
                new SqlParameter("@IsActive", salesPersonnelBL._isActive),
                new SqlParameter("@SupervisorMemberId", salesPersonnelHierarchyBL._supervisorMemberId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Member", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteMember(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId),
                new SqlParameter("@LastModifiedUserId", salesPersonnelBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Member", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion
    }
}
