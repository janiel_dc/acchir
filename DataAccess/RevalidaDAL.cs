﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class RevalidaDAL
    {
        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertRevalida(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaDate", revalidaBL._revalidaDate),
                new SqlParameter("@RevalidaPanelist", revalidaBL._revalidaPanelist),
                new SqlParameter("@RevalidaOfficerInCharge", revalidaBL._revalidaOfficerInCharge),
                new SqlParameter("@RevalidaVenue", revalidaBL._revalidaVenue),
                new SqlParameter("@CreatedUserId", revalidaBL._createdUserId),
                new SqlParameter("@LastModifiedUserId", revalidaBL._lastModifiedUserId),
                new SqlParameter("@isActive", revalidaBL._isActive)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_NewRevalida", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getRevalidaList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getRevalidaDetailsById(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaId", revalidaBL._revalidaId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_RevalidaListById", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getTrainingList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_TrainingList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPositionList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPositionList");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateRevalidaDetails(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaId", revalidaBL._revalidaId),
                new SqlParameter("@RevalidaDate", revalidaBL._revalidaDate),
                new SqlParameter("@RevalidaPanelist", revalidaBL._revalidaPanelist),
                new SqlParameter("@RevalidaOfficerInCharge", revalidaBL._revalidaOfficerInCharge),
                new SqlParameter("@RevalidaVenue", revalidaBL._revalidaVenue),
                new SqlParameter("@LastModifiedUserId", revalidaBL._lastModifiedUserId),
                new SqlParameter("@isActive", revalidaBL._isActive)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_Revalida", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteRevalida(RevalidaBL revalidaBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@RevalidaId", revalidaBL._revalidaId),
                new SqlParameter("@LastModifiedUserId", revalidaBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Revalida", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion
    }
}
