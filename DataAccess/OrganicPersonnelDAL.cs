﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using BusinessObjects;

namespace DataAccess
{
    public class OrganicPersonnelDAL
    {

        #region "MEMBERS"
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        #endregion

        #region "METHODS"

        #region "CREATE"

        public static void insertNewMember(SalesPersonnelBL memberBL, OrganicDocumentsBL organicDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberTypeId", memberBL._memberTypeId),
                new SqlParameter("@FirstName", memberBL._firstName),
                new SqlParameter("@MiddleName", memberBL._middleName),
                new SqlParameter("@LastName", memberBL._lastName),
                new SqlParameter("@Gender", memberBL._gender),
                new SqlParameter("@CivilStatus", memberBL._civilStatus),
                new SqlParameter("@Religion", memberBL._religion),
                new SqlParameter("@Citizenship", memberBL._citizenship),
                new SqlParameter("@BirthDate", memberBL._birthDate),
                new SqlParameter("@BirthPlace", memberBL._birthPlace),
                new SqlParameter("@ContactNo1", memberBL._contactNo1),
                new SqlParameter("@ContactNo2", memberBL._contactNo2),
                new SqlParameter("@EmailAddress", memberBL._emailAddress),
                new SqlParameter("@AddressNo", memberBL._addressNo),
                new SqlParameter("@AddressStreet", memberBL._addressStreet),
                new SqlParameter("@AddressVillageOrSubdivision", memberBL._addressVillageOrSubdivision),
                new SqlParameter("@AddressBarangayOrDistrict", memberBL._addressBarangayOrDistrict),
                new SqlParameter("@AddressTownOrCity", memberBL._addressTownOrCity),
                new SqlParameter("@AddressPostalCode", memberBL._addressPostalCode),
                new SqlParameter("@CreatedUserId", memberBL._createdUserId),
                new SqlParameter("@LastModifiedUserId", memberBL._lastModifiedUserId),
                new SqlParameter("@IsActive", memberBL._isActive),
                //Parameters for organic documents
                new SqlParameter("@2x2Photo", organicDocumentsBL._2x2Pic),
                new SqlParameter("@SSSNo", organicDocumentsBL._SSSNo),
                new SqlParameter("@PagibigNo", organicDocumentsBL._pagIbigNo),
                new SqlParameter("@TINNo", organicDocumentsBL._TINNo),
                new SqlParameter("@PhilHealthNo", organicDocumentsBL._philhealthNo),
                new SqlParameter("@PositionDesired", memberBL._positionDesired),
                new SqlParameter("@DateOfApplication", memberBL._dateOfApplication)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_INSERT_NewMemberOrganic", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "READ"

        public static DataSet getMemberTypeList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_MemberType");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet searchOrganicDocuments(string searchString)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@SearchString", searchString)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SearchUnhiredSalesMemberList", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet organicDocumentsByMemberId(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_OrganicDocumentsByMemberId", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getOrganicPersonnelListById(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@OrganicId", salesPersonnelBL._memberId)};
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_OrganicPersonnelById", param);
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getOrganicPersonnelList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_OrganicPersonnelList");
            }
            catch
            {
                throw;
            }
        }

        public static DataSet getSalesPositionList()
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                return SqlHelper.ExecuteDataset(cn, "DAL_GET_SalesPositionList");
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "UPDATE"

        public static void updateOrganicDocuments(OrganicDocumentsBL organicDocumentsBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@2x2Pic", organicDocumentsBL._2x2Pic),
                new SqlParameter("@SSSNo", organicDocumentsBL._SSSNo),
                new SqlParameter("@PagIbigNo", organicDocumentsBL._pagIbigNo),
                new SqlParameter("@TINNo", organicDocumentsBL._TINNo),
                new SqlParameter("@PhilHealthNo", organicDocumentsBL._philhealthNo),
                new SqlParameter("@DateHired", organicDocumentsBL._dateHired),
                new SqlParameter("@LastModifiedUserId", organicDocumentsBL._lastModifiedUserId),
                new SqlParameter("@OrganicDocumentsId", organicDocumentsBL._organicDocumentsId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_OrganicDocuments", param);
            }
            catch
            {
                throw;
            }
        }

        public static void updateNewMember(SalesPersonnelBL memberBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", memberBL._memberId),
                new SqlParameter("@MemberTypeId", memberBL._memberTypeId),
                new SqlParameter("@FirstName", memberBL._firstName),
                new SqlParameter("@MiddleName", memberBL._middleName),
                new SqlParameter("@LastName", memberBL._lastName),
                new SqlParameter("@Gender", memberBL._gender),
                new SqlParameter("@CivilStatus", memberBL._civilStatus),
                new SqlParameter("@Religion", memberBL._religion),
                new SqlParameter("@Citizenship", memberBL._citizenship),
                new SqlParameter("@BirthDate", memberBL._birthDate),
                new SqlParameter("@BirthPlace", memberBL._birthPlace),
                new SqlParameter("@ContactNo1", memberBL._contactNo1),
                new SqlParameter("@ContactNo2", memberBL._contactNo2),
                new SqlParameter("@EmailAddress", memberBL._emailAddress),
                new SqlParameter("@AddressNo", memberBL._addressNo),
                new SqlParameter("@AddressStreet", memberBL._addressStreet),
                new SqlParameter("@AddressVillageOrSubdivision", memberBL._addressVillageOrSubdivision),
                new SqlParameter("@AddressBarangayOrDistrict", memberBL._addressBarangayOrDistrict),
                new SqlParameter("@AddressTownOrCity", memberBL._addressTownOrCity),
                new SqlParameter("@AddressPostalCode", memberBL._addressPostalCode),
                new SqlParameter("@LastModifiedUserId", memberBL._lastModifiedUserId),
                new SqlParameter("@IsActive", memberBL._isActive),
                new SqlParameter("@PositionDesired", memberBL._positionDesired),
                new SqlParameter("@DateOfApplication", memberBL._dateOfApplication)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_UPDATE_OrganicPersonnel", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region "DELETE"

        public static void deleteMember(SalesPersonnelBL salesPersonnelBL)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlParameter[] param = {
                new SqlParameter("@MemberId", salesPersonnelBL._memberId),
                new SqlParameter("@LastModifiedUserId", salesPersonnelBL._lastModifiedUserId)};
                SqlHelper.ExecuteNonQuery(cn, "DAL_DELETE_Member", param);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion

    }
}
