﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class RevalidaBL
    {
        public int _revalidaId { get; set; }
        public string _revalidaDate { get; set; }
        public string _revalidaPanelist { get; set; }
        public string _revalidaOfficerInCharge { get; set; }
        public string _revalidaVenue { get; set; }
        public string _createdDateTime { get; set; }
        public int _createdUserId { get; set; }
        public string _lastModifiedDateTime { get; set; }
        public int _lastModifiedUserId { get; set; }
        public bool _isActive { get; set; }
        public bool _isDeleted { get; set; }
    }
}
