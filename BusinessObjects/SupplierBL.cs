﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class SupplierBL
    {
        public int _supplierKey = 0;
        public string _supplierName;
        public string _contactName;
        public int _phone;
        public string _email;
        public string _address;

        public int supplierKey
        {
            get { return _supplierKey; }
            set { _supplierKey = value; }
        }

        public int phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string supplierName
        {
            get { return _supplierName; }
            set { _supplierName = value; }
        }

        public string contactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }

        public string email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }
    }
}
