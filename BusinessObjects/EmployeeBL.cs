﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class EmployeeBL : RoleBL
    {
        private int mEmployeeKey;
        private string mFirstName;
        private string mLastName;
        private string mEmployeeUserName;
        private string mEmployeePassword;
        private int mRoleKey;
        private int mCompanyKey;
        private string mSecretQuestion;
        private string mSecretAnswer;
        private bool _isManager = false;
        private bool mStatus;
        private bool mIsDeleted;
        private DateTime mCreatedDateTime;
        private string _username = "";
        private string _storeName = "";
        private string _currencySymbol = "";

        public int EmployeeKey
        {
            get
            {
                return this.mEmployeeKey;
            }
            set
            {
                this.mEmployeeKey = value;
            }
        }
        public string FirstName
        {
            get
            {
                return this.mFirstName;
            }
            set
            {
                this.mFirstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return this.mLastName;
            }
            set
            {
                this.mLastName = value;
            }
        }
        public string EmployeeUserName
        {
            get
            {
                return this.mEmployeeUserName;
            }
            set
            {
                this.mEmployeeUserName = value;
            }
        }
        public string EmployeePassword
        {
            get
            {
                return this.mEmployeePassword;
            }
            set
            {
                this.mEmployeePassword = value;
            }
        }
        public int RoleKey
        {
            get
            {
                return this.mRoleKey;
            }
            set
            {
                this.mRoleKey = value;
            }
        }
        public int CompanyKey
        {
            get
            {
                return this.mCompanyKey;
            }
            set
            {
                this.mCompanyKey = value;
            }
        }
        public string SecretQuestion
        {
            get
            {
                return this.mSecretQuestion;
            }
            set
            {
                this.mSecretQuestion = value;
            }
        }
        public bool Status
        {
            get
            {
                return this.mStatus;
            }
            set
            {
                this.mStatus = value;
            }
        }
        public bool IsDeleted
        {
            get
            {
                return this.mIsDeleted;
            }
            set
            {
                this.mIsDeleted = value;
            }
        }
        private DateTime CreatedDateTime
        {
            get
            {
                return this.mCreatedDateTime;
            }
            set
            {
                this.mCreatedDateTime = value;
            }
        }
        public bool isManager
        {
            get { return _isManager; }
            set { _isManager = value; }
        }
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
        public string storeName
        {
            get { return _storeName; }
            set { _storeName = value; }
        }
        public string currencySymbol
        {
            get { return _currencySymbol; }
            set { _currencySymbol = value; }
        }
    }
}
