﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class ProductTypeBL
    {
        public int _productTypeKey = 0;
        public string _productTypeName;
        public string _description;

        public int productTypeKey
        {
            get { return _productTypeKey; }
            set { _productTypeKey = value; }
        }

        public string productTypeName
        {
            get { return _productTypeName; }
            set { _productTypeName = value; }
        }

        public string description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
