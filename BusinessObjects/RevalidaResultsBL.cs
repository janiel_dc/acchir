﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class RevalidaResultsBL
    {
        public int _revalidaResultId { get; set; }
        public int _revalidaId { get; set; }
        public int _memberId { get; set; }
        public bool _isSubmittedRevalidaForm { get; set; }
        public bool _isPassedInRevalida { get; set; }
        public string _lastModifiedDateTime { get; set; }
        public int _lastModifiedUserId { get; set; }
    }
}
