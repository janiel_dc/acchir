﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class SalesPersonnelHierarchyBL
    {

      public int _salesPersonnelHierarchyId {get; set;}
      public int _memberId {get; set;}
      public int _supervisorMemberId {get; set;}
      public string _lastModifiedDateTime {get; set;}
      public int _lastModifiedUserId { get; set; }

      public SalesPersonnelHierarchyBL()
      {
          this._supervisorMemberId = 0;
      }
    }
}
