﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class StoreBL
    {
        public int _storeKey = 0;
        public int _employeeKey = 0;
        public int _companyKey = 0;
        public int _currencyKey = 0;
        public int _storeTypeKey = 0;
        public string _storeName;
        public string _address;

        public int storeKey
        {
            get { return _storeKey; }
            set { _storeKey = value; }
        }

        public int employeeKey
        {
            get { return _employeeKey; }
            set { _employeeKey = value; }
        }

        public int companyKey
        {
            get { return _companyKey; }
            set { _companyKey = value; }
        }

        public int currencyKey
        {
            get { return _currencyKey; }
            set { _currencyKey = value; }
        }

        public int storeTypeKey
        {
            get { return _storeTypeKey; }
            set { storeTypeKey = value; }
        }

        public string storeName
        {
            get { return _storeName; }
            set { _storeName = value; }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }
    }
}
