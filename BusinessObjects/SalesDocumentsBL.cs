﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class SalesDocumentsBL
    {
      public int _salesDocumentsId {get; set;}
      public int _memberId {get; set;}
      public bool _2x2Photo {get; set;}
      public bool _governmentID {get; set;}
      public string _tINNo {get; set;}
      public bool _applicationAndCOA {get; set;}
      public bool _signatureOfEndorsers {get; set;}
      public bool _basicOrientationSeminar {get; set;}
      public string _dateOfOrientation {get; set;}
      public string _trainingOfficer {get; set;}
      public string _lastModifiedDateTime {get; set;}
      public int _lastModifiedUserId { get; set; }
      public bool _resume { get; set; }
      public string _verificationToPCFI { get; set; }
      public bool _siteOrientation { get; set; }
      public bool _revalida { get; set; }
      public bool _NBI { get; set; }
      public bool _diploma { get; set; }
      public bool _forMSA { get; set; }
      public bool _forBroker { get; set; }
      public bool _signingMSA { get; set; }
      public bool _ATMAppplication { get; set; }
      public bool _bankProcessingFee { get; set; }
      public bool _1x1IdPicForATM { get; set; }
      public bool _copyOfValidId { get; set; }
      public bool _IDApplication { get; set; }
      public bool _1x1IdPicForId { get; set; }
      public bool _enrollmentToBiometrics { get; set; }
      public bool _MSAbyVip { get; set; }
      public string _endorsedBy { get; set; }
      public string _receivedBy { get; set; }
    }
}
