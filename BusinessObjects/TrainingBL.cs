﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
   public class TrainingBL
    {
       public int _trainingId { get; set; }
       public string _trainingNameOrRemarks { get; set; }
       public string _trainingStartDate { get; set; }
       public string _trainingEndDate { get; set; }
       public string _officerInCharge { get; set; }
       public string _facilitator { get; set; }
       public string _venue { get; set; }
       public string _createdDateTime { get; set; }
       public int _createdUserId { get; set; }
       public string _lastModifiedDateTime { get; set; }
       public int _lastModifiedUserId { get; set; }
       public bool _isActive { get; set; }
       public bool _isDeleted { get; set; }
       public int _trainingExamTotalNumber { get; set; }
    }
}
