﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class TrainingResultBL
    {
          public  int _trainingResultId { get; set; }
          public int _trainingId { get; set; }
          public int _memberId { get; set; }
          public bool _isPassed { get; set; }
          public bool _isAttendedDay1 { get; set; }
          public bool _isAttendedDay2 { get; set; }
          public int _examScore { get; set; }
          public int _examItems { get; set; }
          public string _lastModifiedDateTime { get; set; }
          public int _lastModifiedUserId { get; set; }
    }
}
