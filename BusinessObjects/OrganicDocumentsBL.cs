﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class OrganicDocumentsBL
    {
        public int _memberId { get; set; }
        public bool  _2x2Pic { get; set; }
        public string _SSSNo { get; set; }
        public string _pagIbigNo { get; set; }
        public string _TINNo { get; set; }
        public string _philhealthNo { get; set; }
        public string _lastModifiedDateTime { get; set; }
        public int _lastModifiedUserId { get; set; }
        public string _dateHired { get; set; }
        public int _organicDocumentsId { get; set; }
    }
}
