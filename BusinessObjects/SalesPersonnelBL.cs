﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class SalesPersonnelBL
    {
      public int _memberId {get; set;}
      public int _salesPositionTypeId {get; set;}
      public int _memberTypeId {get; set;}
      public string _firstName {get; set;}
      public string _middleName {get; set;}
      public string _lastName {get; set;}
      public string _nickName {get; set;}
      public bool _gender {get; set;}
      public string _civilStatus {get; set;}
      public string _religion {get; set;}
      public string _citizenship {get; set;}
      public string _birthDate {get; set;}
      public string _birthPlace {get; set;}
      public string _contactNo1 {get; set;}
      public string _contactNo2 {get; set;}
      public string _emailAddress {get; set;}
      public string _addressNo {get; set;}
      public string _addressStreet {get; set;}
      public string _addressVillageOrSubdivision {get; set;}
      public string _addressBarangayOrDistrict {get; set;}
      public string _addressTownOrCity {get; set;}
      public string _createdDateTime {get; set;}
      public int _createdUserId {get; set;}
      public string _lastModifiedDateTime {get; set;}
      public int _lastModifiedUserId {get; set;}
      public bool _isActive {get; set;}
      public bool _isDeleted {get; set;}
      public int _version { get; set; } 
      public string _dateOfApplication { get; set; }
      public string  _positionDesired { get; set; }
      public string _addressPostalCode { get; set; }
      public bool _isHiredOrAccredited { get; set; }
    }
}
