﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class UserBL
    {
        public int _userId { get; set; }
        public string _firstName { get; set; }
        public string _middleName { get; set; }
        public string _lastName { get; set; }
        public bool _gender { get; set; }
        public string _birthDate { get; set; }
        public string _address1 { get; set; }
        public string _address2 { get; set; }
        public string _contactNo1 { get; set; }
        public string _contactNo2 { get; set; }
        public string _emailAddress { get; set; }
        public int _userTypeId { get; set; }
        public string _userName { get; set; }
        public string _password { get; set; }
        public string _challengeQuestion { get; set; }
        public string _challengeAnswer { get; set; }
        public bool _isActive { get; set; }
        public bool _isDeleted { get; set; }
        public string _createdDateTime { get; set; }
        public string _modifiedDateTime { get; set; }
        public int _lastModifiedUserId { get; set; }
        public int _version  { get; set; }
    }
}
