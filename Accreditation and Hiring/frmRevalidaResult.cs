﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmRevalidaResult : Form
    {
        private DateTime _revalidaDate = DateTime.Now, _revalidaFinishedDate = DateTime.Now;

        public frmRevalidaResult()
        {
            InitializeComponent();
        }

        private void frmRevalidaResult_Load(object sender, EventArgs e)
        {
            populateDropDowns();
            _revalidaDate = Convert .ToDateTime(cmbActiveRevalidaDate.SelectedValue);
            _revalidaFinishedDate = dtpRevalidaDate.Value;
            loadDataGrids();
        }

        private void loadDataGrids()
        {
            RevalidaBL revalidaBL = new RevalidaBL();
            RevalidaBL revalidaFinishedBL = new RevalidaBL();

            revalidaBL._revalidaDate = _revalidaDate.ToShortDateString();
            revalidaFinishedBL._revalidaDate = _revalidaFinishedDate.ToShortDateString();

            DataSet dsRevalidaDetails = RevalidaResultsDAL.getRevalidaByDate(revalidaBL);
            dgActive.AutoGenerateColumns = false;
            dgActive.DataSource = dsRevalidaDetails.Tables[0];

            DataSet dsFinishedRevalidaDetails = RevalidaResultsDAL.getRevalidaDetailsByRevalidaDate(revalidaFinishedBL);
            dgFinished.AutoGenerateColumns = false;
            dgFinished.DataSource = dsFinishedRevalidaDetails.Tables[0];
        }

        private void populateDropDowns()
        {
            //Set DataSet
            DataSet dsGetActiveRevalida = RevalidaResultsDAL.getRevalidaInProgressList();
            DataSet dsGetTrainingPassers = RevalidaResultsDAL.getTrainingPassersList();

            //Populate DropDownList
            cmbRevalidaDate.DataSource = dsGetActiveRevalida.Tables[0];
            cmbRevalidaDate.DisplayMember = "RevalidaDate";
            cmbRevalidaDate.ValueMember = "RevalidaId";

            cmbActiveRevalidaDate.DataSource = dsGetActiveRevalida.Tables[0];
            cmbActiveRevalidaDate.DisplayMember = "RevalidaDate";
            cmbActiveRevalidaDate.ValueMember = "RevalidaDate";

            cmbMemberName.DataSource = dsGetTrainingPassers.Tables[0];
            cmbMemberName.DisplayMember = "FullName";
            cmbMemberName.ValueMember = "MemberId";

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            frmLogin login = frmLogin.GetInstance();

            try
            {
                RevalidaResultsBL revalidaBL = new RevalidaResultsBL();
                revalidaBL._revalidaId = Convert.ToInt32(cmbRevalidaDate.SelectedValue);
                revalidaBL._memberId = Convert.ToInt32(cmbMemberName.SelectedValue);
                revalidaBL._lastModifiedUserId = login._loggedInUser._userId;

                DataSet dsExistingRevalidaResultDetails = RevalidaResultsDAL.getRevalidaDetailsByRevalidaIdAndMemberId(revalidaBL);
                if (dsExistingRevalidaResultDetails.Tables[0].Rows.Count == 0)
                {
                    RevalidaResultsDAL.insertRevalidaResult(revalidaBL);
                    MessageBox.Show("Participant added.");
                }
                else
                {
                    MessageBox.Show("Entry already saved.");
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error occured please contact your administrator");
                throw;
            }
        }

        private void cmbActiveRevalidaDate_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _revalidaDate = Convert.ToDateTime(cmbActiveRevalidaDate.SelectedValue);
            loadDataGrids();
        }

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgActive.Rows[e.RowIndex].Cells["Save_Active"] == dgActive.CurrentCell)
                {
                    try
                    {
                        RevalidaResultsBL revalidaResultsBL = new RevalidaResultsBL();
                        frmLogin login = frmLogin.GetInstance();

                        revalidaResultsBL._revalidaResultId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["RevalidaResultId_Active"].Value);
                        revalidaResultsBL._lastModifiedUserId = login._loggedInUser._userId;

                        if (Convert.ToBoolean(dgActive.Rows[e.RowIndex].Cells["RevalidaForm_Active"].Value) == true)
                            revalidaResultsBL._isSubmittedRevalidaForm = true;
                        else
                            revalidaResultsBL._isSubmittedRevalidaForm = false;

                        if (Convert.ToBoolean(dgActive.Rows[e.RowIndex].Cells["isPassed_Active"].Value) == true)
                            revalidaResultsBL._isPassedInRevalida = true;
                        else
                            revalidaResultsBL._isPassedInRevalida = false;

                        RevalidaResultsDAL.updateRevalidaResultDetails(revalidaResultsBL);
                        MessageBox.Show("Changes successfully saved.");
                        loadDataGrids();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error occured contact your administrator." + ex.ToString());
                        throw;
                    }
                }
            }
        }

        private void dtpRevalidaDate_ValueChanged(object sender, EventArgs e)
        {
            _revalidaFinishedDate = dtpRevalidaDate.Value;
            loadDataGrids();
        }
    }
}
