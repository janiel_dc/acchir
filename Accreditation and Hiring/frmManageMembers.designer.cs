﻿namespace Accreditation_and_Hiring_System
{
    partial class frmManageMembers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tcManageMembers = new System.Windows.Forms.TabControl();
            this.tpNew = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtContactNo1 = new System.Windows.Forms.TextBox();
            this.txtEmailAdd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtContactNo2 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtBarangay = new System.Windows.Forms.TextBox();
            this.txtSubdivision = new System.Windows.Forms.TextBox();
            this.txtStreetNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAddressNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbCivilStatus = new System.Windows.Forms.ComboBox();
            this.dtpBirthdate = new System.Windows.Forms.DateTimePicker();
            this.rbtnFemale = new System.Windows.Forms.RadioButton();
            this.rbtnMale = new System.Windows.Forms.RadioButton();
            this.txtBirthPlace = new System.Windows.Forms.TextBox();
            this.txtCitizenship = new System.Windows.Forms.TextBox();
            this.txtReligion = new System.Windows.Forms.TextBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMiddleName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbSupervisor = new System.Windows.Forms.ComboBox();
            this.lblSupervisor = new System.Windows.Forms.Label();
            this.cmbSalesPosition = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gbDocments = new System.Windows.Forms.GroupBox();
            this.cbSignatureOfEndorser = new System.Windows.Forms.CheckBox();
            this.txtTINNo = new System.Windows.Forms.TextBox();
            this.lblTINNo = new System.Windows.Forms.Label();
            this.cbTIN = new System.Windows.Forms.CheckBox();
            this.cbValidID = new System.Windows.Forms.CheckBox();
            this.cbPic = new System.Windows.Forms.CheckBox();
            this.cbCOA = new System.Windows.Forms.CheckBox();
            this.tpActive = new System.Windows.Forms.TabPage();
            this.dgActive = new System.Windows.Forms.DataGridView();
            this.MemberId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CivilStatus_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Citizenship_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthdate_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompleteAddress_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactNo1_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmailAddress_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Active = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpInActive = new System.Windows.Forms.TabPage();
            this.dgInactive = new System.Windows.Forms.DataGridView();
            this.MemberId_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CivilStatus_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Citizenship_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthdate_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contact1_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Inactive = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cbResume = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tcManageMembers.SuspendLayout();
            this.tpNew.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbDocments.SuspendLayout();
            this.tpActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).BeginInit();
            this.tpInActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(778, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Manage Members (Sales Personnel)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 398);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(778, 28);
            this.panel1.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(696, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(613, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tcManageMembers
            // 
            this.tcManageMembers.Controls.Add(this.tpNew);
            this.tcManageMembers.Controls.Add(this.tpActive);
            this.tcManageMembers.Controls.Add(this.tpInActive);
            this.tcManageMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcManageMembers.Location = new System.Drawing.Point(0, 33);
            this.tcManageMembers.Name = "tcManageMembers";
            this.tcManageMembers.SelectedIndex = 0;
            this.tcManageMembers.Size = new System.Drawing.Size(778, 365);
            this.tcManageMembers.TabIndex = 3;
            this.tcManageMembers.SelectedIndexChanged += new System.EventHandler(this.tcManageMembers_SelectedIndexChanged);
            // 
            // tpNew
            // 
            this.tpNew.Controls.Add(this.tabControl1);
            this.tpNew.Location = new System.Drawing.Point(4, 22);
            this.tpNew.Name = "tpNew";
            this.tpNew.Padding = new System.Windows.Forms.Padding(3);
            this.tpNew.Size = new System.Drawing.Size(770, 339);
            this.tpNew.TabIndex = 0;
            this.tpNew.Text = "New";
            this.tpNew.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(764, 333);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.cbActive);
            this.tabPage1.Location = new System.Drawing.Point(23, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(737, 325);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Member Info";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtContactNo1);
            this.groupBox5.Controls.Add(this.txtEmailAdd);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.txtContactNo2);
            this.groupBox5.Location = new System.Drawing.Point(367, 180);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(364, 108);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Contact Details";
            // 
            // txtContactNo1
            // 
            this.txtContactNo1.Location = new System.Drawing.Point(128, 16);
            this.txtContactNo1.MaxLength = 20;
            this.txtContactNo1.Name = "txtContactNo1";
            this.txtContactNo1.Size = new System.Drawing.Size(212, 20);
            this.txtContactNo1.TabIndex = 16;
            this.txtContactNo1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactNo1_KeyPress);
            // 
            // txtEmailAdd
            // 
            this.txtEmailAdd.Location = new System.Drawing.Point(128, 68);
            this.txtEmailAdd.MaxLength = 20;
            this.txtEmailAdd.Name = "txtEmailAdd";
            this.txtEmailAdd.Size = new System.Drawing.Size(212, 20);
            this.txtEmailAdd.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Contact # 1:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Contact # 2:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Email Add:";
            // 
            // txtContactNo2
            // 
            this.txtContactNo2.Location = new System.Drawing.Point(128, 42);
            this.txtContactNo2.MaxLength = 20;
            this.txtContactNo2.Name = "txtContactNo2";
            this.txtContactNo2.Size = new System.Drawing.Size(212, 20);
            this.txtContactNo2.TabIndex = 17;
            this.txtContactNo2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactNo2_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCity);
            this.groupBox4.Controls.Add(this.txtBarangay);
            this.groupBox4.Controls.Add(this.txtSubdivision);
            this.groupBox4.Controls.Add(this.txtStreetNo);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtAddressNo);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(367, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(364, 167);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Address";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(128, 122);
            this.txtCity.MaxLength = 20;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(212, 20);
            this.txtCity.TabIndex = 15;
            // 
            // txtBarangay
            // 
            this.txtBarangay.Location = new System.Drawing.Point(128, 96);
            this.txtBarangay.MaxLength = 20;
            this.txtBarangay.Name = "txtBarangay";
            this.txtBarangay.Size = new System.Drawing.Size(212, 20);
            this.txtBarangay.TabIndex = 14;
            // 
            // txtSubdivision
            // 
            this.txtSubdivision.Location = new System.Drawing.Point(128, 70);
            this.txtSubdivision.MaxLength = 20;
            this.txtSubdivision.Name = "txtSubdivision";
            this.txtSubdivision.Size = new System.Drawing.Size(212, 20);
            this.txtSubdivision.TabIndex = 13;
            // 
            // txtStreetNo
            // 
            this.txtStreetNo.Location = new System.Drawing.Point(128, 44);
            this.txtStreetNo.MaxLength = 20;
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.Size = new System.Drawing.Size(212, 20);
            this.txtStreetNo.TabIndex = 12;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(63, 125);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Town/City:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(54, 99);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Brgy/District:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Village/Subdivision:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Street:";
            // 
            // txtAddressNo
            // 
            this.txtAddressNo.Location = new System.Drawing.Point(128, 19);
            this.txtAddressNo.MaxLength = 20;
            this.txtAddressNo.Name = "txtAddressNo";
            this.txtAddressNo.Size = new System.Drawing.Size(212, 20);
            this.txtAddressNo.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "No:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbCivilStatus);
            this.groupBox1.Controls.Add(this.dtpBirthdate);
            this.groupBox1.Controls.Add(this.rbtnFemale);
            this.groupBox1.Controls.Add(this.rbtnMale);
            this.groupBox1.Controls.Add(this.txtBirthPlace);
            this.groupBox1.Controls.Add(this.txtCitizenship);
            this.groupBox1.Controls.Add(this.txtReligion);
            this.groupBox1.Controls.Add(this.txtNickName);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtMiddleName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(19, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 282);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information";
            // 
            // cmbCivilStatus
            // 
            this.cmbCivilStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCivilStatus.FormattingEnabled = true;
            this.cmbCivilStatus.Items.AddRange(new object[] {
            "Single",
            "Married",
            "Separated",
            "Widower"});
            this.cmbCivilStatus.Location = new System.Drawing.Point(108, 147);
            this.cmbCivilStatus.Name = "cmbCivilStatus";
            this.cmbCivilStatus.Size = new System.Drawing.Size(212, 21);
            this.cmbCivilStatus.TabIndex = 7;
            // 
            // dtpBirthdate
            // 
            this.dtpBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBirthdate.Location = new System.Drawing.Point(108, 226);
            this.dtpBirthdate.Name = "dtpBirthdate";
            this.dtpBirthdate.Size = new System.Drawing.Size(113, 20);
            this.dtpBirthdate.TabIndex = 9;
            // 
            // rbtnFemale
            // 
            this.rbtnFemale.AutoSize = true;
            this.rbtnFemale.Location = new System.Drawing.Point(162, 123);
            this.rbtnFemale.Name = "rbtnFemale";
            this.rbtnFemale.Size = new System.Drawing.Size(59, 17);
            this.rbtnFemale.TabIndex = 6;
            this.rbtnFemale.TabStop = true;
            this.rbtnFemale.Text = "Female";
            this.rbtnFemale.UseVisualStyleBackColor = true;
            // 
            // rbtnMale
            // 
            this.rbtnMale.AutoSize = true;
            this.rbtnMale.Checked = true;
            this.rbtnMale.Location = new System.Drawing.Point(108, 123);
            this.rbtnMale.Name = "rbtnMale";
            this.rbtnMale.Size = new System.Drawing.Size(48, 17);
            this.rbtnMale.TabIndex = 5;
            this.rbtnMale.TabStop = true;
            this.rbtnMale.Text = "Male";
            this.rbtnMale.UseVisualStyleBackColor = true;
            // 
            // txtBirthPlace
            // 
            this.txtBirthPlace.Location = new System.Drawing.Point(108, 252);
            this.txtBirthPlace.MaxLength = 100;
            this.txtBirthPlace.Name = "txtBirthPlace";
            this.txtBirthPlace.Size = new System.Drawing.Size(212, 20);
            this.txtBirthPlace.TabIndex = 10;
            // 
            // txtCitizenship
            // 
            this.txtCitizenship.Location = new System.Drawing.Point(108, 200);
            this.txtCitizenship.MaxLength = 50;
            this.txtCitizenship.Name = "txtCitizenship";
            this.txtCitizenship.Size = new System.Drawing.Size(212, 20);
            this.txtCitizenship.TabIndex = 8;
            // 
            // txtReligion
            // 
            this.txtReligion.Location = new System.Drawing.Point(108, 174);
            this.txtReligion.MaxLength = 50;
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(212, 20);
            this.txtReligion.TabIndex = 8;
            // 
            // txtNickName
            // 
            this.txtNickName.Location = new System.Drawing.Point(108, 97);
            this.txtNickName.MaxLength = 50;
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(212, 20);
            this.txtNickName.TabIndex = 4;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(108, 71);
            this.txtLastName.MaxLength = 50;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(212, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 252);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Birth Place:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 203);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Citizenship:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 150);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Civil Status:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 177);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Religion:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(39, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Gender:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Nick Name:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 226);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Birthdate:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Last Name:";
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(108, 45);
            this.txtMiddleName.MaxLength = 50;
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(212, 20);
            this.txtMiddleName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Middle Name:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(108, 19);
            this.txtFirstName.MaxLength = 50;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(212, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "First Name:";
            // 
            // cbActive
            // 
            this.cbActive.AutoSize = true;
            this.cbActive.Checked = true;
            this.cbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbActive.Location = new System.Drawing.Point(675, 294);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(56, 17);
            this.cbActive.TabIndex = 19;
            this.cbActive.Text = "Active";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.gbDocments);
            this.tabPage2.Location = new System.Drawing.Point(23, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(737, 325);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Documents";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbSupervisor);
            this.groupBox3.Controls.Add(this.lblSupervisor);
            this.groupBox3.Controls.Add(this.cmbSalesPosition);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(223, 195);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(282, 87);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Organization Role";
            // 
            // cmbSupervisor
            // 
            this.cmbSupervisor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSupervisor.FormattingEnabled = true;
            this.cmbSupervisor.Location = new System.Drawing.Point(85, 49);
            this.cmbSupervisor.Name = "cmbSupervisor";
            this.cmbSupervisor.Size = new System.Drawing.Size(180, 21);
            this.cmbSupervisor.TabIndex = 1;
            // 
            // lblSupervisor
            // 
            this.lblSupervisor.AutoSize = true;
            this.lblSupervisor.Enabled = false;
            this.lblSupervisor.Location = new System.Drawing.Point(19, 52);
            this.lblSupervisor.Name = "lblSupervisor";
            this.lblSupervisor.Size = new System.Drawing.Size(60, 13);
            this.lblSupervisor.TabIndex = 0;
            this.lblSupervisor.Text = "Supervisor:";
            // 
            // cmbSalesPosition
            // 
            this.cmbSalesPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesPosition.FormattingEnabled = true;
            this.cmbSalesPosition.Location = new System.Drawing.Point(85, 22);
            this.cmbSalesPosition.Name = "cmbSalesPosition";
            this.cmbSalesPosition.Size = new System.Drawing.Size(180, 21);
            this.cmbSalesPosition.TabIndex = 1;
            this.cmbSalesPosition.SelectedValueChanged += new System.EventHandler(this.cmbSalesPosition_SelectedValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Position:";
            // 
            // gbDocments
            // 
            this.gbDocments.Controls.Add(this.cbResume);
            this.gbDocments.Controls.Add(this.cbSignatureOfEndorser);
            this.gbDocments.Controls.Add(this.txtTINNo);
            this.gbDocments.Controls.Add(this.lblTINNo);
            this.gbDocments.Controls.Add(this.cbTIN);
            this.gbDocments.Controls.Add(this.cbValidID);
            this.gbDocments.Controls.Add(this.cbPic);
            this.gbDocments.Controls.Add(this.cbCOA);
            this.gbDocments.Location = new System.Drawing.Point(223, 6);
            this.gbDocments.Name = "gbDocments";
            this.gbDocments.Size = new System.Drawing.Size(282, 183);
            this.gbDocments.TabIndex = 7;
            this.gbDocments.TabStop = false;
            this.gbDocments.Text = "Documents";
            // 
            // cbSignatureOfEndorser
            // 
            this.cbSignatureOfEndorser.AutoSize = true;
            this.cbSignatureOfEndorser.Location = new System.Drawing.Point(22, 89);
            this.cbSignatureOfEndorser.Name = "cbSignatureOfEndorser";
            this.cbSignatureOfEndorser.Size = new System.Drawing.Size(128, 17);
            this.cbSignatureOfEndorser.TabIndex = 4;
            this.cbSignatureOfEndorser.Text = "Signature of Endorser";
            this.cbSignatureOfEndorser.UseVisualStyleBackColor = true;
            // 
            // txtTINNo
            // 
            this.txtTINNo.Enabled = false;
            this.txtTINNo.Location = new System.Drawing.Point(70, 157);
            this.txtTINNo.MaxLength = 16;
            this.txtTINNo.Name = "txtTINNo";
            this.txtTINNo.Size = new System.Drawing.Size(195, 20);
            this.txtTINNo.TabIndex = 3;
            this.txtTINNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTINNo_KeyPress);
            // 
            // lblTINNo
            // 
            this.lblTINNo.AutoSize = true;
            this.lblTINNo.Enabled = false;
            this.lblTINNo.Location = new System.Drawing.Point(21, 160);
            this.lblTINNo.Name = "lblTINNo";
            this.lblTINNo.Size = new System.Drawing.Size(45, 13);
            this.lblTINNo.TabIndex = 2;
            this.lblTINNo.Text = "TIN No:";
            // 
            // cbTIN
            // 
            this.cbTIN.AutoSize = true;
            this.cbTIN.Location = new System.Drawing.Point(22, 139);
            this.cbTIN.Name = "cbTIN";
            this.cbTIN.Size = new System.Drawing.Size(44, 17);
            this.cbTIN.TabIndex = 1;
            this.cbTIN.Text = "TIN";
            this.cbTIN.UseVisualStyleBackColor = true;
            this.cbTIN.CheckedChanged += new System.EventHandler(this.cbTIN_CheckedChanged);
            // 
            // cbValidID
            // 
            this.cbValidID.AutoSize = true;
            this.cbValidID.Location = new System.Drawing.Point(22, 44);
            this.cbValidID.Name = "cbValidID";
            this.cbValidID.Size = new System.Drawing.Size(129, 17);
            this.cbValidID.TabIndex = 0;
            this.cbValidID.Text = "Photocopy of Valid ID";
            this.cbValidID.UseVisualStyleBackColor = true;
            // 
            // cbPic
            // 
            this.cbPic.AutoSize = true;
            this.cbPic.Location = new System.Drawing.Point(22, 67);
            this.cbPic.Name = "cbPic";
            this.cbPic.Size = new System.Drawing.Size(61, 17);
            this.cbPic.TabIndex = 0;
            this.cbPic.Text = "2x2 Pic";
            this.cbPic.UseVisualStyleBackColor = true;
            // 
            // cbCOA
            // 
            this.cbCOA.AutoSize = true;
            this.cbCOA.Location = new System.Drawing.Point(22, 22);
            this.cbCOA.Name = "cbCOA";
            this.cbCOA.Size = new System.Drawing.Size(157, 17);
            this.cbCOA.TabIndex = 0;
            this.cbCOA.Text = "Sales Agen application form";
            this.cbCOA.UseVisualStyleBackColor = true;
            // 
            // tpActive
            // 
            this.tpActive.Controls.Add(this.dgActive);
            this.tpActive.Location = new System.Drawing.Point(4, 22);
            this.tpActive.Name = "tpActive";
            this.tpActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpActive.Size = new System.Drawing.Size(770, 339);
            this.tpActive.TabIndex = 1;
            this.tpActive.Text = "Active";
            this.tpActive.UseVisualStyleBackColor = true;
            // 
            // dgActive
            // 
            this.dgActive.AllowUserToAddRows = false;
            this.dgActive.AllowUserToDeleteRows = false;
            this.dgActive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgActive.BackgroundColor = System.Drawing.Color.White;
            this.dgActive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId_Active,
            this.FullName_Active,
            this.CivilStatus_Active,
            this.Citizenship_Active,
            this.Birthdate_Active,
            this.CompleteAddress_Active,
            this.ContactNo1_Active,
            this.EmailAddress_Active,
            this.Edit_Active});
            this.dgActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgActive.Location = new System.Drawing.Point(3, 3);
            this.dgActive.Name = "dgActive";
            this.dgActive.ReadOnly = true;
            this.dgActive.RowHeadersVisible = false;
            this.dgActive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgActive.Size = new System.Drawing.Size(764, 333);
            this.dgActive.TabIndex = 0;
            this.dgActive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgActive_CellContentClick);
            // 
            // MemberId_Active
            // 
            this.MemberId_Active.DataPropertyName = "MemberId";
            this.MemberId_Active.HeaderText = "MemberId";
            this.MemberId_Active.Name = "MemberId_Active";
            this.MemberId_Active.ReadOnly = true;
            this.MemberId_Active.Visible = false;
            // 
            // FullName_Active
            // 
            this.FullName_Active.DataPropertyName = "FullName";
            this.FullName_Active.HeaderText = "FullName";
            this.FullName_Active.Name = "FullName_Active";
            this.FullName_Active.ReadOnly = true;
            // 
            // CivilStatus_Active
            // 
            this.CivilStatus_Active.DataPropertyName = "CivilStatus";
            this.CivilStatus_Active.HeaderText = "Civil Status";
            this.CivilStatus_Active.Name = "CivilStatus_Active";
            this.CivilStatus_Active.ReadOnly = true;
            // 
            // Citizenship_Active
            // 
            this.Citizenship_Active.DataPropertyName = "Citizenship";
            this.Citizenship_Active.HeaderText = "Citizenship";
            this.Citizenship_Active.Name = "Citizenship_Active";
            this.Citizenship_Active.ReadOnly = true;
            // 
            // Birthdate_Active
            // 
            this.Birthdate_Active.DataPropertyName = "BirthDate";
            this.Birthdate_Active.HeaderText = "Birthdate";
            this.Birthdate_Active.Name = "Birthdate_Active";
            this.Birthdate_Active.ReadOnly = true;
            // 
            // CompleteAddress_Active
            // 
            this.CompleteAddress_Active.DataPropertyName = "CompleteAddress";
            this.CompleteAddress_Active.HeaderText = "Address";
            this.CompleteAddress_Active.Name = "CompleteAddress_Active";
            this.CompleteAddress_Active.ReadOnly = true;
            // 
            // ContactNo1_Active
            // 
            this.ContactNo1_Active.DataPropertyName = "ContactNo1";
            this.ContactNo1_Active.HeaderText = "Contact 1";
            this.ContactNo1_Active.Name = "ContactNo1_Active";
            this.ContactNo1_Active.ReadOnly = true;
            // 
            // EmailAddress_Active
            // 
            this.EmailAddress_Active.DataPropertyName = "EmailAddress";
            this.EmailAddress_Active.HeaderText = "Email";
            this.EmailAddress_Active.Name = "EmailAddress_Active";
            this.EmailAddress_Active.ReadOnly = true;
            // 
            // Edit_Active
            // 
            this.Edit_Active.HeaderText = "";
            this.Edit_Active.Name = "Edit_Active";
            this.Edit_Active.ReadOnly = true;
            this.Edit_Active.Text = "Edit";
            this.Edit_Active.UseColumnTextForButtonValue = true;
            // 
            // tpInActive
            // 
            this.tpInActive.Controls.Add(this.dgInactive);
            this.tpInActive.Location = new System.Drawing.Point(4, 22);
            this.tpInActive.Name = "tpInActive";
            this.tpInActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpInActive.Size = new System.Drawing.Size(770, 339);
            this.tpInActive.TabIndex = 2;
            this.tpInActive.Text = "Inactive";
            this.tpInActive.UseVisualStyleBackColor = true;
            // 
            // dgInactive
            // 
            this.dgInactive.AllowUserToAddRows = false;
            this.dgInactive.AllowUserToDeleteRows = false;
            this.dgInactive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInactive.BackgroundColor = System.Drawing.Color.White;
            this.dgInactive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInactive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId_Inactive,
            this.FullName_Inactive,
            this.CivilStatus_Inactive,
            this.Citizenship_Inactive,
            this.Birthdate_Inactive,
            this.Address_Inactive,
            this.Contact1_Inactive,
            this.Email_Inactive,
            this.Edit_Inactive});
            this.dgInactive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInactive.Location = new System.Drawing.Point(3, 3);
            this.dgInactive.Name = "dgInactive";
            this.dgInactive.ReadOnly = true;
            this.dgInactive.RowHeadersVisible = false;
            this.dgInactive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInactive.Size = new System.Drawing.Size(764, 333);
            this.dgInactive.TabIndex = 1;
            this.dgInactive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInactive_CellContentClick);
            // 
            // MemberId_Inactive
            // 
            this.MemberId_Inactive.DataPropertyName = "MemberId";
            this.MemberId_Inactive.HeaderText = "MemberId";
            this.MemberId_Inactive.Name = "MemberId_Inactive";
            this.MemberId_Inactive.ReadOnly = true;
            this.MemberId_Inactive.Visible = false;
            // 
            // FullName_Inactive
            // 
            this.FullName_Inactive.DataPropertyName = "FullName";
            this.FullName_Inactive.HeaderText = "FullName";
            this.FullName_Inactive.Name = "FullName_Inactive";
            this.FullName_Inactive.ReadOnly = true;
            // 
            // CivilStatus_Inactive
            // 
            this.CivilStatus_Inactive.DataPropertyName = "CivilStatus";
            this.CivilStatus_Inactive.HeaderText = "Civil Status";
            this.CivilStatus_Inactive.Name = "CivilStatus_Inactive";
            this.CivilStatus_Inactive.ReadOnly = true;
            // 
            // Citizenship_Inactive
            // 
            this.Citizenship_Inactive.DataPropertyName = "Citizenship";
            this.Citizenship_Inactive.HeaderText = "Citizenship";
            this.Citizenship_Inactive.Name = "Citizenship_Inactive";
            this.Citizenship_Inactive.ReadOnly = true;
            // 
            // Birthdate_Inactive
            // 
            this.Birthdate_Inactive.DataPropertyName = "BirthDate";
            this.Birthdate_Inactive.HeaderText = "Birthdate";
            this.Birthdate_Inactive.Name = "Birthdate_Inactive";
            this.Birthdate_Inactive.ReadOnly = true;
            // 
            // Address_Inactive
            // 
            this.Address_Inactive.DataPropertyName = "CompleteAddress";
            this.Address_Inactive.HeaderText = "Address";
            this.Address_Inactive.Name = "Address_Inactive";
            this.Address_Inactive.ReadOnly = true;
            // 
            // Contact1_Inactive
            // 
            this.Contact1_Inactive.DataPropertyName = "ContactNo1";
            this.Contact1_Inactive.HeaderText = "Contact 1";
            this.Contact1_Inactive.Name = "Contact1_Inactive";
            this.Contact1_Inactive.ReadOnly = true;
            // 
            // Email_Inactive
            // 
            this.Email_Inactive.DataPropertyName = "EmailAddress";
            this.Email_Inactive.HeaderText = "Email";
            this.Email_Inactive.Name = "Email_Inactive";
            this.Email_Inactive.ReadOnly = true;
            // 
            // Edit_Inactive
            // 
            this.Edit_Inactive.HeaderText = "";
            this.Edit_Inactive.Name = "Edit_Inactive";
            this.Edit_Inactive.ReadOnly = true;
            this.Edit_Inactive.Text = "Edit";
            this.Edit_Inactive.UseColumnTextForButtonValue = true;
            // 
            // cbResume
            // 
            this.cbResume.AutoSize = true;
            this.cbResume.Location = new System.Drawing.Point(22, 112);
            this.cbResume.Name = "cbResume";
            this.cbResume.Size = new System.Drawing.Size(65, 17);
            this.cbResume.TabIndex = 4;
            this.cbResume.Text = "Resume";
            this.cbResume.UseVisualStyleBackColor = true;
            // 
            // frmManageMembers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(778, 426);
            this.Controls.Add(this.tcManageMembers);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "frmManageMembers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmManageMembers_Load);
            this.panel1.ResumeLayout(false);
            this.tcManageMembers.ResumeLayout(false);
            this.tpNew.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbDocments.ResumeLayout(false);
            this.gbDocments.PerformLayout();
            this.tpActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).EndInit();
            this.tpInActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tcManageMembers;
        private System.Windows.Forms.TabPage tpActive;
        private System.Windows.Forms.TabPage tpInActive;
        private System.Windows.Forms.DataGridView dgActive;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabPage tpNew;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.DateTimePicker dtpBirthdate;
        private System.Windows.Forms.RadioButton rbtnFemale;
        private System.Windows.Forms.RadioButton rbtnMale;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMiddleName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbSupervisor;
        private System.Windows.Forms.Label lblSupervisor;
        private System.Windows.Forms.ComboBox cmbSalesPosition;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox gbDocments;
        private System.Windows.Forms.CheckBox cbValidID;
        private System.Windows.Forms.CheckBox cbPic;
        private System.Windows.Forms.CheckBox cbCOA;
        private System.Windows.Forms.TextBox txtNickName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbCivilStatus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBirthPlace;
        private System.Windows.Forms.TextBox txtReligion;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtEmailAdd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtContactNo2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContactNo1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtStreetNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAddressNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtBarangay;
        private System.Windows.Forms.TextBox txtSubdivision;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtTINNo;
        private System.Windows.Forms.Label lblTINNo;
        private System.Windows.Forms.CheckBox cbTIN;
        private System.Windows.Forms.CheckBox cbSignatureOfEndorser;
        private System.Windows.Forms.TextBox txtCitizenship;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgInactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn CivilStatus_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Citizenship_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthdate_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompleteAddress_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactNo1_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmailAddress_Active;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn CivilStatus_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Citizenship_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthdate_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contact1_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_Inactive;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Inactive;
        private System.Windows.Forms.CheckBox cbResume;
    }
}