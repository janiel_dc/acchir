﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;

namespace Inventory_Management
{
    public partial class frmConfigureCompany : Form
    {
        public frmConfigureCompany()
        {
            InitializeComponent();
        }

        private void frmConfigureCompany_Load(object sender, EventArgs e)
        {
            frmMain main = frmMain.GetInstance();
            int companyKey = main.loggedEmployee.CompanyKey;

            DataSet dscurrency = CurrencyDAL.getCurrency(companyKey);
            cbCurrencies.DataSource = dscurrency.Tables[0];
            cbCurrencies.DisplayMember = dscurrency.Tables[0].Columns["Currency"].ToString();
            cbCurrencies.ValueMember = dscurrency.Tables[0].Columns["CurrencyKey"].ToString();

            if (companyKey == 0)
            {
                cbCurrencies.Text = "United States of America(USA), Dollars($)";
            }
            else
            {
                cbCurrencies.Text = dscurrency.Tables[1].Rows[0]["Currency"].ToString();
            }
        }

        private void cbCurrencies_SelectedIndexChanged(object sender, EventArgs e)
        {
            string companyName = txtCompanyName.Text;
            int count = 0;

            if (companyName != "")
            {
                count = Convert.ToInt32(InstallationDAL.checkCompanyName());

                if (count > 0)
                {
                    gbCompanyAdmin.Enabled = false;
                    MessageBox.Show("Company name already exist.");
                }
                else
                {
                    gbCompanyAdmin.Enabled = true;
                }
            }
            else
            {
                gbCompanyAdmin.Enabled = false;
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if ((txtPassword.Text != "" && txtUserName.Text != "" && txtEmail.Text != "" && txtPhone.Text != "" 
                && txtAnswer.Text != "" && txtQuestion.Text != "" && txtFirstName.Text != "" && txtLastName.Text != "")
                ||
                (
                    !gbCompanyAdmin.Enabled &&
                    txtCompanyName.Text != "" &&
                    cbCurrencies.SelectedIndex > 0
                ))
            {
                if (txtPassword.Text == txtPassword2.Text)
                {
                    try
                    {
                        int currrency = Convert.ToInt32(cbCurrencies.SelectedValue);

                        DataSet initializeResult = InstallationDAL.initializeCompanyDatabase(
                            txtCompanyName.Text,
                            txtUserName.Text,
                            txtPassword.Text,
                            txtPhone.Text,
                            txtEmail.Text,
                            txtQuestion.Text,
                            txtAnswer.Text,
                            currrency);
                        switch (Convert.ToInt32(initializeResult.Tables[0].Rows[0]["Result"]))
                        {
                            case 2: MessageBox.Show("Please contact your company/system administrator if you wish to create a new administrator account.");
                                break;
                            case 3:    MessageBox.Show("Your administrator account has been successfully created.");
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An error occured.");
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Administrator passwords do not match.");
                }
            }
            else
            {
                MessageBox.Show("Please complete the form.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar))
            {

                if (!(Char.IsLetter(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar)))
                    e.Handled = true;
            }
        }

        private void txtLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar))
            {

                if (!(Char.IsLetter(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar)))
                    e.Handled = true;
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetterOrDigit(e.KeyChar))
            {

                if (Char.IsWhiteSpace(e.KeyChar))
                    e.Handled = true;
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {

                if (!(Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsSymbol(e.KeyChar)))
                    e.Handled = true;
            }
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetterOrDigit(e.KeyChar))
            {

                if (!(Char.IsControl(e.KeyChar)))
                    e.Handled = true;
            }
        }

        private void txtPassword2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetterOrDigit(e.KeyChar))
            {

                if (!(Char.IsControl(e.KeyChar)))
                    e.Handled = true;
            }
        }

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetterOrDigit(e.KeyChar))
            {

                if (!(Char.IsLetter(e.KeyChar) || Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar)))
                    e.Handled = true;
            }
        }

    }
}
