using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataAccess;

namespace Inventory_Management
{
    [RunInstaller(true)]

    public partial class InstallConfigForm : Form
    {
        //private string connectionString = "Data Source=DEVTRAINEE6-PC\SQLEXPRESS;Initial Catalog=BIMS;Persist Security Info=True;User ID=lester;Password=lester;";
        private string connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";

        public InstallConfigForm()
        {
            InitializeComponent();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            //if ((txtAdminPassword.Text != "" && txtAdminUsername.Text != "" && txtCompanyName.Text != "" &&
            //    txtDatabase.Text != "" && txtDatasource.Text != "" && txtEmail.Text != "" && txtPassword.Text != "" &&
            //    txtPhoneNumber.Text != "" && txtSecretAnswer.Text != "" && txtSecretQuestion.Text != "" && txtUsername.Text != "")
            //    ||
            //    (
            //        !grpAccount.Enabled &&
            //        txtCompanyName.Text != "" &&
            //        txtDatabase.Text != "" && txtDatasource.Text != ""
            //    )
            //    )
            //{
            //    if (txtAdminPassword.Text == txtRePassword.Text)
            //    {
            //        try
            //        {
            //            InstallationDAL.connectionString = connectionString;
            //            DataSet initializeResult = InstallationDAL.initializeCompanyDatabase(
            //                txtCompanyName.Text,
            //                txtAdminUsername.Text,
            //                txtAdminPassword.Text,
            //                txtPhoneNumber.Text,
            //                txtEmail.Text,
            //                txtSecretQuestion.Text,
            //                txtSecretAnswer.Text);
            //            switch (Convert.ToInt32(initializeResult.Tables[0].Rows[0]["Result"]))
            //            {
            //                case 2: MessageBox.Show("Please contact your company/system administrator if you wish to create a new administrator account.");
            //                    break;
            //                //case 3 :    MessageBox.Show("Your administrator account has been successfully created.");
            //                //    break;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show("An error occured.");
            //        }

            //        this.Close();
            //    }
            //    else
            //    {
            //        MessageBox.Show("Administrator passwords do not match.");
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Please complete the form.");
            //}
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDatasource.Text != "" && txtDatabase.Text != "" &&
                   txtPassword.Text != "" && txtUsername.Text != "") //txtDatabase.Text != "" &&
                {

                //if (cbDatabases.SelectedIndex > -1)
                //{
                //    cbDatabases.Enabled = true;
                //}
                    DataSet initializeResult = new DataSet();
                    connectionString = connectionString.Replace("Data Source=", "Data Source=" + txtDatasource.Text)
                                        .Replace("Initial Catalog=", "Initial Catalog=" + txtDatabase.Text)
                                        .Replace("User ID=", "User ID=" + txtUsername.Text)
                                        .Replace("Password=", "Password=" + txtPassword.Text);

                    Configuration c = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    ConnectionStringsSection section = (ConnectionStringsSection)c.GetSection("connectionStrings");
                    section.ConnectionStrings["ConnectionString"].ConnectionString = connectionString;
                    c.Save();

                    //cbDatabases.Enabled = true;

                    InstallationDAL.connectionString = connectionString;

                    //list Databases
                    DataSet dsDatabases = InstallationDAL.getDatabases();
                    cbDatabases.DataSource = dsDatabases.Tables[0];
                    cbDatabases.DisplayMember = "Name";
                    cbDatabases.ValueMember = "Name";
                    cbDatabases.Text = "";

                    if (cbDatabases.Items.Count != 0)
                    {
                        btnNext.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Could not connect to the database. Please check your login and password");
                    }
                    //btnDone.Enabled = true;
                    btnCheck.Enabled = false;
                    btnCancel.Enabled = true;
                    
                    }
                else
                {
                    MessageBox.Show("Please complete the form.");
                    cbDatabases.Enabled = false;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Could not connect to the database. Error: "+ex.Message.ToString());
                connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
                //grpAccount.Enabled = false;
                btnCheck.Enabled = true;
                btnCancel.Enabled = true;
                btnNext.Enabled = false;
        
            }
        }

        private void txtDatasource_TextChanged(object sender, EventArgs e)
        {
            //grpAccount.Enabled = false;
            connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
        }

        private void txtDatabase_TextChanged(object sender, EventArgs e)
        {
            //grpAccount.Enabled = false;
            btnCheck.Enabled = true;
            connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            //grpAccount.Enabled = false;
            btnCheck.Enabled = true;
            connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            //grpAccount.Enabled = false;
            btnCheck.Enabled = true;
            connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
        }

        private void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            //grpAccount.Enabled = false;
            btnCheck.Enabled = true;
            connectionString = "Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=;";
        }

        private void txtDatasource_Enter(object sender, EventArgs e)
        {
            txtDatasource.SelectAll();
        }

        private void txtUsername_Enter(object sender, EventArgs e)
        {
            txtUsername.SelectAll();
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            txtPassword.SelectAll();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            //throw new InstallException("Installation aborted.");
            //base.Rollback();
            Close();
        }

        private void InstallConfigForm_Load(object sender, EventArgs e)
        {
            cbDatabases.Enabled = false;
            btnNext.Enabled = false;
            btnCancel.Enabled = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            this.Close();

            frmConfigureCompany configurecompany = new frmConfigureCompany();
            configurecompany.ShowDialog();
        }

    }
}