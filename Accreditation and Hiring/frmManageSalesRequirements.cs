﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManageSalesRequirements : Form
    {
        private int _memberId = 0, _salesDocumentsId = 0;
        DataSet _dsGetActiveTrainings = null;

        public frmManageSalesRequirements()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmManageSalesRequirements_Load(object sender, EventArgs e)
        {
            loadDataGrid();
            if (dgMembersList.Rows.Count > 0)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[0].Cells[0].Value);
            }
            else
            {
                _memberId = 0;
            }
            viewMemberDocumentDetails();
        }

        #region Datagrids
        private void loadDataGrid()
        {
            DataSet dsMembersList = TrainingResultsDAL.getUnaccreditedList();
            dgMembersList.AutoGenerateColumns = false;
            dgMembersList.DataSource = dsMembersList.Tables[0];

            DataSet dsMembersListAttendance = SalesPersonnelDAL.getSalesAttendance();
            dgAttedance.AutoGenerateColumns = false;
            dgAttedance.DataSource = dsMembersListAttendance.Tables[0];

            DataSet dsMembersListTrainingPassers = SalesPersonnelDAL.getSalesTrainingPassers(_memberId);
            dgRequirements.AutoGenerateColumns = false;
            dgRequirements.DataSource = dsMembersListTrainingPassers.Tables[0];

            dgEndorsement.AutoGenerateColumns = false;
            dgEndorsement.DataSource = dsMembersList.Tables[0];

            dgMSA.AutoGenerateColumns = false;
            dgMSA.DataSource = dsMembersList.Tables[0];

            dgEndorseToAccounting.AutoGenerateColumns = false;
            dgEndorseToAccounting.DataSource = dsMembersList.Tables[0];
        }
        #endregion

        #region Combobox
        private void populateDropDown()
        {
            //Set DataSet
            _dsGetActiveTrainings = TrainingDAL.getActiveTrainingList();

            //Populate DropDownList
            cmbAttendanceSearch.DataSource = _dsGetActiveTrainings.Tables[1];
            cmbAttendanceSearch.DisplayMember = "TrainingNameOrRemarks";
            cmbAttendanceSearch.ValueMember = "TrainingId";
        }
        #endregion


        private void dgMembersList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[e.RowIndex].Cells[0].Value);
                viewMemberDocumentDetails();
            }
        }

        private void viewMemberDocumentDetails()
        {
            try
            {
                SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                salesPersonnelBL._memberId = _memberId;

                DataSet dsSalesDocuments = SalesPersonnelDAL.salesDocumentsByMemberId(salesPersonnelBL);
                _salesDocumentsId = Convert.ToInt32(dsSalesDocuments.Tables[0].Rows[0]["SalesDocumentsId"]);
                txtTINNo.Text = dsSalesDocuments.Tables[0].Rows[0]["TINNo"].ToString();
                txtTrainingOfficer.Text = dsSalesDocuments.Tables[0].Rows[0]["TrainingOfficer"].ToString();
                dtpDateOfOrientation.Value = Convert.ToDateTime(dsSalesDocuments.Tables[0].Rows[0]["DateOfOrientation"]);


                if (Convert.ToBoolean(dsSalesDocuments.Tables[0].Rows[0]["2x2Photo"]) == true)
                    cb2x2Pic.Checked = true;
                else
                    cb2x2Pic.Checked = false;

                if (Convert.ToBoolean(dsSalesDocuments.Tables[0].Rows[0]["GovernmentID"]) == true)
                    cbGovernmentId.Checked = true;
                else
                    cbGovernmentId.Checked = false;

                if (Convert.ToBoolean(dsSalesDocuments.Tables[0].Rows[0]["ApplicationAndCOA"]) == true)
                    cbCOA.Checked = true;
                else
                    cbCOA.Checked = false;

                if (Convert.ToBoolean(dsSalesDocuments.Tables[0].Rows[0]["SignatureOfEndorsers"]) == true &&
                    dsSalesDocuments.Tables[0].Rows[0]["SignatureOfEndorsers"] != null)
                    cbSignatureOfEndorser.Checked = true;
                else
                    cbSignatureOfEndorser.Checked = false;

                if (Convert.ToBoolean(dsSalesDocuments.Tables[0].Rows[0]["BasicOrientationSeminar"]) == true)
                    cbBasicOrientationSeminar.Checked = true;
                else
                    cbBasicOrientationSeminar.Checked = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator." + ex.ToString());
                throw;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_salesDocumentsId == 0)
            {
                MessageBox.Show("No selected sales document set.");
            }
            else
            {
                updateSalesRequirements();
            }
        }

        private void updateSalesRequirements()
        {
            try
            {
                SalesDocumentsBL salesDocumentsBL = new SalesDocumentsBL();
                frmLogin login = frmLogin.GetInstance();

                salesDocumentsBL._lastModifiedUserId = login._loggedInUser._userId;
                salesDocumentsBL._salesDocumentsId = _salesDocumentsId;
                salesDocumentsBL._tINNo = txtTINNo.Text.Trim();
                salesDocumentsBL._dateOfOrientation = dtpDateOfOrientation.Value.ToString();
                salesDocumentsBL._trainingOfficer = txtTrainingOfficer.Text.Trim();

                if (cb2x2Pic.Checked)
                    salesDocumentsBL._2x2Photo = true;
                else
                    salesDocumentsBL._2x2Photo = false;

                if (cbGovernmentId.Checked)
                    salesDocumentsBL._governmentID = true;
                else
                    salesDocumentsBL._governmentID = false;

                if (cbCOA.Checked)
                    salesDocumentsBL._applicationAndCOA = true;
                else
                    salesDocumentsBL._applicationAndCOA = false;

                if (cbSignatureOfEndorser.Checked)
                    salesDocumentsBL._signatureOfEndorsers = true;
                else
                    salesDocumentsBL._signatureOfEndorsers = false;

                if (cbBasicOrientationSeminar.Checked)
                    salesDocumentsBL._basicOrientationSeminar = true;
                else
                    salesDocumentsBL._basicOrientationSeminar = false;

                SalesPersonnelDAL.updateSalesDocuments(salesDocumentsBL);
                MessageBox.Show("Document successfully updated.");
                loadDataGrid();
            }
            catch (Exception)// ex)
            {
                MessageBox.Show("error occured contact your administrator.");
                throw;
            }

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string searchString = txtSearch.Text.Trim();

            DataSet dsMembersList = SalesPersonnelDAL.searchSalesDocuments(searchString);
            dgMembersList.AutoGenerateColumns = false;
            dgMembersList.DataSource = dsMembersList.Tables[0];

            if (dsMembersList.Tables[0].Rows.Count > 0)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[0].Cells[0].Value);
                viewMemberDocumentDetails();
                btnSave.Enabled = true;
            }
            else
            {
                _memberId = 0;
                btnSave.Enabled = false;
                txtTINNo.Text = "";
                dtpDateOfOrientation.Value = DateTime.Now;
                txtTrainingOfficer.Text = "";
                cb2x2Pic.Checked = false;
                cbBasicOrientationSeminar.Checked = false;
                cbCOA.Checked = false;
                cbGovernmentId.Checked = false;
                cbSignatureOfEndorser.Checked = false;
            }
        }

        #region HELPERS

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 32)
                e.Handled = true;
        }

        #endregion

        private void txtTINNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtSearchAttendance_TextChanged(object sender, EventArgs e)
        {
            string searchString = txtSearchAttendance.Text.Trim();

            DataSet dsMembersList = SalesPersonnelDAL.searchSalesAttendance(searchString);
            //dgAttedance.AutoGenerateColumns = false;
            dgAttedance.DataSource = dsMembersList.Tables[0];


        }

        private void dgAttedance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                //isPassed = isPassed Revalida
                //isAttendedDay1 = Basic Orientation Seminar
                //isAttendedDay2 = SiteOrientation
                int memberId = Convert.ToInt32(dgAttedance.Rows[e.RowIndex].Cells[0].Value);
                int trainingId = Convert.ToInt32(cmbAttendanceSearch.SelectedValue);

                DataSet dsGetAttendance = TrainingResultsDAL.getAttendance(memberId, trainingId);

                if (dsGetAttendance.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isPassed"]) == true)
                    {
                        cbRevalida.Checked = true;
                    }
                    else
                    {
                        cbRevalida.Checked = false;
                    }

                    if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isAttendedDay1"]) == true)
                    {
                        cbBasicOrientation.Checked = true;
                    }
                    else
                    {
                        cbBasicOrientation.Checked = false;
                    }

                    if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isAttendedDay2"]) == true)
                    {
                        cbSiteOrientation.Checked = true;
                    }
                    else
                    {
                        cbSiteOrientation.Checked = false;
                    }

                } 
            }


        }

        private void getTrainingResult()
        {
            populateDropDown();
            TrainingResultBL trainingResultBL = new TrainingResultBL();
            if (_dsGetActiveTrainings.Tables[0].Rows.Count > 0)
            {
                int memberId = 0;
                int trainingId = 0;

                trainingResultBL._trainingId = Convert.ToInt32(cmbAttendanceSearch.SelectedValue);

                DataSet dsFinishedTrainingDetails = TrainingResultsDAL.getTrainingDetailsByTrainingId(trainingResultBL);
                if (dsFinishedTrainingDetails.Tables[0].Rows.Count > 0)
                {

                    dgAttedance.AutoGenerateColumns = false;
                    dgAttedance.DataSource = dsFinishedTrainingDetails.Tables[0];


                    //isPassed = isPassed Revalida
                    //isAttendedDay1 = Basic Orientation Seminar
                    //isAttendedDay2 = SiteOrientation
                    memberId = Convert.ToInt32(dgAttedance.Rows[0].Cells[0].Value);
                    trainingId = Convert.ToInt32(cmbAttendanceSearch.SelectedValue);

                    DataSet dsGetAttendance = TrainingResultsDAL.getAttendance(memberId, trainingId);

                    if (dsGetAttendance.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isPassed"]) == true)
                        {
                            cbRevalida.Checked = true;
                        }
                        else
                        {
                            cbRevalida.Checked = false;
                        }

                        if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isAttendedDay1"]) == true)
                        {
                            cbBasicOrientation.Checked = true;
                        }
                        else
                        {
                            cbBasicOrientation.Checked = false;
                        }

                        if (Convert.ToBoolean(dsGetAttendance.Tables[0].Rows[0]["isAttendedDay2"]) == true)
                        {
                            cbSiteOrientation.Checked = true;
                        }
                        else
                        {
                            cbSiteOrientation.Checked = false;
                        }

                    } 
                }
            }
        }

        private void getRequirements()
        {
            DataSet dsMembersListTrainingPassers = SalesPersonnelDAL.getSalesTrainingPassers(0);
            if (dsMembersListTrainingPassers.Tables[0].Rows.Count > 0)
            {
                dgRequirements.AutoGenerateColumns = false;
                dgRequirements.DataSource = dsMembersListTrainingPassers.Tables[0];
            }
            else
            {
                dgRequirements.DataSource = null;
            }
        }

        private void getCompleteRequirements()
        {
            DataSet dsMembersListRequirements = SalesPersonnelDAL.getSalesCompleteRequirements(0);
            if (dsMembersListRequirements.Tables[0].Rows.Count > 0)
            {
                dgEndorsement.AutoGenerateColumns = false;
                dgEndorsement.DataSource = dsMembersListRequirements.Tables[0];
            }
            else
            {
                dgEndorsement.DataSource = null;
            }
        }

        private void getEndorsement()
        {
            DataSet dsMembersListMSA = SalesPersonnelDAL.getSalesForEndorsement(0);
            if (dsMembersListMSA.Tables[0].Rows.Count > 0)
            {
                dgMSA.AutoGenerateColumns = false;
                dgMSA.DataSource = dsMembersListMSA.Tables[0];
            }
            else
            {
                dgMSA.DataSource = null;
            }
        }

        private void getEndorsementToAccounting()
        {
            DataSet dsToAccounting = SalesPersonnelDAL.getSalesForEndorsementToAccounting(0);
            if (dsToAccounting.Tables[0].Rows.Count > 0)
            {
                dgEndorseToAccounting.AutoGenerateColumns = false;
                dgEndorseToAccounting.DataSource = dsToAccounting.Tables[0];
            }
            else
            {
                dgEndorseToAccounting.DataSource = null;
            }
        }

        private void tcSalesPersonnelRequirements_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateDropDown();

            if (tcSalesPersonnelRequirements.SelectedIndex == 1)
            {
                getTrainingResult();
            }
            else if (tcSalesPersonnelRequirements.SelectedIndex == 2)
            {
                getRequirements();
            }
            else if (tcSalesPersonnelRequirements.SelectedIndex == 3)
            {
                getCompleteRequirements();
            }
            else if (tcSalesPersonnelRequirements.SelectedIndex == 4)
            {
                getEndorsement();
            }
            else if (tcSalesPersonnelRequirements.SelectedIndex == 5)
            {
                getEndorsementToAccounting();
            }
            
        }

        private void cmbAttendanceSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {

            TrainingResultBL trainingResultBL = new TrainingResultBL();
            if (Convert.ToInt32(cmbAttendanceSearch.SelectedValue) > 0)//_dsGetActiveTrainings.Tables[0].Rows.Count > 0)
            {
                trainingResultBL._trainingId = Convert.ToInt32(cmbAttendanceSearch.SelectedValue);

                DataSet dsFinishedTrainingDetails = TrainingResultsDAL.getTrainingDetailsByTrainingId(trainingResultBL);
                dgAttedance.AutoGenerateColumns = false;
                dgAttedance.DataSource = dsFinishedTrainingDetails.Tables[0];
            }
            else
            {
                cbRevalida.Checked = false;
                cbBasicOrientation.Checked = false;
                cbSiteOrientation.Checked = false;
            }
        }

        private void btnSaveAttendance_Click(object sender, EventArgs e)
        {
            try
            {
                TrainingResultBL trainingResultBL = new TrainingResultBL();

                trainingResultBL._memberId = Convert.ToInt32(dgAttedance.SelectedRows[0].Cells[0].Value);
                trainingResultBL._trainingId = Convert.ToInt32(cmbAttendanceSearch.SelectedValue);

                if (cbRevalida.Checked)
                {
                    trainingResultBL._isPassed = true;
                }
                else
                {
                    trainingResultBL._isPassed = false;
                }

                if (cbBasicOrientation.Checked)
                {
                    trainingResultBL._isAttendedDay1 = true;
                }
                else
                {
                    trainingResultBL._isAttendedDay1 = false;
                }

                if (cbSiteOrientation.Checked)
                {
                    trainingResultBL._isAttendedDay2 = true;
                }
                else
                {
                    trainingResultBL._isAttendedDay2 = false;
                }

                TrainingResultsDAL.updateAttendance(trainingResultBL);
                MessageBox.Show("Attendance successfully updated");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void dgRequirements_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgRequirements.Rows[e.RowIndex].Cells[0].Value);

                DataSet dsGetRequirementsByMemberId = SalesPersonnelDAL.getSalesTrainingPassers(_memberId);
                if (dsGetRequirementsByMemberId.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dsGetRequirementsByMemberId.Tables[0].Rows[0]["Diploma"]) == false ||
                       Convert.IsDBNull(dsGetRequirementsByMemberId.Tables[0].Rows[0]["Diploma"]))
                    {
                        cbDiploma.Checked = false;
                    }
                    else
                    {
                        cbDiploma.Checked = true;
                    }

                    if (Convert.ToBoolean(dsGetRequirementsByMemberId.Tables[0].Rows[0]["NBI"]) == false ||
                       Convert.IsDBNull(dsGetRequirementsByMemberId.Tables[0].Rows[0]["NBI"]))
                    {
                        cbNBI.Checked = false;
                    }
                    else
                    {
                        cbNBI.Checked = true;
                    }

                } 
            }
        }

        private void btnRequirements_Click(object sender, EventArgs e)
        {
            try
            {
                SalesDocumentsBL salesDocumentsBL = new SalesDocumentsBL();

                salesDocumentsBL._memberId = Convert.ToInt32(dgRequirements.SelectedRows[0].Cells[0].Value);

                if (cbNBI.Checked)
                {
                    salesDocumentsBL._NBI = true;
                }
                else
                {
                    salesDocumentsBL._NBI = false;
                }

                if (cbDiploma.Checked)
                {
                    salesDocumentsBL._diploma = true;
                }
                else
                {
                    salesDocumentsBL._diploma = false;
                }

                SalesPersonnelDAL.updateRequirements(salesDocumentsBL);
                MessageBox.Show("Requirements successfully updated");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void dgEndorsement_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgRequirements.Rows[e.RowIndex].Cells[0].Value);

                DataSet dsGetRequirementByMemberId = SalesPersonnelDAL.getSalesCompleteRequirements(_memberId);
                if (dsGetRequirementByMemberId.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dsGetRequirementByMemberId.Tables[0].Rows[0]["MSAbyVip"]) == false ||
                       Convert.IsDBNull(dsGetRequirementByMemberId.Tables[0].Rows[0]["MSAbyVip"]))
                    {
                        cbMSA.Checked = false;
                    }
                    else
                    {
                        cbMSA.Checked = true;
                    }

                    if (Convert.ToBoolean(dsGetRequirementByMemberId.Tables[0].Rows[0]["ForBroker"]) == false ||
                       Convert.IsDBNull(dsGetRequirementByMemberId.Tables[0].Rows[0]["ForBroker"]))
                    {
                        cbEndorse.Checked = false;
                    }
                    else
                    {
                        cbEndorse.Checked = true;
                    }

                }
            }
        }

        private void btnSaveEndorsement_Click(object sender, EventArgs e)
        {
            try
            {
                SalesDocumentsBL salesDocumentsBL = new SalesDocumentsBL();

                salesDocumentsBL._memberId = Convert.ToInt32(dgEndorsement.SelectedRows[0].Cells[0].Value);

                if (cbMSA.Checked)
                {
                    salesDocumentsBL._MSAbyVip = true;
                }
                else
                {
                    salesDocumentsBL._MSAbyVip = false;
                }

                if (cbEndorse.Checked)
                {
                    salesDocumentsBL._forBroker = true;
                }
                else
                {
                    salesDocumentsBL._forBroker = false;
                }

                SalesPersonnelDAL.updateEndorsement(salesDocumentsBL);
                MessageBox.Show("Endorsement successfully updated");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void dgMSA_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgRequirements.Rows[e.RowIndex].Cells[0].Value);

                DataSet dsGetMSAByMemberId = SalesPersonnelDAL.getSalesForEndorsement(_memberId);
                if (dsGetMSAByMemberId.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["SigningMSA"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["SigningMSA"]))
                    {
                        cbMSASign.Checked = false;
                    }
                    else
                    {
                        cbMSASign.Checked = true;
                    }

                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["ATMAppplication"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["ATMAppplication"]))
                    {
                        cbATMApplication.Checked = false;
                    }
                    else
                    {
                        cbATMApplication.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["BankProcessingFee"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["BankProcessingFee"]))
                    {
                        cbBankProcessing.Checked = false;
                    }
                    else
                    {
                        cbBankProcessing.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["1x1IdPicForATM"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["1x1IdPicForATM"]))
                    {
                        cb1x1IDATM.Checked = false;
                    }
                    else
                    {
                        cb1x1IDATM.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["CopyOfValidId"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["CopyOfValidId"]))
                    {
                        cbValidId.Checked = false;
                    }
                    else
                    {
                        cbValidId.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["IDApplication"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["IDApplication"]))
                    {
                        cbIDApplication.Checked = false;
                    }
                    else
                    {
                        cbIDApplication.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["1x1IdPicForId"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["1x1IdPicForId"]))
                    {
                        cb1x1IDPic.Checked = false;
                    }
                    else
                    {
                        cb1x1IDPic.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["EnrollmentToBiometrics"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["EnrollmentToBiometrics"]))
                    {
                        cbEnrollmentToBiometrics.Checked = false;
                    }
                    else
                    {
                        cbEnrollmentToBiometrics.Checked = true;
                    }
                    if (Convert.ToBoolean(dsGetMSAByMemberId.Tables[0].Rows[0]["ForMSA"]) == false ||
                       Convert.IsDBNull(dsGetMSAByMemberId.Tables[0].Rows[0]["ForMSA"]))
                    {
                        cbForSignMSA.Checked = false;
                    }
                    else
                    {
                        cbForSignMSA.Checked = true;
                    }

                }
            }
        }

        private void btnSaveMSA_Click(object sender, EventArgs e)
        {
            try
            {
                SalesDocumentsBL salesDocumentsBL = new SalesDocumentsBL();

                salesDocumentsBL._memberId = Convert.ToInt32(dgMSA.SelectedRows[0].Cells[0].Value);

                if (cbMSASign.Checked == true)
                {
                    salesDocumentsBL._signingMSA = true;
                }
                else
                {
                    salesDocumentsBL._signingMSA = false;
                }
                //
                if (cbATMApplication.Checked == true)
                {
                    salesDocumentsBL._ATMAppplication = true;
                }
                else
                {
                    salesDocumentsBL._ATMAppplication = false;
                }
                //
                if (cbBankProcessing.Checked == true)
                {
                    salesDocumentsBL._bankProcessingFee = true;
                }
                else
                {
                    salesDocumentsBL._bankProcessingFee = false;
                }
                //
                if (cb1x1IDATM.Checked == true)
                {
                    salesDocumentsBL._1x1IdPicForATM = true;
                }
                else
                {
                    salesDocumentsBL._1x1IdPicForATM = false;
                }
                //
                if (cbValidId.Checked == true)
                {
                    salesDocumentsBL._copyOfValidId = true;
                }
                else
                {
                    salesDocumentsBL._copyOfValidId = false;
                }
                //
                if (cbIDApplication.Checked == true)
                {
                    salesDocumentsBL._IDApplication = true;
                }
                else
                {
                    salesDocumentsBL._IDApplication = false;
                }
                //
                if (cb1x1IDPic.Checked == true)
                {
                    salesDocumentsBL._1x1IdPicForId = true;
                }
                else
                {
                    salesDocumentsBL._1x1IdPicForId = false;
                }
                //
                if (cbEnrollmentToBiometrics.Checked == true)
                {
                    salesDocumentsBL._enrollmentToBiometrics = true;
                }
                else
                {
                    salesDocumentsBL._enrollmentToBiometrics = false;
                }
                //
                if (cbForSignMSA.Checked == true)
                {
                    salesDocumentsBL._forMSA = true;
                }
                //
                else
                {
                    salesDocumentsBL._forMSA = false;
                }

                SalesPersonnelDAL.updateMSA(salesDocumentsBL);
                MessageBox.Show("MSA successfully updated");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void dgEndorseToAccounting_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgEndorseToAccounting.Rows[e.RowIndex].Cells[0].Value);

                DataSet dsGetEndorsementToAccountingByMemberId = SalesPersonnelDAL.getSalesForEndorsementToAccounting(_memberId);
                if (dsGetEndorsementToAccountingByMemberId.Tables[0].Rows.Count > 0)
                {
                    txtEndorser.Text = dsGetEndorsementToAccountingByMemberId.Tables[0].Rows[0]["EndorsedBy"].ToString();
                    txtReceiver.Text = dsGetEndorsementToAccountingByMemberId.Tables[0].Rows[0]["ReceivedBy"].ToString();
                }
            }
        }

        private void btnAccredit_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to accredit this member?y", "Accreditation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SalesDocumentsBL salesDocumentsBL = new SalesDocumentsBL();

                    salesDocumentsBL._memberId = Convert.ToInt32(dgEndorseToAccounting.SelectedRows[0].Cells[0].Value);
                    salesDocumentsBL._endorsedBy = txtEndorser.Text;
                    salesDocumentsBL._receivedBy = txtReceiver.Text;
                
                    SalesPersonnelDAL.updateAccredit(salesDocumentsBL);
                    MessageBox.Show("Member successfully accredited.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
