﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;
using BusinessObjects;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManageMembers : Form
    {
        private int _memberId = 0, _salesPositionTypeId = 0;

        public frmManageMembers()
        {
            InitializeComponent();
        }

        private void tcManageMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcManageMembers.SelectedIndex == 2)
            {
                DataSet dsMemberList = SalesPersonnelDAL.getSalesPersonnelList();
                dgInactive.AutoGenerateColumns = false;
                dgInactive.DataSource = dsMemberList.Tables[1];

                btnSave.Visible = true;
                btnSave.Text = "Delete";
                btnSave.BackColor = Color.Red;
                clear();
            }
            else if (tcManageMembers.SelectedIndex == 1)
            {
                DataSet dsMemberList = SalesPersonnelDAL.getSalesPersonnelList();
                dgActive.AutoGenerateColumns = false;
                dgActive.DataSource = dsMemberList.Tables[0];

                btnSave.Visible = false;
                clear();
            }
            else
            {
                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnSave.BackColor = Color.Green;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tcManageMembers.SelectedIndex != 2 && _memberId > 0)
            {
                if (txtFirstName.Text != "" && txtCitizenship.Text != "" && txtContactNo1.Text != "")
                {
                    updateMember();

                    if (cbActive.Checked)
                        tcManageMembers.SelectedIndex = 1;
                    else if (!cbActive.Checked)
                        tcManageMembers.SelectedIndex = 2;
                    else
                        tcManageMembers.SelectedIndex = 0;
                    clear();
                }
                else
                {
                    MessageBox.Show("Compelete required information.");
                }
            }
            else if (tcManageMembers.SelectedIndex != 2 && _memberId == 0)
            {
                if (txtFirstName.Text != "" && txtCitizenship.Text != "" && txtContactNo1.Text != "")
                {
                    saveNewMember();

                    if (cbActive.Checked)
                        tcManageMembers.SelectedIndex = 1;
                    else if (!cbActive.Checked)
                        tcManageMembers.SelectedIndex = 2;
                    else
                        tcManageMembers.SelectedIndex = 0;
                    clear();
                }
                else
                {
                    MessageBox.Show("Compelete required information.");
                }
            }
            else if (tcManageMembers.SelectedIndex == 2)
            {
                deleteMember();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmManageMembers_Load(object sender, EventArgs e)
        {
            populateDropDowns();
            cmbCivilStatus.SelectedIndex = 0;
        }

        private void deleteMember()
        {
            SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
            frmLogin login = frmLogin.GetInstance();

            string ans = MessageBox.Show("Are you sure you want to delete this member?", "Delete Member", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                try
                {
                    int rowIndex = dgInactive.CurrentCell.RowIndex;
                    salesPersonnelBL._memberId = Convert.ToInt32(dgInactive.Rows[rowIndex].Cells["MemberId_Inactive"].Value);
                    salesPersonnelBL._lastModifiedUserId = login._loggedInUser._userId;
                    SalesPersonnelDAL.deleteMember(salesPersonnelBL);
                    MessageBox.Show("Member deleted successfully");

                    DataSet dsSalesPersonnelList = SalesPersonnelDAL.getSalesPersonnelList();
                    dgInactive.AutoGenerateColumns = false;
                    dgInactive.DataSource = dsSalesPersonnelList.Tables[1];

                }
                catch (Exception)// ex)
                {
                    MessageBox.Show("An error occured, please contact your Administrator.");
                }
            }
        }

        private void updateMember()
        {
            try
            {
                SalesPersonnelBL memberBL = new SalesPersonnelBL();
                SalesPersonnelHierarchyBL salesPersonnelHierarchyBL = new SalesPersonnelHierarchyBL();
                frmLogin login = frmLogin.GetInstance();

                memberBL._memberId = _memberId;
                memberBL._salesPositionTypeId = Convert.ToInt32(cmbSalesPosition.SelectedValue);
                memberBL._firstName = txtFirstName.Text.Trim();
                memberBL._middleName = txtMiddleName.Text.Trim();
                memberBL._lastName = txtLastName.Text.Trim();
                memberBL._nickName = txtNickName.Text.Trim();
                memberBL._civilStatus = cmbCivilStatus.Text.Trim();
                memberBL._religion = txtReligion.Text.Trim();
                memberBL._citizenship = txtCitizenship.Text.Trim();
                memberBL._birthDate = dtpBirthdate.Value.ToString();
                memberBL._birthPlace = txtBirthPlace.Text.Trim();
                memberBL._contactNo1 = txtContactNo1.Text.Trim();
                memberBL._contactNo2 = txtContactNo2.Text.Trim();
                memberBL._emailAddress = txtEmailAdd.Text.Trim();
                memberBL._addressNo = txtAddressNo.Text.Trim();
                memberBL._addressStreet = txtStreetNo.Text.Trim();
                memberBL._addressVillageOrSubdivision = txtSubdivision.Text.Trim();
                memberBL._addressBarangayOrDistrict = txtBarangay.Text.Trim();
                memberBL._addressTownOrCity = txtCity.Text.Trim();
                memberBL._lastModifiedUserId = login._loggedInUser._userId;
                salesPersonnelHierarchyBL._supervisorMemberId = Convert.ToInt32(cmbSupervisor.SelectedValue);
                salesPersonnelHierarchyBL._lastModifiedUserId = login._loggedInUser._userId;


                if (rbtnMale.Checked && !rbtnFemale.Checked)
                    memberBL._gender = true;
                else
                    memberBL._gender = false;

                if (cbActive.Checked)
                    memberBL._isActive = true;
                else
                    memberBL._isActive = false;

                SalesPersonnelDAL.updateMemberDetails(memberBL, salesPersonnelHierarchyBL);
                MessageBox.Show("Member details successfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator." + ex.ToString());
            }
        }

        private void saveNewMember()
        {
            try
            {
                SalesPersonnelBL memberBL = new SalesPersonnelBL();
                SalesDocumentsBL salesDocumentBL = new SalesDocumentsBL();
                SalesPersonnelHierarchyBL salesPersonnelHierarchyBL = new SalesPersonnelHierarchyBL();
                frmLogin login = frmLogin.GetInstance();

                memberBL._salesPositionTypeId = Convert.ToInt32(cmbSalesPosition.SelectedValue);
                memberBL._memberTypeId = 1;
                memberBL._firstName = txtFirstName.Text.Trim();
                memberBL._middleName = txtMiddleName.Text.Trim();
                memberBL._lastName = txtLastName.Text.Trim();
                memberBL._nickName = txtNickName.Text.Trim();
                memberBL._civilStatus = cmbCivilStatus.Text.Trim();
                memberBL._religion = txtReligion.Text.Trim();
                memberBL._citizenship = txtCitizenship.Text.Trim();
                memberBL._birthDate = dtpBirthdate.Value.ToString();
                memberBL._birthPlace = txtBirthPlace.Text.Trim();
                memberBL._contactNo1 = txtContactNo1.Text.Trim();
                memberBL._contactNo2 = txtContactNo2.Text.Trim();
                memberBL._emailAddress = txtEmailAdd.Text.Trim();
                memberBL._addressNo = txtAddressNo.Text.Trim();
                memberBL._addressStreet = txtStreetNo.Text.Trim();
                memberBL._addressVillageOrSubdivision = txtSubdivision.Text.Trim();
                memberBL._addressBarangayOrDistrict = txtBarangay.Text.Trim();
                memberBL._addressTownOrCity = txtCity.Text.Trim();
                memberBL._createdUserId = login._loggedInUser._userId;
                memberBL._lastModifiedUserId = login._loggedInUser._userId;
                salesDocumentBL._tINNo = txtTINNo.Text.Trim();
                salesDocumentBL._lastModifiedUserId = login._loggedInUser._userId;
                salesPersonnelHierarchyBL._supervisorMemberId = Convert.ToInt32(cmbSupervisor.SelectedValue);
                salesPersonnelHierarchyBL._lastModifiedUserId = login._loggedInUser._userId;


                if (cbCOA.Checked)
                    salesDocumentBL._applicationAndCOA = true;
                else
                    salesDocumentBL._applicationAndCOA = false;

                if (cbValidID.Checked)
                    salesDocumentBL._governmentID = true;
                else
                    salesDocumentBL._governmentID = false;

                if (cbPic.Checked)
                    salesDocumentBL._2x2Photo = true;
                else
                    salesDocumentBL._2x2Photo = false;

                if (cbSignatureOfEndorser.Checked)
                    salesDocumentBL._signatureOfEndorsers = true;
                else
                    salesDocumentBL._signatureOfEndorsers = false;

                if (rbtnMale.Checked && !rbtnFemale.Checked)
                    memberBL._gender = true;
                else
                    memberBL._gender = false;

                if (cbActive.Checked)
                    memberBL._isActive = true;
                else
                    memberBL._isActive = false;

                SalesPersonnelDAL.insertNewMember(memberBL, salesDocumentBL, salesPersonnelHierarchyBL);
                MessageBox.Show("New member successfully added.");
            }
            catch (Exception)
            {
                MessageBox.Show("An error occured contact your administrator.");
            }
        }

        private void cbTIN_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTIN.Checked)
            {
                lblTINNo.Enabled = true;
                txtTINNo.Enabled = true;
                txtTINNo.Text = "";
            }
            else
            {
                lblTINNo.Enabled = false;
                txtTINNo.Enabled = false;
            }
        }

        private void cmbSalesPosition_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbSalesPosition.SelectedIndex == 0)
            {
                lblSupervisor.Enabled = false;
                cmbSupervisor.Enabled = false;
            }
            else
            {
                lblSupervisor.Enabled = true;
                cmbSupervisor.Enabled = true;

                //Set DataSet
                _salesPositionTypeId = Convert.ToInt32(cmbSalesPosition.SelectedValue);

                DataSet dsHigherSalesPersonnel = SalesPersonnelDAL.getHigherSalesPersonnelBySalesPositionId(_salesPositionTypeId);

                //Populate DropDownList
                cmbSupervisor.DataSource = dsHigherSalesPersonnel.Tables[0];
                cmbSupervisor.DisplayMember = "FullName";
                cmbSupervisor.ValueMember = "MemberId";
            }
        }

        //HELPERS
        #region HELPERS

        private void txtTINNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtContactNo1_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }

        private void txtContactNo2_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }

        private static void phoneValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46 && e.KeyChar !=43)
                e.Handled = true;

            if (e.KeyChar == 43)
            {
                if ((sender as TextBox).Text.Contains("+"))
                    e.Handled = true;
            }
        }

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 32)
                e.Handled = true;
        }
        
        private void populateDropDowns()
        {
            //Set DataSet
            DataSet dsSalesPersonnelPosition = SalesPersonnelDAL.getSalesPositionList();
            
            //Populate DropDownList
            cmbSalesPosition.DataSource = dsSalesPersonnelPosition.Tables[0];
            cmbSalesPosition.DisplayMember = "SalesPositionName";
            cmbSalesPosition.ValueMember = "SalesPositionId";
        }

        private void clear()
        {
            _memberId = 0;

            tcManageMembers.TabPages[0].Text = "New";
            cmbSalesPosition.SelectedIndex = 0;
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtLastName.Text = "";
            txtNickName.Text = "";
            cmbCivilStatus.Text = "";
            txtReligion.Text = "";
            txtCitizenship.Text = "";
            dtpBirthdate.Value = DateTime.Now;
            txtBirthPlace.Text = "";
            txtContactNo1.Text = "";
            txtContactNo2.Text = "";
            txtEmailAdd.Text = "";
            txtAddressNo.Text = "";
            txtStreetNo.Text = "";;
            txtSubdivision.Text = "";
            txtBarangay.Text = "";
            txtCity.Text = "";
            cbTIN.Checked = false;
            cmbSupervisor.Text = "";
            rbtnMale.Checked = true;
            rbtnFemale.Checked = false;
            cmbCivilStatus.SelectedIndex = 0;
            cbActive.Checked = true;
        }

        #endregion

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgActive.Rows[e.RowIndex].Cells["Edit_Active"] == dgActive.CurrentCell)
                {
                    SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                    salesPersonnelBL._memberId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["MemberId_Active"].Value);

                    DataSet dsGetMemberDetails = SalesPersonnelDAL.getSalesPersonnelListById(salesPersonnelBL);

                    if (dsGetMemberDetails.Tables[0].Rows.Count > 0)
                    {
                        //Personal info
                        _memberId = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["MemberId"]);
                        txtFirstName.Text = dsGetMemberDetails.Tables[0].Rows[0]["FirstName"].ToString();
                        txtMiddleName.Text = dsGetMemberDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                        txtLastName.Text = dsGetMemberDetails.Tables[0].Rows[0]["LastName"].ToString();
                        txtNickName.Text = dsGetMemberDetails.Tables[0].Rows[0]["NickName"].ToString();
                        cmbCivilStatus.Text = dsGetMemberDetails.Tables[0].Rows[0]["CivilStatus"].ToString();
                        txtReligion.Text = dsGetMemberDetails.Tables[0].Rows[0]["Religion"].ToString();
                        txtCitizenship.Text = dsGetMemberDetails.Tables[0].Rows[0]["Citizenship"].ToString();
                        dtpBirthdate.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["BirthDate"]);
                        txtBirthPlace.Text = dsGetMemberDetails.Tables[0].Rows[0]["BirthPlace"].ToString();
                        txtAddressNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressNo"].ToString();
                        txtStreetNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressStreet"].ToString();
                        txtSubdivision.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressVillageOrSubdivision"].ToString();
                        txtBarangay.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressBarangayOrDistrict"].ToString();
                        txtCity.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressTownOrCity"].ToString();
                        txtContactNo1.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                        txtContactNo2.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                        txtEmailAdd.Text = dsGetMemberDetails.Tables[0].Rows[0]["EmailAddress"].ToString();

                        //documents
                        cmbSalesPosition.SelectedValue = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["SalesPositionTypeId"]);
                        cmbSupervisor.SelectedValue = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["SupervisorId"]);

                        if (_memberId > 0)
                        {
                            tcManageMembers.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManageMembers.TabPages[0].Text = "New";
                        }

                        if (Convert.ToBoolean(dsGetMemberDetails.Tables[0].Rows[0]["Gender"]) == true)
                        {
                            rbtnMale.Checked = true;
                            rbtnFemale.Checked = false;
                        }
                        else
                        {
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }

                        cmbSupervisor.Enabled = true;
                        gbDocments.Enabled = false;
                        cbActive.Checked = true;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find member.");
                    }

                    tcManageMembers.SelectedIndex = 0;
                }
            }
        }

        private void dgInactive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgInactive.Rows[e.RowIndex].Cells["Edit_Inactive"] == dgInactive.CurrentCell)
                {
                    SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                    salesPersonnelBL._memberId = Convert.ToInt32(dgInactive.Rows[e.RowIndex].Cells["MemberId_Inactive"].Value);

                    DataSet dsGetMemberDetails = SalesPersonnelDAL.getSalesPersonnelListById(salesPersonnelBL);

                    if (dsGetMemberDetails.Tables[0].Rows.Count > 0)
                    {
                        //Personal info
                        _memberId = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["MemberId"]);
                        txtFirstName.Text = dsGetMemberDetails.Tables[0].Rows[0]["FirstName"].ToString();
                        txtMiddleName.Text = dsGetMemberDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                        txtLastName.Text = dsGetMemberDetails.Tables[0].Rows[0]["LastName"].ToString();
                        txtNickName.Text = dsGetMemberDetails.Tables[0].Rows[0]["NickName"].ToString();
                        cmbCivilStatus.Text = dsGetMemberDetails.Tables[0].Rows[0]["CivilStatus"].ToString();
                        txtReligion.Text = dsGetMemberDetails.Tables[0].Rows[0]["Religion"].ToString();
                        txtCitizenship.Text = dsGetMemberDetails.Tables[0].Rows[0]["Citizenship"].ToString();
                        dtpBirthdate.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["BirthDate"]);
                        txtBirthPlace.Text = dsGetMemberDetails.Tables[0].Rows[0]["BirthPlace"].ToString();
                        txtAddressNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressNo"].ToString();
                        txtStreetNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressStreet"].ToString();
                        txtSubdivision.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressVillageOrSubdivision"].ToString();
                        txtBarangay.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressBarangayOrDistrict"].ToString();
                        txtCity.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressTownOrCity"].ToString();
                        txtContactNo1.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                        txtContactNo2.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                        txtEmailAdd.Text = dsGetMemberDetails.Tables[0].Rows[0]["EmailAddress"].ToString();

                        //documents
                        cmbSalesPosition.SelectedValue = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["SalesPositionTypeId"]);
                        cmbSupervisor.SelectedValue = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["SupervisorId"]);

                        if (_memberId > 0)
                        {
                            tcManageMembers.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManageMembers.TabPages[0].Text = "New";
                        }

                        if (Convert.ToBoolean(dsGetMemberDetails.Tables[0].Rows[0]["Gender"]) == true)
                        {
                            rbtnMale.Checked = true;
                            rbtnFemale.Checked = false;
                        }
                        else
                        {
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }

                        cmbSupervisor.Enabled = true;
                        gbDocments.Enabled = false;
                        cbActive.Checked = false;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find member.");
                    }

                    tcManageMembers.SelectedIndex = 0;
                }
            }
        }
    }
}
