﻿namespace Inventory_Management
{
    partial class frmConfigureCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCompanySetting = new System.Windows.Forms.GroupBox();
            this.cbCurrencies = new System.Windows.Forms.ComboBox();
            this.txtCurrency = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbCompanyAdmin = new System.Windows.Forms.GroupBox();
            this.btnFinish = new System.Windows.Forms.Button();
            this.txtAnswer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.gbCompanySetting.SuspendLayout();
            this.gbCompanyAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCompanySetting
            // 
            this.gbCompanySetting.Controls.Add(this.cbCurrencies);
            this.gbCompanySetting.Controls.Add(this.txtCurrency);
            this.gbCompanySetting.Controls.Add(this.txtCompanyName);
            this.gbCompanySetting.Controls.Add(this.label1);
            this.gbCompanySetting.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCompanySetting.Location = new System.Drawing.Point(33, 21);
            this.gbCompanySetting.Name = "gbCompanySetting";
            this.gbCompanySetting.Size = new System.Drawing.Size(535, 112);
            this.gbCompanySetting.TabIndex = 0;
            this.gbCompanySetting.TabStop = false;
            this.gbCompanySetting.Text = "Company Details";
            // 
            // cbCurrencies
            // 
            this.cbCurrencies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCurrencies.FormattingEnabled = true;
            this.cbCurrencies.Location = new System.Drawing.Point(173, 70);
            this.cbCurrencies.Name = "cbCurrencies";
            this.cbCurrencies.Size = new System.Drawing.Size(274, 23);
            this.cbCurrencies.TabIndex = 3;
            this.cbCurrencies.SelectedIndexChanged += new System.EventHandler(this.cbCurrencies_SelectedIndexChanged);
            // 
            // txtCurrency
            // 
            this.txtCurrency.AutoSize = true;
            this.txtCurrency.Location = new System.Drawing.Point(91, 70);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Size = new System.Drawing.Size(64, 15);
            this.txtCurrency.TabIndex = 2;
            this.txtCurrency.Text = "Currency  :";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(173, 31);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(274, 23);
            this.txtCompanyName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Name :";
            // 
            // gbCompanyAdmin
            // 
            this.gbCompanyAdmin.Controls.Add(this.txtUserName);
            this.gbCompanyAdmin.Controls.Add(this.label10);
            this.gbCompanyAdmin.Controls.Add(this.btnFinish);
            this.gbCompanyAdmin.Controls.Add(this.txtAnswer);
            this.gbCompanyAdmin.Controls.Add(this.label9);
            this.gbCompanyAdmin.Controls.Add(this.txtQuestion);
            this.gbCompanyAdmin.Controls.Add(this.label8);
            this.gbCompanyAdmin.Controls.Add(this.txtPassword);
            this.gbCompanyAdmin.Controls.Add(this.label7);
            this.gbCompanyAdmin.Controls.Add(this.txtPassword2);
            this.gbCompanyAdmin.Controls.Add(this.label6);
            this.gbCompanyAdmin.Controls.Add(this.txtEmail);
            this.gbCompanyAdmin.Controls.Add(this.label5);
            this.gbCompanyAdmin.Controls.Add(this.txtPhone);
            this.gbCompanyAdmin.Controls.Add(this.label4);
            this.gbCompanyAdmin.Controls.Add(this.txtLastName);
            this.gbCompanyAdmin.Controls.Add(this.label3);
            this.gbCompanyAdmin.Controls.Add(this.txtFirstName);
            this.gbCompanyAdmin.Controls.Add(this.label2);
            this.gbCompanyAdmin.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCompanyAdmin.Location = new System.Drawing.Point(33, 142);
            this.gbCompanyAdmin.Name = "gbCompanyAdmin";
            this.gbCompanyAdmin.Size = new System.Drawing.Size(535, 308);
            this.gbCompanyAdmin.TabIndex = 1;
            this.gbCompanyAdmin.TabStop = false;
            this.gbCompanyAdmin.Text = "Company Administrator";
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(272, 276);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(75, 23);
            this.btnFinish.TabIndex = 18;
            this.btnFinish.Text = "Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // txtAnswer
            // 
            this.txtAnswer.Location = new System.Drawing.Point(173, 242);
            this.txtAnswer.MaxLength = 20;
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(274, 23);
            this.txtAnswer.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(70, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Secret Answer :";
            // 
            // txtQuestion
            // 
            this.txtQuestion.Location = new System.Drawing.Point(173, 215);
            this.txtQuestion.MaxLength = 50;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(274, 23);
            this.txtQuestion.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Secret Question :";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(173, 159);
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(274, 23);
            this.txtPassword.TabIndex = 13;
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Employee Password :";
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(173, 187);
            this.txtPassword2.MaxLength = 20;
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.Size = new System.Drawing.Size(274, 23);
            this.txtPassword2.TabIndex = 11;
            this.txtPassword2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword2_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Employee UserName :";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(173, 105);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(274, 23);
            this.txtEmail.TabIndex = 9;
            this.txtEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(115, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Email :";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(173, 76);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(274, 23);
            this.txtPhone.TabIndex = 7;
            this.txtPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(110, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Phone :";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(173, 48);
            this.txtLastName.MaxLength = 25;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(274, 23);
            this.txtLastName.TabIndex = 5;
            this.txtLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLastName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(93, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Lastname :";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(173, 21);
            this.txtFirstName.MaxLength = 25;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(274, 23);
            this.txtFirstName.TabIndex = 3;
            this.txtFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFirstName_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Firstname :";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(173, 132);
            this.txtUserName.MaxLength = 20;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(274, 23);
            this.txtUserName.TabIndex = 20;
            this.txtUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserName_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 19;
            this.label10.Text = "Re-type Password :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(305, 456);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Done";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmConfigureCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 489);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gbCompanyAdmin);
            this.Controls.Add(this.gbCompanySetting);
            this.Name = "frmConfigureCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setup New Company";
            this.Load += new System.EventHandler(this.frmConfigureCompany_Load);
            this.gbCompanySetting.ResumeLayout(false);
            this.gbCompanySetting.PerformLayout();
            this.gbCompanyAdmin.ResumeLayout(false);
            this.gbCompanyAdmin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCompanySetting;
        private System.Windows.Forms.ComboBox cbCurrencies;
        private System.Windows.Forms.Label txtCurrency;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbCompanyAdmin;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.TextBox txtAnswer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}