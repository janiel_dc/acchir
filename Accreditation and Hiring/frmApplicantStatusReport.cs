﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;


namespace Accreditation_And_Hiring
{
    public partial class frmApplicantStatusReport : Form
    {
        private int _memberTypeId = 0;

        public frmApplicantStatusReport()
        {
            InitializeComponent();
        }

        private void frmApplicantStatusReport_Load(object sender, EventArgs e)
        {
            DataSet dsMemberTypeList = OrganicPersonnelDAL.getMemberTypeList();

            cmbApplicantType.DataSource = dsMemberTypeList.Tables[0];
            cmbApplicantType.DisplayMember = "MemberTypeName";
            cmbApplicantType.ValueMember = "MemberTypeId";

            _memberTypeId = Convert.ToInt32(cmbApplicantType.SelectedValue);

            loadReport();
        }

        private void loadReport()
        {
            try
            {
                // TODO: This line of code loads data into the 'dataSetSalesOrderDailyReport.DAL_GET_GetSalesOrderDailyReport' table. You can move, or remove it, as needed.
                this.dAL_GET_ApplicantStatusByAppplicantTypeTableAdapter.Fill(this.dataSetApplicantStatus.DAL_GET_ApplicantStatusByAppplicantType, _memberTypeId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured, please contact your Administrator.");
            }

            this.reportViewer1.RefreshReport();
        }

        private void cmbApplicantType_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbApplicantType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _memberTypeId = Convert.ToInt32(cmbApplicantType.SelectedValue);

            loadReport();
        }
    }
}
