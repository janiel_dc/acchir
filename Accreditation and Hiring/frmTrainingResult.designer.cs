﻿namespace Accreditation_and_Hiring_System
{
    partial class frmTrainingResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tcTrainingResult = new System.Windows.Forms.TabControl();
            this.tpAddParticipants = new System.Windows.Forms.TabPage();
            this.gbAddParticipants = new System.Windows.Forms.GroupBox();
            this.cmbMemberName = new System.Windows.Forms.ComboBox();
            this.btnAddParticipant = new System.Windows.Forms.Button();
            this.cmbTrainingName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tpTrainingResullt = new System.Windows.Forms.TabPage();
            this.dgManageResult = new System.Windows.Forms.DataGridView();
            this.MemberId_Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingId_Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentName_Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Day1_Result = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Day2_Result = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ExamScore_Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamItems_Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pass_Result = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Save_Result = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTrainingNameList = new System.Windows.Forms.ComboBox();
            this.tpFinishedTrainings = new System.Windows.Forms.TabPage();
            this.dgFinishedTraining = new System.Windows.Forms.DataGridView();
            this.MemberId_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingId_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Day1_Finished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Day2_Finished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ExamScore_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamItems_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isPass_Finished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTrainingListFinished = new System.Windows.Forms.ComboBox();
            this.panel2.SuspendLayout();
            this.tcTrainingResult.SuspendLayout();
            this.tpAddParticipants.SuspendLayout();
            this.gbAddParticipants.SuspendLayout();
            this.tpTrainingResullt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgManageResult)).BeginInit();
            this.panel1.SuspendLayout();
            this.tpFinishedTrainings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFinishedTraining)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 435);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(760, 28);
            this.panel2.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(673, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(760, 33);
            this.label1.TabIndex = 13;
            this.label1.Text = "Training Result";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tcTrainingResult
            // 
            this.tcTrainingResult.Controls.Add(this.tpAddParticipants);
            this.tcTrainingResult.Controls.Add(this.tpTrainingResullt);
            this.tcTrainingResult.Controls.Add(this.tpFinishedTrainings);
            this.tcTrainingResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTrainingResult.Location = new System.Drawing.Point(0, 33);
            this.tcTrainingResult.Name = "tcTrainingResult";
            this.tcTrainingResult.SelectedIndex = 0;
            this.tcTrainingResult.Size = new System.Drawing.Size(760, 402);
            this.tcTrainingResult.TabIndex = 15;
            this.tcTrainingResult.SelectedIndexChanged += new System.EventHandler(this.tcTrainingResult_SelectedIndexChanged);
            // 
            // tpAddParticipants
            // 
            this.tpAddParticipants.Controls.Add(this.gbAddParticipants);
            this.tpAddParticipants.Location = new System.Drawing.Point(4, 22);
            this.tpAddParticipants.Name = "tpAddParticipants";
            this.tpAddParticipants.Padding = new System.Windows.Forms.Padding(3);
            this.tpAddParticipants.Size = new System.Drawing.Size(752, 376);
            this.tpAddParticipants.TabIndex = 0;
            this.tpAddParticipants.Text = "Add Participants";
            this.tpAddParticipants.UseVisualStyleBackColor = true;
            // 
            // gbAddParticipants
            // 
            this.gbAddParticipants.Controls.Add(this.cmbMemberName);
            this.gbAddParticipants.Controls.Add(this.btnAddParticipant);
            this.gbAddParticipants.Controls.Add(this.cmbTrainingName);
            this.gbAddParticipants.Controls.Add(this.label2);
            this.gbAddParticipants.Controls.Add(this.label3);
            this.gbAddParticipants.Location = new System.Drawing.Point(212, 98);
            this.gbAddParticipants.Name = "gbAddParticipants";
            this.gbAddParticipants.Size = new System.Drawing.Size(342, 128);
            this.gbAddParticipants.TabIndex = 4;
            this.gbAddParticipants.TabStop = false;
            // 
            // cmbMemberName
            // 
            this.cmbMemberName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMemberName.FormattingEnabled = true;
            this.cmbMemberName.Location = new System.Drawing.Point(91, 61);
            this.cmbMemberName.Name = "cmbMemberName";
            this.cmbMemberName.Size = new System.Drawing.Size(241, 21);
            this.cmbMemberName.TabIndex = 3;
            // 
            // btnAddParticipant
            // 
            this.btnAddParticipant.BackColor = System.Drawing.Color.Green;
            this.btnAddParticipant.ForeColor = System.Drawing.Color.White;
            this.btnAddParticipant.Location = new System.Drawing.Point(232, 88);
            this.btnAddParticipant.Name = "btnAddParticipant";
            this.btnAddParticipant.Size = new System.Drawing.Size(100, 23);
            this.btnAddParticipant.TabIndex = 0;
            this.btnAddParticipant.Text = "Add Participant";
            this.btnAddParticipant.UseVisualStyleBackColor = false;
            this.btnAddParticipant.Click += new System.EventHandler(this.btnAddParticipant_Click);
            // 
            // cmbTrainingName
            // 
            this.cmbTrainingName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTrainingName.FormattingEnabled = true;
            this.cmbTrainingName.Location = new System.Drawing.Point(91, 35);
            this.cmbTrainingName.Name = "cmbTrainingName";
            this.cmbTrainingName.Size = new System.Drawing.Size(241, 21);
            this.cmbTrainingName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Training Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Member Name:";
            // 
            // tpTrainingResullt
            // 
            this.tpTrainingResullt.Controls.Add(this.dgManageResult);
            this.tpTrainingResullt.Controls.Add(this.panel1);
            this.tpTrainingResullt.Location = new System.Drawing.Point(4, 22);
            this.tpTrainingResullt.Name = "tpTrainingResullt";
            this.tpTrainingResullt.Padding = new System.Windows.Forms.Padding(3);
            this.tpTrainingResullt.Size = new System.Drawing.Size(752, 376);
            this.tpTrainingResullt.TabIndex = 1;
            this.tpTrainingResullt.Text = "Manage Result";
            this.tpTrainingResullt.UseVisualStyleBackColor = true;
            // 
            // dgManageResult
            // 
            this.dgManageResult.AllowUserToAddRows = false;
            this.dgManageResult.AllowUserToDeleteRows = false;
            this.dgManageResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgManageResult.BackgroundColor = System.Drawing.Color.White;
            this.dgManageResult.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgManageResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgManageResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId_Result,
            this.TrainingId_Result,
            this.AgentName_Result,
            this.Day1_Result,
            this.Day2_Result,
            this.ExamScore_Result,
            this.ExamItems_Result,
            this.Pass_Result,
            this.Save_Result});
            this.dgManageResult.Location = new System.Drawing.Point(11, 45);
            this.dgManageResult.Name = "dgManageResult";
            this.dgManageResult.RowHeadersVisible = false;
            this.dgManageResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgManageResult.Size = new System.Drawing.Size(733, 310);
            this.dgManageResult.TabIndex = 21;
            this.dgManageResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgManageResult_CellContentClick);
            // 
            // MemberId_Result
            // 
            this.MemberId_Result.DataPropertyName = "MemberId";
            this.MemberId_Result.HeaderText = "MemberId";
            this.MemberId_Result.Name = "MemberId_Result";
            this.MemberId_Result.Visible = false;
            // 
            // TrainingId_Result
            // 
            this.TrainingId_Result.DataPropertyName = "TrainingId";
            this.TrainingId_Result.HeaderText = "TrainingId";
            this.TrainingId_Result.Name = "TrainingId_Result";
            this.TrainingId_Result.Visible = false;
            // 
            // AgentName_Result
            // 
            this.AgentName_Result.DataPropertyName = "FullName";
            this.AgentName_Result.HeaderText = "AgentName";
            this.AgentName_Result.Name = "AgentName_Result";
            // 
            // Day1_Result
            // 
            this.Day1_Result.DataPropertyName = "isAttendedDay1";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.NullValue = false;
            this.Day1_Result.DefaultCellStyle = dataGridViewCellStyle1;
            this.Day1_Result.HeaderText = "Day1";
            this.Day1_Result.Name = "Day1_Result";
            // 
            // Day2_Result
            // 
            this.Day2_Result.DataPropertyName = "isAttendedDay2";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.NullValue = false;
            this.Day2_Result.DefaultCellStyle = dataGridViewCellStyle2;
            this.Day2_Result.HeaderText = "Day2";
            this.Day2_Result.Name = "Day2_Result";
            // 
            // ExamScore_Result
            // 
            this.ExamScore_Result.DataPropertyName = "ExamScore";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.ExamScore_Result.DefaultCellStyle = dataGridViewCellStyle3;
            this.ExamScore_Result.HeaderText = "ExamScore";
            this.ExamScore_Result.MaxInputLength = 3;
            this.ExamScore_Result.Name = "ExamScore_Result";
            // 
            // ExamItems_Result
            // 
            this.ExamItems_Result.DataPropertyName = "TrainingExamTotalNumber";
            this.ExamItems_Result.HeaderText = "Exam Items";
            this.ExamItems_Result.Name = "ExamItems_Result";
            // 
            // Pass_Result
            // 
            this.Pass_Result.DataPropertyName = "isPassed";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.NullValue = false;
            this.Pass_Result.DefaultCellStyle = dataGridViewCellStyle4;
            this.Pass_Result.HeaderText = "Pass";
            this.Pass_Result.Name = "Pass_Result";
            // 
            // Save_Result
            // 
            this.Save_Result.HeaderText = "";
            this.Save_Result.Name = "Save_Result";
            this.Save_Result.Text = "Save";
            this.Save_Result.UseColumnTextForButtonValue = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cmbTrainingNameList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 36);
            this.panel1.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(386, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Training Name:";
            // 
            // cmbTrainingNameList
            // 
            this.cmbTrainingNameList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTrainingNameList.FormattingEnabled = true;
            this.cmbTrainingNameList.Location = new System.Drawing.Point(471, 8);
            this.cmbTrainingNameList.Name = "cmbTrainingNameList";
            this.cmbTrainingNameList.Size = new System.Drawing.Size(270, 21);
            this.cmbTrainingNameList.TabIndex = 0;
            this.cmbTrainingNameList.SelectionChangeCommitted += new System.EventHandler(this.cmbTrainingNameList_SelectionChangeCommitted);
            // 
            // tpFinishedTrainings
            // 
            this.tpFinishedTrainings.Controls.Add(this.dgFinishedTraining);
            this.tpFinishedTrainings.Controls.Add(this.panel3);
            this.tpFinishedTrainings.Location = new System.Drawing.Point(4, 22);
            this.tpFinishedTrainings.Name = "tpFinishedTrainings";
            this.tpFinishedTrainings.Padding = new System.Windows.Forms.Padding(3);
            this.tpFinishedTrainings.Size = new System.Drawing.Size(752, 376);
            this.tpFinishedTrainings.TabIndex = 2;
            this.tpFinishedTrainings.Text = "Finished Trainings";
            this.tpFinishedTrainings.UseVisualStyleBackColor = true;
            // 
            // dgFinishedTraining
            // 
            this.dgFinishedTraining.AllowUserToAddRows = false;
            this.dgFinishedTraining.AllowUserToDeleteRows = false;
            this.dgFinishedTraining.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFinishedTraining.BackgroundColor = System.Drawing.Color.White;
            this.dgFinishedTraining.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgFinishedTraining.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFinishedTraining.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId_Finished,
            this.TrainingId_Finished,
            this.FullName_Finished,
            this.Day1_Finished,
            this.Day2_Finished,
            this.ExamScore_Finished,
            this.ExamItems_Finished,
            this.isPass_Finished});
            this.dgFinishedTraining.Location = new System.Drawing.Point(8, 50);
            this.dgFinishedTraining.Name = "dgFinishedTraining";
            this.dgFinishedTraining.ReadOnly = true;
            this.dgFinishedTraining.RowHeadersVisible = false;
            this.dgFinishedTraining.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgFinishedTraining.Size = new System.Drawing.Size(733, 310);
            this.dgFinishedTraining.TabIndex = 22;
            // 
            // MemberId_Finished
            // 
            this.MemberId_Finished.DataPropertyName = "MemberId";
            this.MemberId_Finished.HeaderText = "MemberId";
            this.MemberId_Finished.Name = "MemberId_Finished";
            this.MemberId_Finished.ReadOnly = true;
            this.MemberId_Finished.Visible = false;
            // 
            // TrainingId_Finished
            // 
            this.TrainingId_Finished.DataPropertyName = "TrainingId";
            this.TrainingId_Finished.HeaderText = "TrainingId";
            this.TrainingId_Finished.Name = "TrainingId_Finished";
            this.TrainingId_Finished.ReadOnly = true;
            this.TrainingId_Finished.Visible = false;
            // 
            // FullName_Finished
            // 
            this.FullName_Finished.DataPropertyName = "FullName";
            this.FullName_Finished.HeaderText = "AgentName";
            this.FullName_Finished.Name = "FullName_Finished";
            this.FullName_Finished.ReadOnly = true;
            // 
            // Day1_Finished
            // 
            this.Day1_Finished.DataPropertyName = "isAttendedDay1";
            this.Day1_Finished.HeaderText = "Day1";
            this.Day1_Finished.Name = "Day1_Finished";
            this.Day1_Finished.ReadOnly = true;
            // 
            // Day2_Finished
            // 
            this.Day2_Finished.DataPropertyName = "isAttendedDay2";
            this.Day2_Finished.HeaderText = "Day2";
            this.Day2_Finished.Name = "Day2_Finished";
            this.Day2_Finished.ReadOnly = true;
            // 
            // ExamScore_Finished
            // 
            this.ExamScore_Finished.DataPropertyName = "ExamScore";
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.ExamScore_Finished.DefaultCellStyle = dataGridViewCellStyle5;
            this.ExamScore_Finished.HeaderText = "ExamScore";
            this.ExamScore_Finished.Name = "ExamScore_Finished";
            this.ExamScore_Finished.ReadOnly = true;
            // 
            // ExamItems_Finished
            // 
            this.ExamItems_Finished.DataPropertyName = "TrainingExamTotalNumber";
            this.ExamItems_Finished.HeaderText = "Exam Items";
            this.ExamItems_Finished.Name = "ExamItems_Finished";
            this.ExamItems_Finished.ReadOnly = true;
            // 
            // isPass_Finished
            // 
            this.isPass_Finished.DataPropertyName = "isPassed";
            this.isPass_Finished.HeaderText = "Pass";
            this.isPass_Finished.Name = "isPass_Finished";
            this.isPass_Finished.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.cmbTrainingListFinished);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(746, 41);
            this.panel3.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(386, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Training Name:";
            // 
            // cmbTrainingListFinished
            // 
            this.cmbTrainingListFinished.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTrainingListFinished.FormattingEnabled = true;
            this.cmbTrainingListFinished.Location = new System.Drawing.Point(471, 10);
            this.cmbTrainingListFinished.Name = "cmbTrainingListFinished";
            this.cmbTrainingListFinished.Size = new System.Drawing.Size(270, 21);
            this.cmbTrainingListFinished.TabIndex = 0;
            this.cmbTrainingListFinished.SelectionChangeCommitted += new System.EventHandler(this.cmbTrainingListFinished_SelectionChangeCommitted);
            // 
            // frmTrainingResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(760, 463);
            this.Controls.Add(this.tcTrainingResult);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Name = "frmTrainingResult";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frmTrainingResult_Load);
            this.panel2.ResumeLayout(false);
            this.tcTrainingResult.ResumeLayout(false);
            this.tpAddParticipants.ResumeLayout(false);
            this.gbAddParticipants.ResumeLayout(false);
            this.gbAddParticipants.PerformLayout();
            this.tpTrainingResullt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgManageResult)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tpFinishedTrainings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFinishedTraining)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tcTrainingResult;
        private System.Windows.Forms.TabPage tpAddParticipants;
        private System.Windows.Forms.TabPage tpTrainingResullt;
        private System.Windows.Forms.ComboBox cmbMemberName;
        private System.Windows.Forms.ComboBox cmbTrainingName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddParticipant;
        private System.Windows.Forms.GroupBox gbAddParticipants;
        private System.Windows.Forms.TabPage tpFinishedTrainings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTrainingNameList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTrainingListFinished;
        private System.Windows.Forms.DataGridView dgManageResult;
        private System.Windows.Forms.DataGridView dgFinishedTraining;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingId_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName_Finished;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Day1_Finished;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Day2_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamItems_Finished;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPass_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingId_Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentName_Result;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Day1_Result;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Day2_Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore_Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamItems_Result;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Pass_Result;
        private System.Windows.Forms.DataGridViewButtonColumn Save_Result;
    }
}