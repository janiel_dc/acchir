﻿namespace Accreditation_and_Hiring_System
{
    partial class frmManageRevalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcRevalida = new System.Windows.Forms.TabControl();
            this.tpNew = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPannelist = new System.Windows.Forms.RichTextBox();
            this.cbOnGoing = new System.Windows.Forms.CheckBox();
            this.dtpRevalidaDate = new System.Windows.Forms.DateTimePicker();
            this.txtVenue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOfficerInCharge = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tpActive = new System.Windows.Forms.TabPage();
            this.dgActive = new System.Windows.Forms.DataGridView();
            this.RevalidaId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaDate_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaPanelist_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaOfficerInCharge_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Venue_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Active = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpInActive = new System.Windows.Forms.TabPage();
            this.dgInactive = new System.Windows.Forms.DataGridView();
            this.RevalidaId_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaDate_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panelist_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OfficerInCharge_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Venue_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Inactive = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tcRevalida.SuspendLayout();
            this.tpNew.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).BeginInit();
            this.tpInActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcRevalida
            // 
            this.tcRevalida.Controls.Add(this.tpNew);
            this.tcRevalida.Controls.Add(this.tpActive);
            this.tcRevalida.Controls.Add(this.tpInActive);
            this.tcRevalida.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcRevalida.Location = new System.Drawing.Point(0, 33);
            this.tcRevalida.Name = "tcRevalida";
            this.tcRevalida.SelectedIndex = 0;
            this.tcRevalida.Size = new System.Drawing.Size(787, 294);
            this.tcRevalida.TabIndex = 13;
            this.tcRevalida.SelectedIndexChanged += new System.EventHandler(this.tcRevalida_SelectedIndexChanged);
            // 
            // tpNew
            // 
            this.tpNew.Controls.Add(this.groupBox1);
            this.tpNew.Location = new System.Drawing.Point(4, 22);
            this.tpNew.Name = "tpNew";
            this.tpNew.Padding = new System.Windows.Forms.Padding(3);
            this.tpNew.Size = new System.Drawing.Size(779, 268);
            this.tpNew.TabIndex = 0;
            this.tpNew.Text = "New";
            this.tpNew.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPannelist);
            this.groupBox1.Controls.Add(this.cbOnGoing);
            this.groupBox1.Controls.Add(this.dtpRevalidaDate);
            this.groupBox1.Controls.Add(this.txtVenue);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtOfficerInCharge);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(246, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 207);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Revalida Info";
            // 
            // txtPannelist
            // 
            this.txtPannelist.Location = new System.Drawing.Point(110, 84);
            this.txtPannelist.MaxLength = 50;
            this.txtPannelist.Name = "txtPannelist";
            this.txtPannelist.Size = new System.Drawing.Size(227, 62);
            this.txtPannelist.TabIndex = 20;
            this.txtPannelist.Text = "";
            // 
            // cbOnGoing
            // 
            this.cbOnGoing.AutoSize = true;
            this.cbOnGoing.Checked = true;
            this.cbOnGoing.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOnGoing.Location = new System.Drawing.Point(266, 178);
            this.cbOnGoing.Name = "cbOnGoing";
            this.cbOnGoing.Size = new System.Drawing.Size(71, 17);
            this.cbOnGoing.TabIndex = 19;
            this.cbOnGoing.Text = "On-Going";
            this.cbOnGoing.UseVisualStyleBackColor = true;
            // 
            // dtpRevalidaDate
            // 
            this.dtpRevalidaDate.Location = new System.Drawing.Point(110, 32);
            this.dtpRevalidaDate.Name = "dtpRevalidaDate";
            this.dtpRevalidaDate.Size = new System.Drawing.Size(227, 20);
            this.dtpRevalidaDate.TabIndex = 18;
            // 
            // txtVenue
            // 
            this.txtVenue.Location = new System.Drawing.Point(110, 152);
            this.txtVenue.MaxLength = 50;
            this.txtVenue.Name = "txtVenue";
            this.txtVenue.Size = new System.Drawing.Size(227, 20);
            this.txtVenue.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Pannelist:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(71, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Venue:";
            // 
            // txtOfficerInCharge
            // 
            this.txtOfficerInCharge.Location = new System.Drawing.Point(110, 58);
            this.txtOfficerInCharge.MaxLength = 50;
            this.txtOfficerInCharge.Name = "txtOfficerInCharge";
            this.txtOfficerInCharge.Size = new System.Drawing.Size(227, 20);
            this.txtOfficerInCharge.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Officer In-Charge:";
            // 
            // tpActive
            // 
            this.tpActive.Controls.Add(this.dgActive);
            this.tpActive.Location = new System.Drawing.Point(4, 22);
            this.tpActive.Name = "tpActive";
            this.tpActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpActive.Size = new System.Drawing.Size(779, 268);
            this.tpActive.TabIndex = 1;
            this.tpActive.Text = "In-Progress";
            this.tpActive.UseVisualStyleBackColor = true;
            // 
            // dgActive
            // 
            this.dgActive.AllowUserToAddRows = false;
            this.dgActive.AllowUserToDeleteRows = false;
            this.dgActive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgActive.BackgroundColor = System.Drawing.Color.White;
            this.dgActive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RevalidaId_Active,
            this.RevalidaDate_Active,
            this.RevalidaPanelist_Active,
            this.RevalidaOfficerInCharge_Active,
            this.Venue_Active,
            this.Edit_Active});
            this.dgActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgActive.Location = new System.Drawing.Point(3, 3);
            this.dgActive.Name = "dgActive";
            this.dgActive.ReadOnly = true;
            this.dgActive.RowHeadersVisible = false;
            this.dgActive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgActive.Size = new System.Drawing.Size(773, 262);
            this.dgActive.TabIndex = 0;
            this.dgActive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgActive_CellContentClick);
            // 
            // RevalidaId_Active
            // 
            this.RevalidaId_Active.DataPropertyName = "RevalidaId";
            this.RevalidaId_Active.HeaderText = "RevalidaId";
            this.RevalidaId_Active.Name = "RevalidaId_Active";
            this.RevalidaId_Active.ReadOnly = true;
            this.RevalidaId_Active.Visible = false;
            // 
            // RevalidaDate_Active
            // 
            this.RevalidaDate_Active.DataPropertyName = "RevalidaDate";
            this.RevalidaDate_Active.HeaderText = "RevalidaDate";
            this.RevalidaDate_Active.Name = "RevalidaDate_Active";
            this.RevalidaDate_Active.ReadOnly = true;
            // 
            // RevalidaPanelist_Active
            // 
            this.RevalidaPanelist_Active.DataPropertyName = "RevalidaPanelist";
            this.RevalidaPanelist_Active.HeaderText = "Panelist";
            this.RevalidaPanelist_Active.Name = "RevalidaPanelist_Active";
            this.RevalidaPanelist_Active.ReadOnly = true;
            // 
            // RevalidaOfficerInCharge_Active
            // 
            this.RevalidaOfficerInCharge_Active.DataPropertyName = "RevalidaOfficerInCharge";
            this.RevalidaOfficerInCharge_Active.HeaderText = "Officer In-Charge";
            this.RevalidaOfficerInCharge_Active.Name = "RevalidaOfficerInCharge_Active";
            this.RevalidaOfficerInCharge_Active.ReadOnly = true;
            // 
            // Venue_Active
            // 
            this.Venue_Active.DataPropertyName = "RevalidaVenue";
            this.Venue_Active.HeaderText = "Venue";
            this.Venue_Active.Name = "Venue_Active";
            this.Venue_Active.ReadOnly = true;
            // 
            // Edit_Active
            // 
            this.Edit_Active.HeaderText = "";
            this.Edit_Active.Name = "Edit_Active";
            this.Edit_Active.ReadOnly = true;
            this.Edit_Active.Text = "Edit";
            this.Edit_Active.UseColumnTextForButtonValue = true;
            // 
            // tpInActive
            // 
            this.tpInActive.Controls.Add(this.dgInactive);
            this.tpInActive.Location = new System.Drawing.Point(4, 22);
            this.tpInActive.Name = "tpInActive";
            this.tpInActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpInActive.Size = new System.Drawing.Size(779, 268);
            this.tpInActive.TabIndex = 2;
            this.tpInActive.Text = "Finished";
            this.tpInActive.UseVisualStyleBackColor = true;
            // 
            // dgInactive
            // 
            this.dgInactive.AllowUserToAddRows = false;
            this.dgInactive.AllowUserToDeleteRows = false;
            this.dgInactive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInactive.BackgroundColor = System.Drawing.Color.White;
            this.dgInactive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInactive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RevalidaId_Inactive,
            this.RevalidaDate_Inactive,
            this.Panelist_Inactive,
            this.OfficerInCharge_Inactive,
            this.Venue_Inactive,
            this.Edit_Inactive});
            this.dgInactive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInactive.Location = new System.Drawing.Point(3, 3);
            this.dgInactive.Name = "dgInactive";
            this.dgInactive.ReadOnly = true;
            this.dgInactive.RowHeadersVisible = false;
            this.dgInactive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInactive.Size = new System.Drawing.Size(773, 262);
            this.dgInactive.TabIndex = 1;
            this.dgInactive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInactive_CellContentClick);
            // 
            // RevalidaId_Inactive
            // 
            this.RevalidaId_Inactive.DataPropertyName = "RevalidaId";
            this.RevalidaId_Inactive.HeaderText = "RevalidaId";
            this.RevalidaId_Inactive.Name = "RevalidaId_Inactive";
            this.RevalidaId_Inactive.ReadOnly = true;
            this.RevalidaId_Inactive.Visible = false;
            // 
            // RevalidaDate_Inactive
            // 
            this.RevalidaDate_Inactive.DataPropertyName = "RevalidaDate";
            this.RevalidaDate_Inactive.HeaderText = "RevalidaDate";
            this.RevalidaDate_Inactive.Name = "RevalidaDate_Inactive";
            this.RevalidaDate_Inactive.ReadOnly = true;
            // 
            // Panelist_Inactive
            // 
            this.Panelist_Inactive.DataPropertyName = "RevalidaPanelist";
            this.Panelist_Inactive.HeaderText = "Panelist";
            this.Panelist_Inactive.Name = "Panelist_Inactive";
            this.Panelist_Inactive.ReadOnly = true;
            // 
            // OfficerInCharge_Inactive
            // 
            this.OfficerInCharge_Inactive.DataPropertyName = "RevalidaOfficerInCharge";
            this.OfficerInCharge_Inactive.HeaderText = "Officer In-Charge";
            this.OfficerInCharge_Inactive.Name = "OfficerInCharge_Inactive";
            this.OfficerInCharge_Inactive.ReadOnly = true;
            // 
            // Venue_Inactive
            // 
            this.Venue_Inactive.DataPropertyName = "RevalidaVenue";
            this.Venue_Inactive.HeaderText = "Venue";
            this.Venue_Inactive.Name = "Venue_Inactive";
            this.Venue_Inactive.ReadOnly = true;
            // 
            // Edit_Inactive
            // 
            this.Edit_Inactive.HeaderText = "";
            this.Edit_Inactive.Name = "Edit_Inactive";
            this.Edit_Inactive.ReadOnly = true;
            this.Edit_Inactive.Text = "Edit";
            this.Edit_Inactive.UseColumnTextForButtonValue = true;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(619, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 327);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(787, 28);
            this.panel2.TabIndex = 12;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(700, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(787, 33);
            this.label1.TabIndex = 11;
            this.label1.Text = "Manage Revalida";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmManageRevalida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(787, 355);
            this.Controls.Add(this.tcRevalida);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Name = "frmManageRevalida";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tcRevalida.ResumeLayout(false);
            this.tpNew.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).EndInit();
            this.tpInActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcRevalida;
        private System.Windows.Forms.TabPage tpNew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbOnGoing;
        private System.Windows.Forms.DateTimePicker dtpRevalidaDate;
        private System.Windows.Forms.TextBox txtVenue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOfficerInCharge;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tpActive;
        private System.Windows.Forms.DataGridView dgActive;
        private System.Windows.Forms.TabPage tpInActive;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtPannelist;
        private System.Windows.Forms.DataGridView dgInactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaDate_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaPanelist_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaOfficerInCharge_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Venue_Active;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaId_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaDate_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Panelist_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn OfficerInCharge_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Venue_Inactive;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Inactive;
    }
}