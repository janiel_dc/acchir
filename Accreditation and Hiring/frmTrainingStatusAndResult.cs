﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accreditation_And_Hiring
{
    public partial class frmTrainingStatusAndResult : Form
    {
        public frmTrainingStatusAndResult()
        {
            InitializeComponent();
        }

        private void frmTrainingStatusAndResult_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'AccreditationHiringDataSet.DAL_GET_TrainingStatusAndResult' table. You can move, or remove it, as needed.
            this.DAL_GET_TrainingStatusAndResultTableAdapter.Fill(this.AccreditationHiringDataSet.DAL_GET_TrainingStatusAndResult);

            this.reportViewer1.RefreshReport();
        }
    }
}
