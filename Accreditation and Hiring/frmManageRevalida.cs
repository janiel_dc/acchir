﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManageRevalida : Form
    {
        private int _revalidaId = 0;

        public frmManageRevalida()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tcRevalida.SelectedIndex != 2 && _revalidaId > 0)
            {
                if (txtOfficerInCharge.Text != "" && txtPannelist.Text != "" && txtVenue.Text != "")
                {
                    updateTraining();

                    if (cbOnGoing.Checked)
                        tcRevalida.SelectedIndex = 1;
                    else if (!cbOnGoing.Checked)
                        tcRevalida.SelectedIndex = 2;
                    else
                        tcRevalida.SelectedIndex = 0;

                    clear();
                }
                else
                {
                    MessageBox.Show("Please compete information");
                }
            }
            else if (tcRevalida.SelectedIndex != 2 && _revalidaId == 0)
            {
                if (txtOfficerInCharge.Text != "" && txtPannelist.Text != "" && txtVenue.Text != "")
                {
                    savingRevalida();

                    if (cbOnGoing.Checked)
                        tcRevalida.SelectedIndex = 1;
                    else if (!cbOnGoing.Checked)
                        tcRevalida.SelectedIndex = 2;
                    else
                        tcRevalida.SelectedIndex = 0;

                    clear();
                }
                else
                {
                    MessageBox.Show("Please compete information");
                }
            }
            else if (tcRevalida.SelectedIndex == 2)
            {
                deleteTraining();
            }
        }

        private void deleteTraining()
        {
            RevalidaBL revalidaBL = new RevalidaBL();
            frmLogin login = frmLogin.GetInstance();

            string ans = MessageBox.Show("Are you sure you want to delete this revalida?", "Delete Revalida", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                try
                {
                    int rowIndex = dgInactive.CurrentCell.RowIndex;
                    revalidaBL._revalidaId = Convert.ToInt32(dgInactive.Rows[rowIndex].Cells["RevalidaId_Inactive"].Value);
                    revalidaBL._lastModifiedUserId = login._loggedInUser._userId;
                    RevalidaDAL.deleteRevalida(revalidaBL);
                    MessageBox.Show("Revalida deleted successfully");

                    DataSet dsRevaldiaList = RevalidaDAL.getRevalidaList();
                    dgInactive.AutoGenerateColumns = false;
                    dgInactive.DataSource = dsRevaldiaList.Tables[1];

                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured, please contact your Administrator." + ex.ToString());
                }
            }
        }

        private void updateTraining()
        {
            RevalidaBL revalidaBL = new RevalidaBL();
            frmLogin login = frmLogin.GetInstance();

            try
            {
                revalidaBL._revalidaId = _revalidaId;
                revalidaBL._revalidaDate = dtpRevalidaDate.Value.ToString();
                revalidaBL._revalidaPanelist = txtPannelist.Text.Trim();
                revalidaBL._revalidaOfficerInCharge = txtOfficerInCharge.Text.Trim();
                revalidaBL._revalidaVenue = txtVenue.Text.Trim();
                revalidaBL._lastModifiedUserId = login._loggedInUser._userId;

                if (cbOnGoing.Checked)
                    revalidaBL._isActive = true;
                else
                    revalidaBL._isActive = false;

                RevalidaDAL.updateRevalidaDetails(revalidaBL);
                MessageBox.Show("Revalida successfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator" + ex.ToString());
                throw;
            }
        }

        private void savingRevalida()
        {
            RevalidaBL revalidaBL = new RevalidaBL();
            frmLogin login = frmLogin.GetInstance();

            try
            {
                revalidaBL._revalidaDate = dtpRevalidaDate.Value.ToString();
                revalidaBL._revalidaPanelist = txtPannelist.Text.Trim();
                revalidaBL._revalidaOfficerInCharge = txtOfficerInCharge.Text.Trim();
                revalidaBL._revalidaVenue = txtVenue.Text.Trim();
                revalidaBL._createdUserId = login._loggedInUser._userId;
                revalidaBL._lastModifiedUserId = login._loggedInUser._userId;

                if (cbOnGoing.Checked)
                    revalidaBL._isActive = true;
                else
                    revalidaBL._isActive = false;

                RevalidaDAL.insertRevalida(revalidaBL);
                MessageBox.Show("Revalida successfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator" + ex.ToString());
                throw;
            }
        }

        #region HELPERS

        private void clear()
        {
            tcRevalida.TabPages[0].Text = "New";

            _revalidaId = 0;
            dtpRevalidaDate.Value = DateTime.Now;
            txtVenue.Text = "";
            txtPannelist.Text = "";
            txtOfficerInCharge.Text = "";
            cbOnGoing.Checked = true;
        }

        #endregion

        private void tcRevalida_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcRevalida.SelectedIndex == 1)
            {
                DataSet dsRevalidaList = RevalidaDAL.getRevalidaList();
                dgActive.AutoGenerateColumns = false;
                dgActive.DataSource = dsRevalidaList.Tables[0];

                btnSave.Visible = false;
                clear();
            }
            else if (tcRevalida.SelectedIndex == 2)
            {
                DataSet dsRevalidaList = RevalidaDAL.getRevalidaList();
                dgInactive.AutoGenerateColumns = false;
                dgInactive.DataSource = dsRevalidaList.Tables[1];

                btnSave.Visible = true;
                btnSave.Text = "Delete";
                btnSave.BackColor = Color.Red;

                clear();
            }
            else if (tcRevalida.SelectedIndex == 0)
            {
                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnSave.BackColor = Color.Green;
            }
        }

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgActive.Rows[e.RowIndex].Cells["Edit_Active"] == dgActive.CurrentCell)
                {
                    RevalidaBL revalidaBL = new RevalidaBL();
                    revalidaBL._revalidaId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["RevalidaId_Active"].Value);

                    DataSet dsGetRevalidaDetails = RevalidaDAL.getRevalidaDetailsById(revalidaBL);

                    if (dsGetRevalidaDetails.Tables[0].Rows.Count > 0)
                    {
                        //Revalida info
                        _revalidaId = Convert.ToInt32(dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaId"]);
                        dtpRevalidaDate.Value = Convert.ToDateTime(dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaDate"]);
                        txtOfficerInCharge.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaOfficerInCharge"].ToString();
                        txtPannelist.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaPanelist"].ToString();
                        txtVenue.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaVenue"].ToString();
                        
                        if (_revalidaId > 0)
                        {
                            tcRevalida.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcRevalida.TabPages[0].Text = "New";
                        }
                        cbOnGoing.Checked = true;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find revalida.");
                    }

                    tcRevalida.SelectedIndex = 0;
                }
            }
        }

        private void dgInactive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgInactive.Rows[e.RowIndex].Cells["Edit_Inactive"] == dgInactive.CurrentCell)
                {
                    RevalidaBL revalidaBL = new RevalidaBL();
                    revalidaBL._revalidaId = Convert.ToInt32(dgInactive.Rows[e.RowIndex].Cells["RevalidaId_Inactive"].Value);

                    DataSet dsGetRevalidaDetails = RevalidaDAL.getRevalidaDetailsById(revalidaBL);

                    if (dsGetRevalidaDetails.Tables[0].Rows.Count > 0)
                    {
                        //Revalida info
                        _revalidaId = Convert.ToInt32(dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaId"]);
                        dtpRevalidaDate.Value = Convert.ToDateTime(dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaDate"]);
                        txtOfficerInCharge.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaOfficerInCharge"].ToString();
                        txtPannelist.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaPanelist"].ToString();
                        txtVenue.Text = dsGetRevalidaDetails.Tables[0].Rows[0]["RevalidaVenue"].ToString();

                        if (_revalidaId > 0)
                        {
                            tcRevalida.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcRevalida.TabPages[0].Text = "New";
                        }
                        cbOnGoing.Checked = false;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find revalida.");
                    }

                    tcRevalida.SelectedIndex = 0;
                }
            }
        }
    }
}
