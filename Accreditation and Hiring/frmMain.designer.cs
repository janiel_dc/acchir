﻿namespace Accreditation_and_Hiring_System
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statLoggedIn = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.msHome = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageRolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.organicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageAdminsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hiringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.organicToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revalidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resultToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supportFundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appraisalFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfApplicantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfApplicantsProcessedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfBrokersAndInhouseSalesTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationForOrganicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationForSellersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allowanceReleasePerSellerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeOfEmploymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateHiredToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateAccreditedByDeveloperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationOfEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.msHome.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.statLoggedIn});
            this.statusStrip.Location = new System.Drawing.Point(0, 493);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(733, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(63, 17);
            this.toolStripStatusLabel.Text = "Logged In:";
            // 
            // statLoggedIn
            // 
            this.statLoggedIn.Name = "statLoggedIn";
            this.statLoggedIn.Size = new System.Drawing.Size(30, 17);
            this.statLoggedIn.Text = "User";
            // 
            // msHome
            // 
            this.msHome.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.msHome.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.applicationToolStripMenuItem,
            this.hiringToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.reportsToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.msHome.Location = new System.Drawing.Point(0, 0);
            this.msHome.Name = "msHome";
            this.msHome.Size = new System.Drawing.Size(733, 24);
            this.msHome.TabIndex = 4;
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dashboardToolStripMenuItem,
            this.accountSettingsToolStripMenuItem,
            this.manageRolesToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.settingsToolStripMenuItem.Text = "General";
            // 
            // dashboardToolStripMenuItem
            // 
            this.dashboardToolStripMenuItem.Name = "dashboardToolStripMenuItem";
            this.dashboardToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.dashboardToolStripMenuItem.Text = "Dashboard";
            this.dashboardToolStripMenuItem.Click += new System.EventHandler(this.dashboardToolStripMenuItem_Click);
            // 
            // accountSettingsToolStripMenuItem
            // 
            this.accountSettingsToolStripMenuItem.Name = "accountSettingsToolStripMenuItem";
            this.accountSettingsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.accountSettingsToolStripMenuItem.Text = "Account Settings";
            this.accountSettingsToolStripMenuItem.Click += new System.EventHandler(this.accountSettingsToolStripMenuItem_Click);
            // 
            // manageRolesToolStripMenuItem
            // 
            this.manageRolesToolStripMenuItem.Name = "manageRolesToolStripMenuItem";
            this.manageRolesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.manageRolesToolStripMenuItem.Text = "Manage Roles";
            this.manageRolesToolStripMenuItem.Click += new System.EventHandler(this.manageRolesToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // applicationToolStripMenuItem
            // 
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesToolStripMenuItem,
            this.organicToolStripMenuItem,
            this.manageAdminsToolStripMenuItem});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            this.applicationToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.applicationToolStripMenuItem.Text = "Members";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.salesToolStripMenuItem.Text = "Manage Sales Personnel";
            this.salesToolStripMenuItem.Click += new System.EventHandler(this.salesToolStripMenuItem_Click);
            // 
            // organicToolStripMenuItem
            // 
            this.organicToolStripMenuItem.Name = "organicToolStripMenuItem";
            this.organicToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.organicToolStripMenuItem.Text = "Manage Organic";
            this.organicToolStripMenuItem.Click += new System.EventHandler(this.organicToolStripMenuItem_Click);
            // 
            // manageAdminsToolStripMenuItem
            // 
            this.manageAdminsToolStripMenuItem.Name = "manageAdminsToolStripMenuItem";
            this.manageAdminsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.manageAdminsToolStripMenuItem.Text = "Manage Admins";
            this.manageAdminsToolStripMenuItem.Click += new System.EventHandler(this.manageAdminsToolStripMenuItem_Click);
            // 
            // hiringToolStripMenuItem
            // 
            this.hiringToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentsToolStripMenuItem,
            this.trainingsToolStripMenuItem,
            this.revalidaToolStripMenuItem});
            this.hiringToolStripMenuItem.Name = "hiringToolStripMenuItem";
            this.hiringToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.hiringToolStripMenuItem.Text = "Requirements";
            // 
            // documentsToolStripMenuItem
            // 
            this.documentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesToolStripMenuItem2,
            this.organicToolStripMenuItem2});
            this.documentsToolStripMenuItem.Name = "documentsToolStripMenuItem";
            this.documentsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.documentsToolStripMenuItem.Text = "Documents";
            // 
            // salesToolStripMenuItem2
            // 
            this.salesToolStripMenuItem2.Name = "salesToolStripMenuItem2";
            this.salesToolStripMenuItem2.Size = new System.Drawing.Size(116, 22);
            this.salesToolStripMenuItem2.Text = "Sales";
            this.salesToolStripMenuItem2.Click += new System.EventHandler(this.salesToolStripMenuItem2_Click);
            // 
            // organicToolStripMenuItem2
            // 
            this.organicToolStripMenuItem2.Name = "organicToolStripMenuItem2";
            this.organicToolStripMenuItem2.Size = new System.Drawing.Size(116, 22);
            this.organicToolStripMenuItem2.Text = "Organic";
            this.organicToolStripMenuItem2.Click += new System.EventHandler(this.organicToolStripMenuItem2_Click);
            // 
            // trainingsToolStripMenuItem
            // 
            this.trainingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageToolStripMenuItem,
            this.resultToolStripMenuItem});
            this.trainingsToolStripMenuItem.Name = "trainingsToolStripMenuItem";
            this.trainingsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.trainingsToolStripMenuItem.Text = "Trainings";
            // 
            // manageToolStripMenuItem
            // 
            this.manageToolStripMenuItem.Name = "manageToolStripMenuItem";
            this.manageToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.manageToolStripMenuItem.Text = "Manage";
            this.manageToolStripMenuItem.Click += new System.EventHandler(this.manageToolStripMenuItem_Click);
            // 
            // resultToolStripMenuItem
            // 
            this.resultToolStripMenuItem.Name = "resultToolStripMenuItem";
            this.resultToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.resultToolStripMenuItem.Text = "Result";
            this.resultToolStripMenuItem.Click += new System.EventHandler(this.resultToolStripMenuItem_Click);
            // 
            // revalidaToolStripMenuItem
            // 
            this.revalidaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageToolStripMenuItem1,
            this.resultToolStripMenuItem1});
            this.revalidaToolStripMenuItem.Name = "revalidaToolStripMenuItem";
            this.revalidaToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.revalidaToolStripMenuItem.Text = "Revalida";
            // 
            // manageToolStripMenuItem1
            // 
            this.manageToolStripMenuItem1.Name = "manageToolStripMenuItem1";
            this.manageToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.manageToolStripMenuItem1.Text = "Manage";
            this.manageToolStripMenuItem1.Click += new System.EventHandler(this.manageToolStripMenuItem1_Click);
            // 
            // resultToolStripMenuItem1
            // 
            this.resultToolStripMenuItem1.Name = "resultToolStripMenuItem1";
            this.resultToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.resultToolStripMenuItem1.Text = "Result";
            this.resultToolStripMenuItem1.Click += new System.EventHandler(this.resultToolStripMenuItem1_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supportFundToolStripMenuItem,
            this.appraisalFormToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Signing";
            this.reportsToolStripMenuItem.Visible = false;
            // 
            // supportFundToolStripMenuItem
            // 
            this.supportFundToolStripMenuItem.Name = "supportFundToolStripMenuItem";
            this.supportFundToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.supportFundToolStripMenuItem.Text = "Support Fund";
            // 
            // appraisalFormToolStripMenuItem
            // 
            this.appraisalFormToolStripMenuItem.Name = "appraisalFormToolStripMenuItem";
            this.appraisalFormToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.appraisalFormToolStripMenuItem.Text = "Appraisal Form";
            // 
            // reportsToolStripMenuItem1
            // 
            this.reportsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numberOfApplicantsToolStripMenuItem,
            this.applicantStatusToolStripMenuItem,
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem,
            this.trainingToolStripMenuItem,
            this.numberOfApplicantsProcessedToolStripMenuItem,
            this.numberOfBrokersAndInhouseSalesTeamToolStripMenuItem,
            this.evaluationForOrganicToolStripMenuItem,
            this.evaluationForSellersToolStripMenuItem,
            this.allowanceReleasePerSellerToolStripMenuItem,
            this.activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem,
            this.typeOfEmploymentToolStripMenuItem,
            this.dateHiredToolStripMenuItem,
            this.dateAccreditedByDeveloperToolStripMenuItem,
            this.evaluationOfEmployeeToolStripMenuItem});
            this.reportsToolStripMenuItem1.Name = "reportsToolStripMenuItem1";
            this.reportsToolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem1.Text = "Reports";
            // 
            // numberOfApplicantsToolStripMenuItem
            // 
            this.numberOfApplicantsToolStripMenuItem.Name = "numberOfApplicantsToolStripMenuItem";
            this.numberOfApplicantsToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.numberOfApplicantsToolStripMenuItem.Text = "Number of Applicants By Month";
            this.numberOfApplicantsToolStripMenuItem.Click += new System.EventHandler(this.numberOfApplicantsToolStripMenuItem_Click);
            // 
            // applicantStatusToolStripMenuItem
            // 
            this.applicantStatusToolStripMenuItem.Name = "applicantStatusToolStripMenuItem";
            this.applicantStatusToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.applicantStatusToolStripMenuItem.Text = "Applicant Status";
            this.applicantStatusToolStripMenuItem.Click += new System.EventHandler(this.applicantStatusToolStripMenuItem_Click);
            // 
            // numberOfMarketingServicesAgreementGeneratedToolStripMenuItem
            // 
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem.Name = "numberOfMarketingServicesAgreementGeneratedToolStripMenuItem";
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem.Text = "Number of Marketing Services Agreement Generated";
            this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem.Click += new System.EventHandler(this.numberOfMarketingServicesAgreementGeneratedToolStripMenuItem_Click);
            // 
            // trainingToolStripMenuItem
            // 
            this.trainingToolStripMenuItem.Name = "trainingToolStripMenuItem";
            this.trainingToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.trainingToolStripMenuItem.Text = "Training Status and Result";
            this.trainingToolStripMenuItem.Click += new System.EventHandler(this.trainingToolStripMenuItem_Click);
            // 
            // numberOfApplicantsProcessedToolStripMenuItem
            // 
            this.numberOfApplicantsProcessedToolStripMenuItem.Name = "numberOfApplicantsProcessedToolStripMenuItem";
            this.numberOfApplicantsProcessedToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.numberOfApplicantsProcessedToolStripMenuItem.Text = "Number of Applicants Processed";
            // 
            // numberOfBrokersAndInhouseSalesTeamToolStripMenuItem
            // 
            this.numberOfBrokersAndInhouseSalesTeamToolStripMenuItem.Name = "numberOfBrokersAndInhouseSalesTeamToolStripMenuItem";
            this.numberOfBrokersAndInhouseSalesTeamToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.numberOfBrokersAndInhouseSalesTeamToolStripMenuItem.Text = "Number of Brokers and In-house sales team";
            // 
            // evaluationForOrganicToolStripMenuItem
            // 
            this.evaluationForOrganicToolStripMenuItem.Name = "evaluationForOrganicToolStripMenuItem";
            this.evaluationForOrganicToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.evaluationForOrganicToolStripMenuItem.Text = "Evaluation for organic";
            // 
            // evaluationForSellersToolStripMenuItem
            // 
            this.evaluationForSellersToolStripMenuItem.Name = "evaluationForSellersToolStripMenuItem";
            this.evaluationForSellersToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.evaluationForSellersToolStripMenuItem.Text = "Evaluation for Sellers";
            // 
            // allowanceReleasePerSellerToolStripMenuItem
            // 
            this.allowanceReleasePerSellerToolStripMenuItem.Name = "allowanceReleasePerSellerToolStripMenuItem";
            this.allowanceReleasePerSellerToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.allowanceReleasePerSellerToolStripMenuItem.Text = "Allowance release per seller";
            // 
            // activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem
            // 
            this.activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem.Name = "activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem";
            this.activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem.Text = "Activation Report - Date of First sale made from date hired";
            // 
            // typeOfEmploymentToolStripMenuItem
            // 
            this.typeOfEmploymentToolStripMenuItem.Name = "typeOfEmploymentToolStripMenuItem";
            this.typeOfEmploymentToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.typeOfEmploymentToolStripMenuItem.Text = "Type of Employment";
            // 
            // dateHiredToolStripMenuItem
            // 
            this.dateHiredToolStripMenuItem.Name = "dateHiredToolStripMenuItem";
            this.dateHiredToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.dateHiredToolStripMenuItem.Text = "Date hired";
            // 
            // dateAccreditedByDeveloperToolStripMenuItem
            // 
            this.dateAccreditedByDeveloperToolStripMenuItem.Name = "dateAccreditedByDeveloperToolStripMenuItem";
            this.dateAccreditedByDeveloperToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.dateAccreditedByDeveloperToolStripMenuItem.Text = "Date Accredited by developer";
            // 
            // evaluationOfEmployeeToolStripMenuItem
            // 
            this.evaluationOfEmployeeToolStripMenuItem.Name = "evaluationOfEmployeeToolStripMenuItem";
            this.evaluationOfEmployeeToolStripMenuItem.Size = new System.Drawing.Size(381, 22);
            this.evaluationOfEmployeeToolStripMenuItem.Text = "Evaluation of Employee";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(733, 515);
            this.Controls.Add(this.msHome);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.msHome.ResumeLayout(false);
            this.msHome.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripStatusLabel statLoggedIn;
        private System.Windows.Forms.MenuStrip msHome;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem organicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageAdminsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hiringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revalidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resultToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supportFundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem appraisalFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem documentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem organicToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem manageRolesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numberOfApplicantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numberOfMarketingServicesAgreementGeneratedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numberOfApplicantsProcessedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numberOfBrokersAndInhouseSalesTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationForOrganicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationForSellersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allowanceReleasePerSellerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activationReportDateOfFirstSaleMadeFromDateHiredToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeOfEmploymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateHiredToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateAccreditedByDeveloperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationOfEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}



