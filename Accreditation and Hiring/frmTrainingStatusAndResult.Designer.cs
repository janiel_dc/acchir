﻿namespace Accreditation_And_Hiring
{
    partial class frmTrainingStatusAndResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AccreditationHiringDataSet = new Accreditation_And_Hiring.AccreditationHiringDataSet();
            this.DAL_GET_TrainingStatusAndResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DAL_GET_TrainingStatusAndResultTableAdapter = new Accreditation_And_Hiring.AccreditationHiringDataSetTableAdapters.DAL_GET_TrainingStatusAndResultTableAdapter();
            this.dALGETTrainingStatusAndResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AccreditationHiringDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DAL_GET_TrainingStatusAndResultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETTrainingStatusAndResultBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsTrainingStatus";
            reportDataSource1.Value = this.dALGETTrainingStatusAndResultBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Accreditation_And_Hiring.Reports.rptTrainingStatusAndResults.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(808, 449);
            this.reportViewer1.TabIndex = 0;
            // 
            // AccreditationHiringDataSet
            // 
            this.AccreditationHiringDataSet.DataSetName = "AccreditationHiringDataSet";
            this.AccreditationHiringDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DAL_GET_TrainingStatusAndResultBindingSource
            // 
            this.DAL_GET_TrainingStatusAndResultBindingSource.DataMember = "DAL_GET_TrainingStatusAndResult";
            this.DAL_GET_TrainingStatusAndResultBindingSource.DataSource = this.AccreditationHiringDataSet;
            // 
            // DAL_GET_TrainingStatusAndResultTableAdapter
            // 
            this.DAL_GET_TrainingStatusAndResultTableAdapter.ClearBeforeFill = true;
            // 
            // dALGETTrainingStatusAndResultBindingSource
            // 
            this.dALGETTrainingStatusAndResultBindingSource.DataMember = "DAL_GET_TrainingStatusAndResult";
            this.dALGETTrainingStatusAndResultBindingSource.DataSource = this.AccreditationHiringDataSet;
            // 
            // frmTrainingStatusAndResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 449);
            this.Controls.Add(this.reportViewer1);
            this.Name = "frmTrainingStatusAndResult";
            this.Load += new System.EventHandler(this.frmTrainingStatusAndResult_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AccreditationHiringDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DAL_GET_TrainingStatusAndResultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETTrainingStatusAndResultBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource DAL_GET_TrainingStatusAndResultBindingSource;
        private AccreditationHiringDataSet AccreditationHiringDataSet;
        private AccreditationHiringDataSetTableAdapters.DAL_GET_TrainingStatusAndResultTableAdapter DAL_GET_TrainingStatusAndResultTableAdapter;
        private System.Windows.Forms.BindingSource dALGETTrainingStatusAndResultBindingSource;
    }
}