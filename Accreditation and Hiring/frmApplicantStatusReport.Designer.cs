﻿namespace Accreditation_And_Hiring
{
    partial class frmApplicantStatusReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbApplicantType = new System.Windows.Forms.ComboBox();
            this.dataSetApplicantStatus = new Accreditation_And_Hiring.DataSource.DataSetApplicantStatus();
            this.dALGETApplicantStatusByAppplicantTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAL_GET_ApplicantStatusByAppplicantTypeTableAdapter = new Accreditation_And_Hiring.DataSource.DataSetApplicantStatusTableAdapters.DAL_GET_ApplicantStatusByAppplicantTypeTableAdapter();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetApplicantStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETApplicantStatusByAppplicantTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbApplicantType);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(661, 45);
            this.panel1.TabIndex = 0;
            // 
            // cmbApplicantType
            // 
            this.cmbApplicantType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbApplicantType.FormattingEnabled = true;
            this.cmbApplicantType.Items.AddRange(new object[] {
            "Sales",
            "Organic"});
            this.cmbApplicantType.Location = new System.Drawing.Point(12, 12);
            this.cmbApplicantType.Name = "cmbApplicantType";
            this.cmbApplicantType.Size = new System.Drawing.Size(237, 21);
            this.cmbApplicantType.TabIndex = 1;
            this.cmbApplicantType.SelectionChangeCommitted += new System.EventHandler(this.cmbApplicantType_SelectionChangeCommitted);
            this.cmbApplicantType.SelectedValueChanged += new System.EventHandler(this.cmbApplicantType_SelectedValueChanged);
            // 
            // dataSetApplicantStatus
            // 
            this.dataSetApplicantStatus.DataSetName = "DataSetApplicantStatus";
            this.dataSetApplicantStatus.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dALGETApplicantStatusByAppplicantTypeBindingSource
            // 
            this.dALGETApplicantStatusByAppplicantTypeBindingSource.DataMember = "DAL_GET_ApplicantStatusByAppplicantType";
            this.dALGETApplicantStatusByAppplicantTypeBindingSource.DataSource = this.dataSetApplicantStatus;
            // 
            // dAL_GET_ApplicantStatusByAppplicantTypeTableAdapter
            // 
            this.dAL_GET_ApplicantStatusByAppplicantTypeTableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsApplicantStatus";
            reportDataSource1.Value = this.dALGETApplicantStatusByAppplicantTypeBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Accreditation_And_Hiring.Reports.ApplicantStatusByMembertypeId.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 45);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(661, 321);
            this.reportViewer1.TabIndex = 1;
            // 
            // frmApplicantStatusReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 366);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.panel1);
            this.Name = "frmApplicantStatusReport";
            this.Load += new System.EventHandler(this.frmApplicantStatusReport_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetApplicantStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETApplicantStatusByAppplicantTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbApplicantType;
        private System.Windows.Forms.BindingSource dALGETApplicantStatusByAppplicantTypeBindingSource;
        private DataSource.DataSetApplicantStatus dataSetApplicantStatus;
        private DataSource.DataSetApplicantStatusTableAdapters.DAL_GET_ApplicantStatusByAppplicantTypeTableAdapter dAL_GET_ApplicantStatusByAppplicantTypeTableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}