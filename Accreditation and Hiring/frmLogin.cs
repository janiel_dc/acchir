﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;
using BusinessObjects;

namespace Accreditation_and_Hiring_System
{
    public partial class frmLogin : Form
    {
        #region MEMBERS
        private static frmLogin _instance = null;
        private UserBL _usersBL = null;
        public UserBL _loggedInUser = null;

        public string _cQuestion = "", _cUsername = "", _cAnswer = "";
        public int _cUserID = 0;


        #endregion

        public static frmLogin GetInstance()
        {
            if (_instance == null)
            {
                _instance = new frmLogin();
            }
            return _instance;
        }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            logIn();
        }

        private void logIn()
        {
            DataSet ds = null;
            DataRow dr = null;

            if (txtUserName.Text != "" && txtPassword.Text != "")
            {
                _usersBL = new UserBL();

                _usersBL._userName = txtUserName.Text;
                _usersBL._password = txtPassword.Text;

                    ds = UserDAL.userAuthentication(_usersBL);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    dr = ds.Tables[0].Rows[0];
                    _loggedInUser = new UserBL();
                    _loggedInUser._userId = Convert.ToInt32(dr["UserId"]);
                    _loggedInUser._firstName = dr["FirstName"].ToString();
                    _loggedInUser._userTypeId = Convert.ToInt32(dr["UserTypeId"]);
                    this.DialogResult = DialogResult.Yes;
                }
                else
                {
                    MessageBox.Show("Invalid username and/or password.");
                }
            }
            else
            {
                MessageBox.Show("Invalid username and/or password.");
            }
        }

        private void lnkForgotPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtUserName.Text != "")
            {
                try
                {
                    DataSet dsQuestionAndAnswer = UserDAL.getSecretQuestion(txtUserName.Text);


                    _cUsername = txtUserName.Text;
                    _cQuestion = dsQuestionAndAnswer.Tables[0].Rows[0]["ChallengeQuestion"].ToString();
                    _cAnswer = dsQuestionAndAnswer.Tables[0].Rows[0]["ChallengeAnswer"].ToString();
                    _cUserID = Convert.ToInt32(dsQuestionAndAnswer.Tables[0].Rows[0]["UserId"]);
                    frmForgotPassword frmForgot = new frmForgotPassword();
                    frmForgot.ShowDialog();
                }
                catch (Exception)// ex)
                {
                    //log.Error(ex);
                    MessageBox.Show("Invalid username.");
                }
            }
            else MessageBox.Show("Please enter your username.");
        }

        private void txtUserName_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //TextBox tb = new TextBox();
            //tb.KeyDown += new KeyEventHandler(txtUserName_KeyUp);

            if (e.KeyChar == 13)
            {
                logIn();
            }
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            //TextBox tb = new TextBox();
            //tb.KeyDown += new KeyEventHandler(txtPassword_KeyPress);

            if (e.KeyChar == 13)
            {
                logIn();
            }
        }
    }
}
