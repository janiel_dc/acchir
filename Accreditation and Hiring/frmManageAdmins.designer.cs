﻿namespace Accreditation_and_Hiring_System
{
    partial class frmManageAdmins
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.tcManageAdmins = new System.Windows.Forms.TabControl();
            this.tpNew = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtChallengeAnswer = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtChallengeQuestion = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.dtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.rbtnFemale = new System.Windows.Forms.RadioButton();
            this.rbtnMale = new System.Windows.Forms.RadioButton();
            this.txtEmailAdd = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtContact2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContact1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMiddleName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tpActive = new System.Windows.Forms.TabPage();
            this.dgActive = new System.Windows.Forms.DataGridView();
            this.UserId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactNo_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Active = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpInActive = new System.Windows.Forms.TabPage();
            this.dgInactive = new System.Windows.Forms.DataGridView();
            this.UserId_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name_InActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address_InActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactNo_InActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_InActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_InActive = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1.SuspendLayout();
            this.tcManageAdmins.SuspendLayout();
            this.tpNew.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).BeginInit();
            this.tpInActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(516, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(686, 33);
            this.label1.TabIndex = 6;
            this.label1.Text = "Manage Admins";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 382);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(686, 28);
            this.panel1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(599, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tcManageAdmins
            // 
            this.tcManageAdmins.Controls.Add(this.tpNew);
            this.tcManageAdmins.Controls.Add(this.tpActive);
            this.tcManageAdmins.Controls.Add(this.tpInActive);
            this.tcManageAdmins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcManageAdmins.Location = new System.Drawing.Point(0, 33);
            this.tcManageAdmins.Name = "tcManageAdmins";
            this.tcManageAdmins.SelectedIndex = 0;
            this.tcManageAdmins.Size = new System.Drawing.Size(686, 349);
            this.tcManageAdmins.TabIndex = 8;
            this.tcManageAdmins.SelectedIndexChanged += new System.EventHandler(this.tcManageAdmins_SelectedIndexChanged);
            // 
            // tpNew
            // 
            this.tpNew.Controls.Add(this.groupBox2);
            this.tpNew.Controls.Add(this.groupBox1);
            this.tpNew.Location = new System.Drawing.Point(4, 22);
            this.tpNew.Name = "tpNew";
            this.tpNew.Padding = new System.Windows.Forms.Padding(3);
            this.tpNew.Size = new System.Drawing.Size(678, 323);
            this.tpNew.TabIndex = 0;
            this.tpNew.Text = "New";
            this.tpNew.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtChallengeAnswer);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtChallengeQuestion);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtPassword);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtUserName);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(348, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(303, 140);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Login Credentials";
            // 
            // txtChallengeAnswer
            // 
            this.txtChallengeAnswer.Location = new System.Drawing.Point(115, 99);
            this.txtChallengeAnswer.MaxLength = 50;
            this.txtChallengeAnswer.Name = "txtChallengeAnswer";
            this.txtChallengeAnswer.Size = new System.Drawing.Size(182, 20);
            this.txtChallengeAnswer.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Challenge Answer:";
            // 
            // txtChallengeQuestion
            // 
            this.txtChallengeQuestion.Location = new System.Drawing.Point(115, 76);
            this.txtChallengeQuestion.MaxLength = 50;
            this.txtChallengeQuestion.Name = "txtChallengeQuestion";
            this.txtChallengeQuestion.Size = new System.Drawing.Size(182, 20);
            this.txtChallengeQuestion.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 79);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Challenge Question:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(115, 50);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(182, 20);
            this.txtPassword.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(53, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Password:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(115, 24);
            this.txtUserName.MaxLength = 50;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(182, 20);
            this.txtUserName.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(51, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Username:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbActive);
            this.groupBox1.Controls.Add(this.dtpBirthDate);
            this.groupBox1.Controls.Add(this.rbtnFemale);
            this.groupBox1.Controls.Add(this.rbtnMale);
            this.groupBox1.Controls.Add(this.txtEmailAdd);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtContact2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtContact1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtAddress2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtAddress1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtMiddleName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 304);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Information";
            // 
            // cbActive
            // 
            this.cbActive.AutoSize = true;
            this.cbActive.Checked = true;
            this.cbActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbActive.Location = new System.Drawing.Point(264, 279);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(56, 17);
            this.cbActive.TabIndex = 12;
            this.cbActive.Text = "Active";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBirthDate.Location = new System.Drawing.Point(108, 121);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(91, 20);
            this.dtpBirthDate.TabIndex = 6;
            // 
            // rbtnFemale
            // 
            this.rbtnFemale.AutoSize = true;
            this.rbtnFemale.Location = new System.Drawing.Point(162, 98);
            this.rbtnFemale.Name = "rbtnFemale";
            this.rbtnFemale.Size = new System.Drawing.Size(59, 17);
            this.rbtnFemale.TabIndex = 5;
            this.rbtnFemale.Text = "Female";
            this.rbtnFemale.UseVisualStyleBackColor = true;
            // 
            // rbtnMale
            // 
            this.rbtnMale.AutoSize = true;
            this.rbtnMale.Checked = true;
            this.rbtnMale.Location = new System.Drawing.Point(108, 98);
            this.rbtnMale.Name = "rbtnMale";
            this.rbtnMale.Size = new System.Drawing.Size(48, 17);
            this.rbtnMale.TabIndex = 4;
            this.rbtnMale.TabStop = true;
            this.rbtnMale.Text = "Male";
            this.rbtnMale.UseVisualStyleBackColor = true;
            this.rbtnMale.CheckedChanged += new System.EventHandler(this.rbtnMale_CheckedChanged);
            // 
            // txtEmailAdd
            // 
            this.txtEmailAdd.Location = new System.Drawing.Point(108, 253);
            this.txtEmailAdd.MaxLength = 20;
            this.txtEmailAdd.Name = "txtEmailAdd";
            this.txtEmailAdd.Size = new System.Drawing.Size(212, 20);
            this.txtEmailAdd.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Email Add:";
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(108, 227);
            this.txtContact2.MaxLength = 20;
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Size = new System.Drawing.Size(212, 20);
            this.txtContact2.TabIndex = 10;
            this.txtContact2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContact2_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Contact # 2:";
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(108, 201);
            this.txtContact1.MaxLength = 20;
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Size = new System.Drawing.Size(212, 20);
            this.txtContact1.TabIndex = 9;
            this.txtContact1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContact1_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Contact # 1:";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(108, 175);
            this.txtAddress2.MaxLength = 150;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(212, 20);
            this.txtAddress2.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Address 2:";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(108, 149);
            this.txtAddress1.MaxLength = 150;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(212, 20);
            this.txtAddress1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Address 1:";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(108, 71);
            this.txtLastName.MaxLength = 50;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(212, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Birthdate:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Last Name:";
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(108, 45);
            this.txtMiddleName.MaxLength = 50;
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(212, 20);
            this.txtMiddleName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Middle Name:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(108, 19);
            this.txtFirstName.MaxLength = 50;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(212, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "First Name:";
            // 
            // tpActive
            // 
            this.tpActive.Controls.Add(this.dgActive);
            this.tpActive.Location = new System.Drawing.Point(4, 22);
            this.tpActive.Name = "tpActive";
            this.tpActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpActive.Size = new System.Drawing.Size(678, 323);
            this.tpActive.TabIndex = 1;
            this.tpActive.Text = "Active";
            this.tpActive.UseVisualStyleBackColor = true;
            // 
            // dgActive
            // 
            this.dgActive.AllowUserToAddRows = false;
            this.dgActive.AllowUserToDeleteRows = false;
            this.dgActive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgActive.BackgroundColor = System.Drawing.Color.White;
            this.dgActive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserId_Active,
            this.Name_Active,
            this.Address_Active,
            this.ContactNo_Active,
            this.Email_Active,
            this.Edit_Active});
            this.dgActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgActive.Location = new System.Drawing.Point(3, 3);
            this.dgActive.Name = "dgActive";
            this.dgActive.ReadOnly = true;
            this.dgActive.RowHeadersVisible = false;
            this.dgActive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgActive.Size = new System.Drawing.Size(672, 317);
            this.dgActive.TabIndex = 0;
            this.dgActive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgActive_CellContentClick);
            // 
            // UserId_Active
            // 
            this.UserId_Active.DataPropertyName = "UserId";
            this.UserId_Active.HeaderText = "UserId";
            this.UserId_Active.Name = "UserId_Active";
            this.UserId_Active.ReadOnly = true;
            this.UserId_Active.Visible = false;
            // 
            // Name_Active
            // 
            this.Name_Active.DataPropertyName = "UserFullName";
            this.Name_Active.HeaderText = "Name";
            this.Name_Active.Name = "Name_Active";
            this.Name_Active.ReadOnly = true;
            // 
            // Address_Active
            // 
            this.Address_Active.DataPropertyName = "Address1";
            this.Address_Active.HeaderText = "Address";
            this.Address_Active.Name = "Address_Active";
            this.Address_Active.ReadOnly = true;
            // 
            // ContactNo_Active
            // 
            this.ContactNo_Active.DataPropertyName = "ContactNo1";
            this.ContactNo_Active.HeaderText = "Contact #";
            this.ContactNo_Active.Name = "ContactNo_Active";
            this.ContactNo_Active.ReadOnly = true;
            // 
            // Email_Active
            // 
            this.Email_Active.DataPropertyName = "EmailAddress";
            this.Email_Active.HeaderText = "Email";
            this.Email_Active.Name = "Email_Active";
            this.Email_Active.ReadOnly = true;
            // 
            // Edit_Active
            // 
            this.Edit_Active.HeaderText = "";
            this.Edit_Active.Name = "Edit_Active";
            this.Edit_Active.ReadOnly = true;
            this.Edit_Active.Text = "Edit";
            this.Edit_Active.UseColumnTextForButtonValue = true;
            // 
            // tpInActive
            // 
            this.tpInActive.Controls.Add(this.dgInactive);
            this.tpInActive.Location = new System.Drawing.Point(4, 22);
            this.tpInActive.Name = "tpInActive";
            this.tpInActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpInActive.Size = new System.Drawing.Size(678, 323);
            this.tpInActive.TabIndex = 2;
            this.tpInActive.Text = "Inactive";
            this.tpInActive.UseVisualStyleBackColor = true;
            // 
            // dgInactive
            // 
            this.dgInactive.AllowUserToAddRows = false;
            this.dgInactive.AllowUserToDeleteRows = false;
            this.dgInactive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInactive.BackgroundColor = System.Drawing.Color.White;
            this.dgInactive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInactive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserId_Inactive,
            this.Name_InActive,
            this.Address_InActive,
            this.ContactNo_InActive,
            this.Email_InActive,
            this.Edit_InActive});
            this.dgInactive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInactive.Location = new System.Drawing.Point(3, 3);
            this.dgInactive.Name = "dgInactive";
            this.dgInactive.ReadOnly = true;
            this.dgInactive.RowHeadersVisible = false;
            this.dgInactive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInactive.Size = new System.Drawing.Size(672, 317);
            this.dgInactive.TabIndex = 1;
            this.dgInactive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInactive_CellContentClick);
            // 
            // UserId_Inactive
            // 
            this.UserId_Inactive.DataPropertyName = "UserId";
            this.UserId_Inactive.HeaderText = "UserId";
            this.UserId_Inactive.Name = "UserId_Inactive";
            this.UserId_Inactive.ReadOnly = true;
            this.UserId_Inactive.Visible = false;
            // 
            // Name_InActive
            // 
            this.Name_InActive.DataPropertyName = "UserFullName";
            this.Name_InActive.HeaderText = "Name";
            this.Name_InActive.Name = "Name_InActive";
            this.Name_InActive.ReadOnly = true;
            // 
            // Address_InActive
            // 
            this.Address_InActive.DataPropertyName = "Address1";
            this.Address_InActive.HeaderText = "Address";
            this.Address_InActive.Name = "Address_InActive";
            this.Address_InActive.ReadOnly = true;
            // 
            // ContactNo_InActive
            // 
            this.ContactNo_InActive.DataPropertyName = "ContactNo1";
            this.ContactNo_InActive.HeaderText = "Contact #";
            this.ContactNo_InActive.Name = "ContactNo_InActive";
            this.ContactNo_InActive.ReadOnly = true;
            // 
            // Email_InActive
            // 
            this.Email_InActive.DataPropertyName = "EmailAddress";
            this.Email_InActive.HeaderText = "Email";
            this.Email_InActive.Name = "Email_InActive";
            this.Email_InActive.ReadOnly = true;
            // 
            // Edit_InActive
            // 
            this.Edit_InActive.HeaderText = "";
            this.Edit_InActive.Name = "Edit_InActive";
            this.Edit_InActive.ReadOnly = true;
            this.Edit_InActive.Text = "Edit";
            this.Edit_InActive.UseColumnTextForButtonValue = true;
            // 
            // frmManageAdmins
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(686, 410);
            this.Controls.Add(this.tcManageAdmins);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "frmManageAdmins";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmManageAdmins_Load);
            this.panel1.ResumeLayout(false);
            this.tcManageAdmins.ResumeLayout(false);
            this.tpNew.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).EndInit();
            this.tpInActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tcManageAdmins;
        private System.Windows.Forms.TabPage tpNew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.RadioButton rbtnFemale;
        private System.Windows.Forms.RadioButton rbtnMale;
        private System.Windows.Forms.TextBox txtEmailAdd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtContact2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContact1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMiddleName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpActive;
        private System.Windows.Forms.TabPage tpInActive;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgActive;
        private System.Windows.Forms.DataGridView dgInactive;
        private System.Windows.Forms.TextBox txtChallengeAnswer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtChallengeQuestion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactNo_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_Active;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserId_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name_InActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address_InActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactNo_InActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_InActive;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_InActive;
    }
}