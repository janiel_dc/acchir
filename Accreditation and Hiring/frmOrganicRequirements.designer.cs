﻿namespace Accreditation_and_Hiring_System
{
    partial class frmOrganicRequirements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTINNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgMembersList = new System.Windows.Forms.DataGridView();
            this.MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpDateHired = new System.Windows.Forms.DateTimePicker();
            this.cb2x2Pic = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbHired = new System.Windows.Forms.CheckBox();
            this.txtPhilHealthNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPagIbigNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSSSNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgMembersList)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTINNo
            // 
            this.txtTINNo.Location = new System.Drawing.Point(79, 40);
            this.txtTINNo.MaxLength = 16;
            this.txtTINNo.Name = "txtTINNo";
            this.txtTINNo.Size = new System.Drawing.Size(192, 20);
            this.txtTINNo.TabIndex = 9;
            this.txtTINNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTINNo_KeyPress);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(528, 33);
            this.label1.TabIndex = 9;
            this.label1.Text = "Organic Personnel Requirements";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgMembersList
            // 
            this.dgMembersList.AllowUserToAddRows = false;
            this.dgMembersList.AllowUserToDeleteRows = false;
            this.dgMembersList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMembersList.BackgroundColor = System.Drawing.Color.White;
            this.dgMembersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMembersList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId,
            this.MemberName});
            this.dgMembersList.Location = new System.Drawing.Point(12, 70);
            this.dgMembersList.Name = "dgMembersList";
            this.dgMembersList.ReadOnly = true;
            this.dgMembersList.RowHeadersVisible = false;
            this.dgMembersList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMembersList.Size = new System.Drawing.Size(221, 259);
            this.dgMembersList.TabIndex = 3;
            this.dgMembersList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMembersList_CellClick);
            // 
            // MemberId
            // 
            this.MemberId.DataPropertyName = "MemberId";
            this.MemberId.HeaderText = "MemberId";
            this.MemberId.Name = "MemberId";
            this.MemberId.ReadOnly = true;
            this.MemberId.Visible = false;
            // 
            // MemberName
            // 
            this.MemberName.DataPropertyName = "FullName";
            this.MemberName.HeaderText = "Name";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // dtpDateHired
            // 
            this.dtpDateHired.Enabled = false;
            this.dtpDateHired.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateHired.Location = new System.Drawing.Point(79, 146);
            this.dtpDateHired.Name = "dtpDateHired";
            this.dtpDateHired.Size = new System.Drawing.Size(147, 20);
            this.dtpDateHired.TabIndex = 10;
            // 
            // cb2x2Pic
            // 
            this.cb2x2Pic.AutoSize = true;
            this.cb2x2Pic.Location = new System.Drawing.Point(79, 17);
            this.cb2x2Pic.Name = "cb2x2Pic";
            this.cb2x2Pic.Size = new System.Drawing.Size(74, 17);
            this.cb2x2Pic.TabIndex = 7;
            this.cb2x2Pic.Text = "2x2 Photo";
            this.cb2x2Pic.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 335);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(528, 28);
            this.panel2.TabIndex = 11;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(445, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(364, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(64, 44);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(169, 20);
            this.txtSearch.TabIndex = 4;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.dgMembersList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 363);
            this.panel1.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbHired);
            this.groupBox1.Controls.Add(this.txtPhilHealthNo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtPagIbigNo);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtSSSNo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtTINNo);
            this.groupBox1.Controls.Add(this.dtpDateHired);
            this.groupBox1.Controls.Add(this.cb2x2Pic);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(239, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 190);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Documents";
            // 
            // cbHired
            // 
            this.cbHired.AutoSize = true;
            this.cbHired.Location = new System.Drawing.Point(19, 149);
            this.cbHired.Name = "cbHired";
            this.cbHired.Size = new System.Drawing.Size(54, 17);
            this.cbHired.TabIndex = 13;
            this.cbHired.Text = "Hired:";
            this.cbHired.UseVisualStyleBackColor = true;
            this.cbHired.CheckedChanged += new System.EventHandler(this.cbHired_CheckedChanged);
            // 
            // txtPhilHealthNo
            // 
            this.txtPhilHealthNo.Location = new System.Drawing.Point(79, 118);
            this.txtPhilHealthNo.MaxLength = 20;
            this.txtPhilHealthNo.Name = "txtPhilHealthNo";
            this.txtPhilHealthNo.Size = new System.Drawing.Size(192, 20);
            this.txtPhilHealthNo.TabIndex = 12;
            this.txtPhilHealthNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhilHealthNo_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "PhilHealth #:";
            // 
            // txtPagIbigNo
            // 
            this.txtPagIbigNo.Location = new System.Drawing.Point(79, 92);
            this.txtPagIbigNo.MaxLength = 20;
            this.txtPagIbigNo.Name = "txtPagIbigNo";
            this.txtPagIbigNo.Size = new System.Drawing.Size(192, 20);
            this.txtPagIbigNo.TabIndex = 12;
            this.txtPagIbigNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPagIbigNo_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Pag Ibig #:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "SSS #:";
            // 
            // txtSSSNo
            // 
            this.txtSSSNo.Location = new System.Drawing.Point(79, 66);
            this.txtSSSNo.MaxLength = 20;
            this.txtSSSNo.Name = "txtSSSNo";
            this.txtSSSNo.Size = new System.Drawing.Size(192, 20);
            this.txtSSSNo.TabIndex = 12;
            this.txtSSSNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSSSNo_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "SSS #:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "TIN #:";
            // 
            // frmOrganicRequirements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(528, 363);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmOrganicRequirements";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmOrganicRequirements_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMembersList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtTINNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgMembersList;
        private System.Windows.Forms.DateTimePicker dtpDateHired;
        private System.Windows.Forms.CheckBox cb2x2Pic;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSSSNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPhilHealthNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPagIbigNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbHired;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;

    }
}