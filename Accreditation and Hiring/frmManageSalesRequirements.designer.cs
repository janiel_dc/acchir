﻿namespace Accreditation_and_Hiring_System
{
    partial class frmManageSalesRequirements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTINNo = new System.Windows.Forms.TextBox();
            this.dtpDateOfOrientation = new System.Windows.Forms.DateTimePicker();
            this.cb2x2Pic = new System.Windows.Forms.CheckBox();
            this.txtTrainingOfficer = new System.Windows.Forms.TextBox();
            this.cbGovernmentId = new System.Windows.Forms.CheckBox();
            this.cbCOA = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbSignatureOfEndorser = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbBasicOrientationSeminar = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dgMembersList = new System.Windows.Forms.DataGridView();
            this.MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.tcSalesPersonnelRequirements = new System.Windows.Forms.TabControl();
            this.tpStep1 = new System.Windows.Forms.TabPage();
            this.tpStep2 = new System.Windows.Forms.TabPage();
            this.cmbAttendanceSearch = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaveAttendance = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSiteOrientation = new System.Windows.Forms.CheckBox();
            this.cbRevalida = new System.Windows.Forms.CheckBox();
            this.cbBasicOrientation = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSearchAttendance = new System.Windows.Forms.TextBox();
            this.dgAttedance = new System.Windows.Forms.DataGridView();
            this.MemberId_Attendance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName_Attendance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpStep3 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnRequirements = new System.Windows.Forms.Button();
            this.cbDiploma = new System.Windows.Forms.CheckBox();
            this.cbNBI = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dgRequirements = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpStep4 = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.btnSaveEndorsement = new System.Windows.Forms.Button();
            this.cbEndorse = new System.Windows.Forms.CheckBox();
            this.cbMSA = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.dgEndorsement = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpStep5 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnSaveMSA = new System.Windows.Forms.Button();
            this.cbForSignMSA = new System.Windows.Forms.CheckBox();
            this.cbEnrollmentToBiometrics = new System.Windows.Forms.CheckBox();
            this.cb1x1IDPic = new System.Windows.Forms.CheckBox();
            this.cbIDApplication = new System.Windows.Forms.CheckBox();
            this.cbValidId = new System.Windows.Forms.CheckBox();
            this.cb1x1IDATM = new System.Windows.Forms.CheckBox();
            this.cbBankProcessing = new System.Windows.Forms.CheckBox();
            this.cbATMApplication = new System.Windows.Forms.CheckBox();
            this.cbMSASign = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.dgMSA = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpStep6 = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.btnAccredit = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.dgEndorseToAccounting = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtEndorser = new System.Windows.Forms.TextBox();
            this.txtReceiver = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMembersList)).BeginInit();
            this.panel2.SuspendLayout();
            this.tcSalesPersonnelRequirements.SuspendLayout();
            this.tpStep1.SuspendLayout();
            this.tpStep2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttedance)).BeginInit();
            this.tpStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRequirements)).BeginInit();
            this.tpStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEndorsement)).BeginInit();
            this.tpStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMSA)).BeginInit();
            this.tpStep6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEndorseToAccounting)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(547, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sales Agent";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.dgMembersList);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(533, 360);
            this.panel1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Segoe UI Symbol", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(533, 26);
            this.label7.TabIndex = 21;
            this.label7.Text = "Documents";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(337, 324);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(338, 301);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(185, 17);
            this.checkBox2.TabIndex = 12;
            this.checkBox2.Text = "Accredited to other marketing arm";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(255, 301);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(77, 17);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "Accredited";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtTINNo);
            this.groupBox1.Controls.Add(this.dtpDateOfOrientation);
            this.groupBox1.Controls.Add(this.cb2x2Pic);
            this.groupBox1.Controls.Add(this.txtTrainingOfficer);
            this.groupBox1.Controls.Add(this.cbGovernmentId);
            this.groupBox1.Controls.Add(this.cbCOA);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbSignatureOfEndorser);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbBasicOrientationSeminar);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(239, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 220);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Documents";
            // 
            // txtTINNo
            // 
            this.txtTINNo.Location = new System.Drawing.Point(53, 19);
            this.txtTINNo.MaxLength = 16;
            this.txtTINNo.Name = "txtTINNo";
            this.txtTINNo.Size = new System.Drawing.Size(210, 20);
            this.txtTINNo.TabIndex = 9;
            this.txtTINNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTINNo_KeyPress);
            // 
            // dtpDateOfOrientation
            // 
            this.dtpDateOfOrientation.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateOfOrientation.Location = new System.Drawing.Point(116, 43);
            this.dtpDateOfOrientation.Name = "dtpDateOfOrientation";
            this.dtpDateOfOrientation.Size = new System.Drawing.Size(147, 20);
            this.dtpDateOfOrientation.TabIndex = 10;
            // 
            // cb2x2Pic
            // 
            this.cb2x2Pic.AutoSize = true;
            this.cb2x2Pic.Location = new System.Drawing.Point(16, 99);
            this.cb2x2Pic.Name = "cb2x2Pic";
            this.cb2x2Pic.Size = new System.Drawing.Size(74, 17);
            this.cb2x2Pic.TabIndex = 7;
            this.cb2x2Pic.Text = "2x2 Photo";
            this.cb2x2Pic.UseVisualStyleBackColor = true;
            // 
            // txtTrainingOfficer
            // 
            this.txtTrainingOfficer.Location = new System.Drawing.Point(98, 68);
            this.txtTrainingOfficer.MaxLength = 50;
            this.txtTrainingOfficer.Name = "txtTrainingOfficer";
            this.txtTrainingOfficer.Size = new System.Drawing.Size(165, 20);
            this.txtTrainingOfficer.TabIndex = 9;
            // 
            // cbGovernmentId
            // 
            this.cbGovernmentId.AutoSize = true;
            this.cbGovernmentId.Location = new System.Drawing.Point(16, 122);
            this.cbGovernmentId.Name = "cbGovernmentId";
            this.cbGovernmentId.Size = new System.Drawing.Size(96, 17);
            this.cbGovernmentId.TabIndex = 7;
            this.cbGovernmentId.Text = "Government Id";
            this.cbGovernmentId.UseVisualStyleBackColor = true;
            // 
            // cbCOA
            // 
            this.cbCOA.AutoSize = true;
            this.cbCOA.Location = new System.Drawing.Point(16, 145);
            this.cbCOA.Name = "cbCOA";
            this.cbCOA.Size = new System.Drawing.Size(160, 17);
            this.cbCOA.TabIndex = 7;
            this.cbCOA.Text = "Sales Agent application form";
            this.cbCOA.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Training Officer:";
            // 
            // cbSignatureOfEndorser
            // 
            this.cbSignatureOfEndorser.AutoSize = true;
            this.cbSignatureOfEndorser.Location = new System.Drawing.Point(16, 168);
            this.cbSignatureOfEndorser.Name = "cbSignatureOfEndorser";
            this.cbSignatureOfEndorser.Size = new System.Drawing.Size(130, 17);
            this.cbSignatureOfEndorser.TabIndex = 7;
            this.cbSignatureOfEndorser.Text = "Signature Of Endorser";
            this.cbSignatureOfEndorser.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Date Of Orientation:";
            // 
            // cbBasicOrientationSeminar
            // 
            this.cbBasicOrientationSeminar.AutoSize = true;
            this.cbBasicOrientationSeminar.Location = new System.Drawing.Point(16, 191);
            this.cbBasicOrientationSeminar.Name = "cbBasicOrientationSeminar";
            this.cbBasicOrientationSeminar.Size = new System.Drawing.Size(147, 17);
            this.cbBasicOrientationSeminar.TabIndex = 7;
            this.cbBasicOrientationSeminar.Text = "Basic Orientation Seminar";
            this.cbBasicOrientationSeminar.UseVisualStyleBackColor = true;
            this.cbBasicOrientationSeminar.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "TIN #:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(64, 33);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(169, 20);
            this.txtSearch.TabIndex = 4;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // dgMembersList
            // 
            this.dgMembersList.AllowUserToAddRows = false;
            this.dgMembersList.AllowUserToDeleteRows = false;
            this.dgMembersList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMembersList.BackgroundColor = System.Drawing.Color.White;
            this.dgMembersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMembersList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId,
            this.MemberName});
            this.dgMembersList.Location = new System.Drawing.Point(12, 59);
            this.dgMembersList.Name = "dgMembersList";
            this.dgMembersList.ReadOnly = true;
            this.dgMembersList.RowHeadersVisible = false;
            this.dgMembersList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMembersList.Size = new System.Drawing.Size(221, 259);
            this.dgMembersList.TabIndex = 3;
            this.dgMembersList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMembersList_CellClick);
            // 
            // MemberId
            // 
            this.MemberId.DataPropertyName = "MemberId";
            this.MemberId.HeaderText = "MemberId";
            this.MemberId.Name = "MemberId";
            this.MemberId.ReadOnly = true;
            this.MemberId.Visible = false;
            // 
            // MemberName
            // 
            this.MemberName.DataPropertyName = "FullName";
            this.MemberName.HeaderText = "Name";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(249, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Verification to PFCI";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 425);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(547, 28);
            this.panel2.TabIndex = 8;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(445, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tcSalesPersonnelRequirements
            // 
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep1);
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep2);
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep3);
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep4);
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep5);
            this.tcSalesPersonnelRequirements.Controls.Add(this.tpStep6);
            this.tcSalesPersonnelRequirements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSalesPersonnelRequirements.Location = new System.Drawing.Point(0, 33);
            this.tcSalesPersonnelRequirements.Name = "tcSalesPersonnelRequirements";
            this.tcSalesPersonnelRequirements.SelectedIndex = 0;
            this.tcSalesPersonnelRequirements.Size = new System.Drawing.Size(547, 392);
            this.tcSalesPersonnelRequirements.TabIndex = 13;
            this.tcSalesPersonnelRequirements.SelectedIndexChanged += new System.EventHandler(this.tcSalesPersonnelRequirements_SelectedIndexChanged);
            // 
            // tpStep1
            // 
            this.tpStep1.Controls.Add(this.panel1);
            this.tpStep1.Location = new System.Drawing.Point(4, 22);
            this.tpStep1.Name = "tpStep1";
            this.tpStep1.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep1.Size = new System.Drawing.Size(539, 366);
            this.tpStep1.TabIndex = 0;
            this.tpStep1.Text = "Step 1";
            this.tpStep1.UseVisualStyleBackColor = true;
            // 
            // tpStep2
            // 
            this.tpStep2.Controls.Add(this.cmbAttendanceSearch);
            this.tpStep2.Controls.Add(this.label8);
            this.tpStep2.Controls.Add(this.groupBox2);
            this.tpStep2.Controls.Add(this.label19);
            this.tpStep2.Controls.Add(this.label12);
            this.tpStep2.Controls.Add(this.txtSearchAttendance);
            this.tpStep2.Controls.Add(this.dgAttedance);
            this.tpStep2.Location = new System.Drawing.Point(4, 22);
            this.tpStep2.Name = "tpStep2";
            this.tpStep2.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep2.Size = new System.Drawing.Size(539, 366);
            this.tpStep2.TabIndex = 1;
            this.tpStep2.Text = "Step 2";
            this.tpStep2.UseVisualStyleBackColor = true;
            // 
            // cmbAttendanceSearch
            // 
            this.cmbAttendanceSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAttendanceSearch.FormattingEnabled = true;
            this.cmbAttendanceSearch.Location = new System.Drawing.Point(72, 38);
            this.cmbAttendanceSearch.Name = "cmbAttendanceSearch";
            this.cmbAttendanceSearch.Size = new System.Drawing.Size(174, 21);
            this.cmbAttendanceSearch.TabIndex = 21;
            this.cmbAttendanceSearch.SelectionChangeCommitted += new System.EventHandler(this.cmbAttendanceSearch_SelectionChangeCommitted);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Segoe UI Symbol", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(533, 26);
            this.label8.TabIndex = 20;
            this.label8.Text = "Attendance";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSaveAttendance);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.cbSiteOrientation);
            this.groupBox2.Controls.Add(this.cbRevalida);
            this.groupBox2.Controls.Add(this.cbBasicOrientation);
            this.groupBox2.Location = new System.Drawing.Point(260, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 196);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // btnSaveAttendance
            // 
            this.btnSaveAttendance.BackColor = System.Drawing.Color.Green;
            this.btnSaveAttendance.ForeColor = System.Drawing.Color.White;
            this.btnSaveAttendance.Location = new System.Drawing.Point(45, 147);
            this.btnSaveAttendance.Name = "btnSaveAttendance";
            this.btnSaveAttendance.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAttendance.TabIndex = 31;
            this.btnSaveAttendance.Text = "Save";
            this.btnSaveAttendance.UseVisualStyleBackColor = false;
            this.btnSaveAttendance.Click += new System.EventHandler(this.btnSaveAttendance_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(42, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 16);
            this.label11.TabIndex = 29;
            this.label11.Text = "Revalida";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(42, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 16);
            this.label10.TabIndex = 30;
            this.label10.Text = "Site orientation";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(42, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(158, 16);
            this.label9.TabIndex = 28;
            this.label9.Text = "Basic orientation seminar";
            // 
            // cbSiteOrientation
            // 
            this.cbSiteOrientation.AutoSize = true;
            this.cbSiteOrientation.Location = new System.Drawing.Point(69, 85);
            this.cbSiteOrientation.Name = "cbSiteOrientation";
            this.cbSiteOrientation.Size = new System.Drawing.Size(69, 17);
            this.cbSiteOrientation.TabIndex = 27;
            this.cbSiteOrientation.Text = "Attended";
            this.cbSiteOrientation.UseVisualStyleBackColor = true;
            // 
            // cbRevalida
            // 
            this.cbRevalida.AutoSize = true;
            this.cbRevalida.Location = new System.Drawing.Point(69, 124);
            this.cbRevalida.Name = "cbRevalida";
            this.cbRevalida.Size = new System.Drawing.Size(49, 17);
            this.cbRevalida.TabIndex = 26;
            this.cbRevalida.Text = "Pass";
            this.cbRevalida.UseVisualStyleBackColor = true;
            // 
            // cbBasicOrientation
            // 
            this.cbBasicOrientation.AutoSize = true;
            this.cbBasicOrientation.Location = new System.Drawing.Point(69, 46);
            this.cbBasicOrientation.Name = "cbBasicOrientation";
            this.cbBasicOrientation.Size = new System.Drawing.Size(69, 17);
            this.cbBasicOrientation.TabIndex = 25;
            this.cbBasicOrientation.Text = "Attended";
            this.cbBasicOrientation.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(257, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(198, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "*click on the customer name to view info";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Search:";
            // 
            // txtSearchAttendance
            // 
            this.txtSearchAttendance.Location = new System.Drawing.Point(333, 39);
            this.txtSearchAttendance.MaxLength = 100;
            this.txtSearchAttendance.Name = "txtSearchAttendance";
            this.txtSearchAttendance.Size = new System.Drawing.Size(169, 20);
            this.txtSearchAttendance.TabIndex = 14;
            this.txtSearchAttendance.Visible = false;
            this.txtSearchAttendance.TextChanged += new System.EventHandler(this.txtSearchAttendance_TextChanged);
            // 
            // dgAttedance
            // 
            this.dgAttedance.AllowUserToAddRows = false;
            this.dgAttedance.AllowUserToDeleteRows = false;
            this.dgAttedance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAttedance.BackgroundColor = System.Drawing.Color.White;
            this.dgAttedance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAttedance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberId_Attendance,
            this.MemberName_Attendance});
            this.dgAttedance.Location = new System.Drawing.Point(25, 65);
            this.dgAttedance.Name = "dgAttedance";
            this.dgAttedance.ReadOnly = true;
            this.dgAttedance.RowHeadersVisible = false;
            this.dgAttedance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAttedance.Size = new System.Drawing.Size(221, 259);
            this.dgAttedance.TabIndex = 13;
            this.dgAttedance.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAttedance_CellClick);
            // 
            // MemberId_Attendance
            // 
            this.MemberId_Attendance.DataPropertyName = "MemberId";
            this.MemberId_Attendance.HeaderText = "MemberId";
            this.MemberId_Attendance.Name = "MemberId_Attendance";
            this.MemberId_Attendance.ReadOnly = true;
            this.MemberId_Attendance.Visible = false;
            // 
            // MemberName_Attendance
            // 
            this.MemberName_Attendance.DataPropertyName = "FullName";
            this.MemberName_Attendance.HeaderText = "Name";
            this.MemberName_Attendance.Name = "MemberName_Attendance";
            this.MemberName_Attendance.ReadOnly = true;
            // 
            // tpStep3
            // 
            this.tpStep3.Controls.Add(this.label22);
            this.tpStep3.Controls.Add(this.label16);
            this.tpStep3.Controls.Add(this.btnRequirements);
            this.tpStep3.Controls.Add(this.cbDiploma);
            this.tpStep3.Controls.Add(this.cbNBI);
            this.tpStep3.Controls.Add(this.label17);
            this.tpStep3.Controls.Add(this.textBox1);
            this.tpStep3.Controls.Add(this.dgRequirements);
            this.tpStep3.Location = new System.Drawing.Point(4, 22);
            this.tpStep3.Name = "tpStep3";
            this.tpStep3.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep3.Size = new System.Drawing.Size(539, 366);
            this.tpStep3.TabIndex = 2;
            this.tpStep3.Text = "Step 3";
            this.tpStep3.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(288, 66);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(198, 13);
            this.label22.TabIndex = 36;
            this.label22.Text = "*click on the customer name to view info";
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Segoe UI Symbol", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(3, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(533, 26);
            this.label16.TabIndex = 35;
            this.label16.Text = "Requirements";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnRequirements
            // 
            this.btnRequirements.BackColor = System.Drawing.Color.Green;
            this.btnRequirements.ForeColor = System.Drawing.Color.White;
            this.btnRequirements.Location = new System.Drawing.Point(337, 192);
            this.btnRequirements.Name = "btnRequirements";
            this.btnRequirements.Size = new System.Drawing.Size(75, 23);
            this.btnRequirements.TabIndex = 34;
            this.btnRequirements.Text = "Save";
            this.btnRequirements.UseVisualStyleBackColor = false;
            this.btnRequirements.Click += new System.EventHandler(this.btnRequirements_Click);
            // 
            // cbDiploma
            // 
            this.cbDiploma.AutoSize = true;
            this.cbDiploma.Location = new System.Drawing.Point(337, 160);
            this.cbDiploma.Name = "cbDiploma";
            this.cbDiploma.Size = new System.Drawing.Size(98, 17);
            this.cbDiploma.TabIndex = 33;
            this.cbDiploma.Text = "TOR / Diploma";
            this.cbDiploma.UseVisualStyleBackColor = true;
            // 
            // cbNBI
            // 
            this.cbNBI.AutoSize = true;
            this.cbNBI.Location = new System.Drawing.Point(337, 137);
            this.cbNBI.Name = "cbNBI";
            this.cbNBI.Size = new System.Drawing.Size(70, 17);
            this.cbNBI.TabIndex = 32;
            this.cbNBI.Text = "Valid NBI";
            this.cbNBI.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(30, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Search:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(80, 40);
            this.textBox1.MaxLength = 100;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(169, 20);
            this.textBox1.TabIndex = 21;
            // 
            // dgRequirements
            // 
            this.dgRequirements.AllowUserToAddRows = false;
            this.dgRequirements.AllowUserToDeleteRows = false;
            this.dgRequirements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRequirements.BackgroundColor = System.Drawing.Color.White;
            this.dgRequirements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRequirements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgRequirements.Location = new System.Drawing.Point(33, 66);
            this.dgRequirements.Name = "dgRequirements";
            this.dgRequirements.ReadOnly = true;
            this.dgRequirements.RowHeadersVisible = false;
            this.dgRequirements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRequirements.Size = new System.Drawing.Size(221, 259);
            this.dgRequirements.TabIndex = 20;
            this.dgRequirements.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRequirements_CellClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MemberId";
            this.dataGridViewTextBoxColumn3.HeaderText = "MemberId";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "FullName";
            this.dataGridViewTextBoxColumn4.HeaderText = "Name";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // tpStep4
            // 
            this.tpStep4.Controls.Add(this.label23);
            this.tpStep4.Controls.Add(this.label21);
            this.tpStep4.Controls.Add(this.btnSaveEndorsement);
            this.tpStep4.Controls.Add(this.cbEndorse);
            this.tpStep4.Controls.Add(this.cbMSA);
            this.tpStep4.Controls.Add(this.label14);
            this.tpStep4.Controls.Add(this.textBox2);
            this.tpStep4.Controls.Add(this.dgEndorsement);
            this.tpStep4.Location = new System.Drawing.Point(4, 22);
            this.tpStep4.Name = "tpStep4";
            this.tpStep4.Padding = new System.Windows.Forms.Padding(3);
            this.tpStep4.Size = new System.Drawing.Size(539, 366);
            this.tpStep4.TabIndex = 3;
            this.tpStep4.Text = "Step 4";
            this.tpStep4.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.White;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Segoe UI Symbol", 12F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(3, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(533, 26);
            this.label21.TabIndex = 42;
            this.label21.Text = "Endorsement to VP / CEO";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSaveEndorsement
            // 
            this.btnSaveEndorsement.BackColor = System.Drawing.Color.Green;
            this.btnSaveEndorsement.ForeColor = System.Drawing.Color.White;
            this.btnSaveEndorsement.Location = new System.Drawing.Point(297, 181);
            this.btnSaveEndorsement.Name = "btnSaveEndorsement";
            this.btnSaveEndorsement.Size = new System.Drawing.Size(75, 23);
            this.btnSaveEndorsement.TabIndex = 41;
            this.btnSaveEndorsement.Text = "Save";
            this.btnSaveEndorsement.UseVisualStyleBackColor = false;
            this.btnSaveEndorsement.Click += new System.EventHandler(this.btnSaveEndorsement_Click);
            // 
            // cbEndorse
            // 
            this.cbEndorse.AutoSize = true;
            this.cbEndorse.Location = new System.Drawing.Point(297, 149);
            this.cbEndorse.Name = "cbEndorse";
            this.cbEndorse.Size = new System.Drawing.Size(192, 17);
            this.cbEndorse.TabIndex = 40;
            this.cbEndorse.Text = "For Broker Endorse to Broker Head";
            this.cbEndorse.UseVisualStyleBackColor = true;
            // 
            // cbMSA
            // 
            this.cbMSA.AutoSize = true;
            this.cbMSA.Location = new System.Drawing.Point(297, 126);
            this.cbMSA.Name = "cbMSA";
            this.cbMSA.Size = new System.Drawing.Size(67, 17);
            this.cbMSA.TabIndex = 39;
            this.cbMSA.Text = "For MSA";
            this.cbMSA.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Search:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(77, 47);
            this.textBox2.MaxLength = 100;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(169, 20);
            this.textBox2.TabIndex = 36;
            // 
            // dgEndorsement
            // 
            this.dgEndorsement.AllowUserToAddRows = false;
            this.dgEndorsement.AllowUserToDeleteRows = false;
            this.dgEndorsement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgEndorsement.BackgroundColor = System.Drawing.Color.White;
            this.dgEndorsement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEndorsement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.dgEndorsement.Location = new System.Drawing.Point(30, 73);
            this.dgEndorsement.Name = "dgEndorsement";
            this.dgEndorsement.ReadOnly = true;
            this.dgEndorsement.RowHeadersVisible = false;
            this.dgEndorsement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEndorsement.Size = new System.Drawing.Size(221, 259);
            this.dgEndorsement.TabIndex = 35;
            this.dgEndorsement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEndorsement_CellClick);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MemberId";
            this.dataGridViewTextBoxColumn5.HeaderText = "MemberId";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "FullName";
            this.dataGridViewTextBoxColumn6.HeaderText = "Name";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // tpStep5
            // 
            this.tpStep5.Controls.Add(this.label24);
            this.tpStep5.Controls.Add(this.label13);
            this.tpStep5.Controls.Add(this.dateTimePicker4);
            this.tpStep5.Controls.Add(this.dateTimePicker3);
            this.tpStep5.Controls.Add(this.dateTimePicker2);
            this.tpStep5.Controls.Add(this.dateTimePicker1);
            this.tpStep5.Controls.Add(this.btnSaveMSA);
            this.tpStep5.Controls.Add(this.cbForSignMSA);
            this.tpStep5.Controls.Add(this.cbEnrollmentToBiometrics);
            this.tpStep5.Controls.Add(this.cb1x1IDPic);
            this.tpStep5.Controls.Add(this.cbIDApplication);
            this.tpStep5.Controls.Add(this.cbValidId);
            this.tpStep5.Controls.Add(this.cb1x1IDATM);
            this.tpStep5.Controls.Add(this.cbBankProcessing);
            this.tpStep5.Controls.Add(this.cbATMApplication);
            this.tpStep5.Controls.Add(this.cbMSASign);
            this.tpStep5.Controls.Add(this.label18);
            this.tpStep5.Controls.Add(this.textBox4);
            this.tpStep5.Controls.Add(this.dgMSA);
            this.tpStep5.Location = new System.Drawing.Point(4, 22);
            this.tpStep5.Name = "tpStep5";
            this.tpStep5.Size = new System.Drawing.Size(539, 366);
            this.tpStep5.TabIndex = 4;
            this.tpStep5.Text = "Step 5";
            this.tpStep5.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Segoe UI Symbol", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(539, 26);
            this.label13.TabIndex = 53;
            this.label13.Text = "MSA";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(453, 274);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(78, 20);
            this.dateTimePicker4.TabIndex = 52;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(408, 206);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(89, 20);
            this.dateTimePicker3.TabIndex = 51;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(408, 114);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(89, 20);
            this.dateTimePicker2.TabIndex = 50;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(408, 88);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(89, 20);
            this.dateTimePicker1.TabIndex = 49;
            // 
            // btnSaveMSA
            // 
            this.btnSaveMSA.BackColor = System.Drawing.Color.Green;
            this.btnSaveMSA.ForeColor = System.Drawing.Color.White;
            this.btnSaveMSA.Location = new System.Drawing.Point(296, 307);
            this.btnSaveMSA.Name = "btnSaveMSA";
            this.btnSaveMSA.Size = new System.Drawing.Size(75, 23);
            this.btnSaveMSA.TabIndex = 48;
            this.btnSaveMSA.Text = "Save";
            this.btnSaveMSA.UseVisualStyleBackColor = false;
            this.btnSaveMSA.Click += new System.EventHandler(this.btnSaveMSA_Click);
            // 
            // cbForSignMSA
            // 
            this.cbForSignMSA.AutoSize = true;
            this.cbForSignMSA.Location = new System.Drawing.Point(296, 277);
            this.cbForSignMSA.Name = "cbForSignMSA";
            this.cbForSignMSA.Size = new System.Drawing.Size(156, 17);
            this.cbForSignMSA.TabIndex = 47;
            this.cbForSignMSA.Text = "For signature of MSA by VP";
            this.cbForSignMSA.UseVisualStyleBackColor = true;
            // 
            // cbEnrollmentToBiometrics
            // 
            this.cbEnrollmentToBiometrics.AutoSize = true;
            this.cbEnrollmentToBiometrics.Location = new System.Drawing.Point(296, 254);
            this.cbEnrollmentToBiometrics.Name = "cbEnrollmentToBiometrics";
            this.cbEnrollmentToBiometrics.Size = new System.Drawing.Size(144, 17);
            this.cbEnrollmentToBiometrics.TabIndex = 47;
            this.cbEnrollmentToBiometrics.Text = "Enrollment to Biomentrics";
            this.cbEnrollmentToBiometrics.UseVisualStyleBackColor = true;
            // 
            // cb1x1IDPic
            // 
            this.cb1x1IDPic.AutoSize = true;
            this.cb1x1IDPic.Location = new System.Drawing.Point(306, 231);
            this.cb1x1IDPic.Name = "cb1x1IDPic";
            this.cb1x1IDPic.Size = new System.Drawing.Size(93, 17);
            this.cb1x1IDPic.TabIndex = 47;
            this.cb1x1IDPic.Text = "1x1 ID Picture";
            this.cb1x1IDPic.UseVisualStyleBackColor = true;
            // 
            // cbIDApplication
            // 
            this.cbIDApplication.AutoSize = true;
            this.cbIDApplication.Location = new System.Drawing.Point(296, 208);
            this.cbIDApplication.Name = "cbIDApplication";
            this.cbIDApplication.Size = new System.Drawing.Size(92, 17);
            this.cbIDApplication.TabIndex = 47;
            this.cbIDApplication.Text = "ID Application";
            this.cbIDApplication.UseVisualStyleBackColor = true;
            // 
            // cbValidId
            // 
            this.cbValidId.AutoSize = true;
            this.cbValidId.Location = new System.Drawing.Point(306, 185);
            this.cbValidId.Name = "cbValidId";
            this.cbValidId.Size = new System.Drawing.Size(101, 17);
            this.cbValidId.TabIndex = 47;
            this.cbValidId.Text = "Copy of valid ID";
            this.cbValidId.UseVisualStyleBackColor = true;
            // 
            // cb1x1IDATM
            // 
            this.cb1x1IDATM.AutoSize = true;
            this.cb1x1IDATM.Location = new System.Drawing.Point(306, 162);
            this.cb1x1IDATM.Name = "cb1x1IDATM";
            this.cb1x1IDATM.Size = new System.Drawing.Size(93, 17);
            this.cb1x1IDATM.TabIndex = 47;
            this.cb1x1IDATM.Text = "1x1 ID Picture";
            this.cb1x1IDATM.UseVisualStyleBackColor = true;
            // 
            // cbBankProcessing
            // 
            this.cbBankProcessing.AutoSize = true;
            this.cbBankProcessing.Location = new System.Drawing.Point(306, 139);
            this.cbBankProcessing.Name = "cbBankProcessing";
            this.cbBankProcessing.Size = new System.Drawing.Size(124, 17);
            this.cbBankProcessing.TabIndex = 47;
            this.cbBankProcessing.Text = "Bank Processing fee";
            this.cbBankProcessing.UseVisualStyleBackColor = true;
            // 
            // cbATMApplication
            // 
            this.cbATMApplication.AutoSize = true;
            this.cbATMApplication.Location = new System.Drawing.Point(296, 116);
            this.cbATMApplication.Name = "cbATMApplication";
            this.cbATMApplication.Size = new System.Drawing.Size(104, 17);
            this.cbATMApplication.TabIndex = 47;
            this.cbATMApplication.Text = "ATM Application";
            this.cbATMApplication.UseVisualStyleBackColor = true;
            // 
            // cbMSASign
            // 
            this.cbMSASign.AutoSize = true;
            this.cbMSASign.Location = new System.Drawing.Point(296, 88);
            this.cbMSASign.Name = "cbMSASign";
            this.cbMSASign.Size = new System.Drawing.Size(71, 17);
            this.cbMSASign.TabIndex = 46;
            this.cbMSASign.Text = "MSA sign";
            this.cbMSASign.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(29, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "Search:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(79, 47);
            this.textBox4.MaxLength = 100;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(169, 20);
            this.textBox4.TabIndex = 43;
            // 
            // dgMSA
            // 
            this.dgMSA.AllowUserToAddRows = false;
            this.dgMSA.AllowUserToDeleteRows = false;
            this.dgMSA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMSA.BackgroundColor = System.Drawing.Color.White;
            this.dgMSA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMSA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgMSA.Location = new System.Drawing.Point(32, 73);
            this.dgMSA.Name = "dgMSA";
            this.dgMSA.ReadOnly = true;
            this.dgMSA.RowHeadersVisible = false;
            this.dgMSA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMSA.Size = new System.Drawing.Size(221, 259);
            this.dgMSA.TabIndex = 42;
            this.dgMSA.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMSA_CellClick);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MemberId";
            this.dataGridViewTextBoxColumn7.HeaderText = "MemberId";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "FullName";
            this.dataGridViewTextBoxColumn8.HeaderText = "Name";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // tpStep6
            // 
            this.tpStep6.Controls.Add(this.label27);
            this.tpStep6.Controls.Add(this.label26);
            this.tpStep6.Controls.Add(this.label25);
            this.tpStep6.Controls.Add(this.txtReceiver);
            this.tpStep6.Controls.Add(this.txtEndorser);
            this.tpStep6.Controls.Add(this.label15);
            this.tpStep6.Controls.Add(this.btnAccredit);
            this.tpStep6.Controls.Add(this.label20);
            this.tpStep6.Controls.Add(this.textBox5);
            this.tpStep6.Controls.Add(this.dgEndorseToAccounting);
            this.tpStep6.Location = new System.Drawing.Point(4, 22);
            this.tpStep6.Name = "tpStep6";
            this.tpStep6.Size = new System.Drawing.Size(539, 366);
            this.tpStep6.TabIndex = 5;
            this.tpStep6.Text = "Step 6";
            this.tpStep6.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(539, 26);
            this.label15.TabIndex = 63;
            this.label15.Text = "Endorsement Of The Following To Accounting";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnAccredit
            // 
            this.btnAccredit.BackColor = System.Drawing.Color.Green;
            this.btnAccredit.ForeColor = System.Drawing.Color.White;
            this.btnAccredit.Location = new System.Drawing.Point(305, 211);
            this.btnAccredit.Name = "btnAccredit";
            this.btnAccredit.Size = new System.Drawing.Size(75, 23);
            this.btnAccredit.TabIndex = 62;
            this.btnAccredit.Text = "Save";
            this.btnAccredit.UseVisualStyleBackColor = false;
            this.btnAccredit.Click += new System.EventHandler(this.btnAccredit_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(40, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 13);
            this.label20.TabIndex = 51;
            this.label20.Text = "Search:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(90, 45);
            this.textBox5.MaxLength = 100;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(169, 20);
            this.textBox5.TabIndex = 50;
            // 
            // dgEndorseToAccounting
            // 
            this.dgEndorseToAccounting.AllowUserToAddRows = false;
            this.dgEndorseToAccounting.AllowUserToDeleteRows = false;
            this.dgEndorseToAccounting.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgEndorseToAccounting.BackgroundColor = System.Drawing.Color.White;
            this.dgEndorseToAccounting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEndorseToAccounting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dgEndorseToAccounting.Location = new System.Drawing.Point(43, 71);
            this.dgEndorseToAccounting.Name = "dgEndorseToAccounting";
            this.dgEndorseToAccounting.ReadOnly = true;
            this.dgEndorseToAccounting.RowHeadersVisible = false;
            this.dgEndorseToAccounting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEndorseToAccounting.Size = new System.Drawing.Size(221, 259);
            this.dgEndorseToAccounting.TabIndex = 49;
            this.dgEndorseToAccounting.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEndorseToAccounting_CellClick);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MemberId";
            this.dataGridViewTextBoxColumn9.HeaderText = "MemberId";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "FullName";
            this.dataGridViewTextBoxColumn10.HeaderText = "Name";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(291, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(198, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "*click on the customer name to view info";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(293, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(198, 13);
            this.label24.TabIndex = 54;
            this.label24.Text = "*click on the customer name to view info";
            // 
            // txtEndorser
            // 
            this.txtEndorser.Location = new System.Drawing.Point(305, 128);
            this.txtEndorser.MaxLength = 100;
            this.txtEndorser.Name = "txtEndorser";
            this.txtEndorser.Size = new System.Drawing.Size(196, 20);
            this.txtEndorser.TabIndex = 64;
            // 
            // txtReceiver
            // 
            this.txtReceiver.Location = new System.Drawing.Point(305, 174);
            this.txtReceiver.MaxLength = 100;
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(196, 20);
            this.txtReceiver.TabIndex = 64;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(302, 112);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(137, 13);
            this.label25.TabIndex = 65;
            this.label25.Text = "Checked and Endorsed By:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(302, 158);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "Recieved By:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(302, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(198, 13);
            this.label27.TabIndex = 66;
            this.label27.Text = "*click on the customer name to view info";
            // 
            // frmManageSalesRequirements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(547, 453);
            this.Controls.Add(this.tcSalesPersonnelRequirements);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Name = "frmManageSalesRequirements";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmManageSalesRequirements_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMembersList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tcSalesPersonnelRequirements.ResumeLayout(false);
            this.tpStep1.ResumeLayout(false);
            this.tpStep2.ResumeLayout(false);
            this.tpStep2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttedance)).EndInit();
            this.tpStep3.ResumeLayout(false);
            this.tpStep3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRequirements)).EndInit();
            this.tpStep4.ResumeLayout(false);
            this.tpStep4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEndorsement)).EndInit();
            this.tpStep5.ResumeLayout(false);
            this.tpStep5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMSA)).EndInit();
            this.tpStep6.ResumeLayout(false);
            this.tpStep6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEndorseToAccounting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgMembersList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtTINNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb2x2Pic;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox cbSignatureOfEndorser;
        private System.Windows.Forms.CheckBox cbCOA;
        private System.Windows.Forms.CheckBox cbGovernmentId;
        private System.Windows.Forms.DateTimePicker dtpDateOfOrientation;
        private System.Windows.Forms.TextBox txtTrainingOfficer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbBasicOrientationSeminar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;
        private System.Windows.Forms.TabControl tcSalesPersonnelRequirements;
        private System.Windows.Forms.TabPage tpStep1;
        private System.Windows.Forms.TabPage tpStep2;
        private System.Windows.Forms.TabPage tpStep3;
        private System.Windows.Forms.TabPage tpStep4;
        private System.Windows.Forms.TabPage tpStep5;
        private System.Windows.Forms.TabPage tpStep6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSearchAttendance;
        private System.Windows.Forms.DataGridView dgAttedance;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSaveAttendance;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbSiteOrientation;
        private System.Windows.Forms.CheckBox cbRevalida;
        private System.Windows.Forms.CheckBox cbBasicOrientation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dgRequirements;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Button btnRequirements;
        private System.Windows.Forms.CheckBox cbDiploma;
        private System.Windows.Forms.CheckBox cbNBI;
        private System.Windows.Forms.Button btnSaveEndorsement;
        private System.Windows.Forms.CheckBox cbEndorse;
        private System.Windows.Forms.CheckBox cbMSA;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridView dgEndorsement;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Button btnSaveMSA;
        private System.Windows.Forms.CheckBox cbATMApplication;
        private System.Windows.Forms.CheckBox cbMSASign;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.DataGridView dgMSA;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.CheckBox cbBankProcessing;
        private System.Windows.Forms.CheckBox cbForSignMSA;
        private System.Windows.Forms.CheckBox cbEnrollmentToBiometrics;
        private System.Windows.Forms.CheckBox cb1x1IDPic;
        private System.Windows.Forms.CheckBox cbIDApplication;
        private System.Windows.Forms.CheckBox cbValidId;
        private System.Windows.Forms.CheckBox cb1x1IDATM;
        private System.Windows.Forms.Button btnAccredit;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.DataGridView dgEndorseToAccounting;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Attendance;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName_Attendance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbAttendanceSearch;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtReceiver;
        private System.Windows.Forms.TextBox txtEndorser;
        private System.Windows.Forms.Label label27;
    }
}