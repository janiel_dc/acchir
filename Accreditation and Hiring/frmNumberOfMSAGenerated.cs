﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accreditation_And_Hiring
{
    public partial class frmNumberOfMSAGenerated : Form
    {
        public frmNumberOfMSAGenerated()
        {
            InitializeComponent();
        }

        private void frmNumberOfMSAGenerated_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'AccreditationHiringDataSet.DAL_GET_NumberOfGeneratedMSA' table. You can move, or remove it, as needed.
            this.DAL_GET_NumberOfGeneratedMSATableAdapter.Fill(this.AccreditationHiringDataSet.DAL_GET_NumberOfGeneratedMSA);

            this.reportViewer1.RefreshReport();
        }
    }
}
