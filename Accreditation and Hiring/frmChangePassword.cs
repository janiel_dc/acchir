﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmChangePassword : Form
    {
        public frmChangePassword()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            frmMain mainFrm = new frmMain();
            UserBL userBL = new UserBL();

            if ((txtNewPassword.Text != "" && txtRepeatNewPassword.Text != "") &&
                txtNewPassword.Text == txtRepeatNewPassword.Text)
            {
                userBL._userId = frmLogin.GetInstance()._cUserID;
                userBL._password = txtNewPassword.Text;
                UserDAL.updatePassword(userBL);
                MessageBox.Show("Password changed successfully.");
                Application.Restart();
            }
            else
            {
                MessageBox.Show("Invalid Entries.");
            }
        }

        private void frmChangePassword_Load(object sender, EventArgs e)
        {
           
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
