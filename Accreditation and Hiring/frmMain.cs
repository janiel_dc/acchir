﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataAccess;
using BusinessObjects;
using Accreditation_And_Hiring;

namespace Accreditation_and_Hiring_System
{
    public partial class frmMain : Form
    {
        private static frmMain _instance = null;
        public UserBL loggedEmployee = null;
        public static bool isLoggedIn = false;

        public static frmMain GetInstance()
        {
            frmLogin login = frmLogin.GetInstance();
            UserBL loggedUser = null;

             if (!isLoggedIn && !login.IsDisposed)
            {
                UserDAL.connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["ConnectionString"]);
                login.ShowDialog();

                if (login.DialogResult != DialogResult.Yes)
                {
                    isLoggedIn = true;
                    return _instance;
                }
                else
                {
                    isLoggedIn = false;
                    loggedUser = login._loggedInUser;
                    login.Close();
                }
            }
            
            loggedUser = login._loggedInUser;

            if (_instance == null)
            {
                _instance = new frmMain();
                _instance.loggedEmployee = loggedUser;
                _instance.loggedEmployee._userId = loggedUser._userId;
                _instance.statLoggedIn.Text = loggedUser._firstName;
            }
            else loggedUser = _instance.loggedEmployee;
            
            return _instance;
        }
        public frmMain()
        {
            InitializeComponent();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ans = MessageBox.Show("Are you sure you want to logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                Application.Restart();
            }
        }

        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDashboard frmDashboard = new frmDashboard();
            frmDashboard.WindowState = FormWindowState.Maximized;
            frmDashboard.Show();
            frmDashboard.MdiParent = this;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //frmDashboard frmDashboard = new frmDashboard();
            //frmDashboard.WindowState = FormWindowState.Maximized;
            //frmDashboard.Show();
            //frmDashboard.MdiParent = this;
        }
        #region HELPERS
        
        private void resultToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRevalidaResult frmRevalidaResult = new frmRevalidaResult();
            frmRevalidaResult.Show();
        }

        private void accountSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAccountSettings frmAccountSettings = new frmAccountSettings();
            frmAccountSettings.Show();
        }

        private void salesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageMembers frmManageMembers = new frmManageMembers();
            frmManageMembers.Show();
        }

        private void organicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManagePersonnel frmManagePersonnel = new frmManagePersonnel();
            frmManagePersonnel.Show();
        }

        private void manageAdminsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageAdmins frmManageAdmins = new frmManageAdmins();
            frmManageAdmins.Show();
        }

        private void salesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmManageSalesRequirements frmManageSalesRequirements = new frmManageSalesRequirements();
            frmManageSalesRequirements.Show();
        }

        private void organicToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageTrainings frmManageTrainings = new frmManageTrainings();
            frmManageTrainings.Show();
        }

        private void manageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmManageRevalida frmManageRevalida = new frmManageRevalida();
            frmManageRevalida.Show();
        }

        private void resultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTrainingResult frmTrainingResult = new frmTrainingResult();
            frmTrainingResult.Show();
        }

        private void salesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmManageSalesRequirements frmManageSalesRequirements = new frmManageSalesRequirements();
            frmManageSalesRequirements.Show();
        }

        private void organicToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmOrganicRequirements frmOrganicRequirements = new frmOrganicRequirements();
            frmOrganicRequirements.Show();
        }
    #endregion

        private void numberOfApplicantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTest test = new frmTest();
            test.Show();
        }

        private void manageRolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageRoles manageRoles = new frmManageRoles();
            manageRoles.Show();
        }

        private void applicantStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmApplicantStatusReport frmApplicantStatusReport = new frmApplicantStatusReport();
            frmApplicantStatusReport.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout frmAbout = new frmAbout();
            frmAbout.Show();
        }

        private void numberOfMarketingServicesAgreementGeneratedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNumberOfMSAGenerated frmNumberOfMSAGenerated = new frmNumberOfMSAGenerated();
            frmNumberOfMSAGenerated.Show();

        }

        private void trainingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTrainingStatusAndResult frmTrainingStatusAndResult = new frmTrainingStatusAndResult();
            frmTrainingStatusAndResult.Show();
        }
    }
}
