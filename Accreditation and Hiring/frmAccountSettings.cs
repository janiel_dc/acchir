﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmAccountSettings : Form
    {
        public frmAccountSettings()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAccountSettings_Load(object sender, EventArgs e)
        {
            //Getting user details of logged in user
            UserBL userBL = new UserBL();

            frmLogin login = frmLogin.GetInstance();
            userBL._userId = login._loggedInUser._userId;
            DataSet dsGetUserDetails = UserDAL.getUserDetailsById(userBL);

            //Personal Info
            txtFirstName.Text = dsGetUserDetails.Tables[0].Rows[0]["FirstName"].ToString();
            txtMiddleName.Text = dsGetUserDetails.Tables[0].Rows[0]["MiddleName"].ToString();
            txtLastName.Text = dsGetUserDetails.Tables[0].Rows[0]["LastName"].ToString();
            txtAddress1.Text = dsGetUserDetails.Tables[0].Rows[0]["Address1"].ToString();
            txtAddress2.Text = dsGetUserDetails.Tables[0].Rows[0]["Address2"].ToString();
            txtContact1.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
            txtContact2.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
            txtEmail.Text = dsGetUserDetails.Tables[0].Rows[0]["EmailAddress"].ToString();

            //Security Info
            txtPassword.Text = dsGetUserDetails.Tables[0].Rows[0]["Password"].ToString();
            txtChallengeQuestion.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeQuestion"].ToString();
            txtAnswer.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeAnswer"].ToString();

        }

        private void cbRevealAnswer_CheckedChanged(object sender, EventArgs e)
        {
            if (cbRevealAnswer.Checked == true)
            {
                txtAnswer.UseSystemPasswordChar = false;
            }
            else
            {
                txtAnswer.UseSystemPasswordChar = true;
            }
        }

        private void btnSavePersonalInfo_Click(object sender, EventArgs e)
        {
            //updating personal information of logged in user
            try
            {
                UserBL userBL = new UserBL();

                frmLogin login = frmLogin.GetInstance();

                userBL._firstName = txtFirstName.Text.Trim();
                userBL._middleName = txtMiddleName.Text.Trim();
                userBL._lastName = txtLastName.Text.Trim();
                userBL._address1 = txtAddress1.Text.Trim();
                userBL._address2 = txtAddress2.Text.Trim();
                userBL._contactNo1 = txtContact1.Text.Trim();
                userBL._contactNo2 = txtContact2.Text.Trim();
                userBL._emailAddress = txtEmail.Text.Trim();
                userBL._userId = login._loggedInUser._userId;
                userBL._lastModifiedUserId = login._loggedInUser._userId;
                UserDAL.updateUserPersonalInfo(userBL);
                MessageBox.Show("Personal information updated.");
            }
            catch (Exception)
            {
                MessageBox.Show("Error occured contact your administrator.");
            }
        }

        private void btnSaveAccountInfo_Click(object sender, EventArgs e)
        {
            //saving account information of logged in user.
            try
            {
                if (txtPassword.Text.Trim() == txtRepeatPassword.Text.Trim())
                {
                    if (txtChallengeQuestion.Text != "" && txtAnswer.Text != "")
                    {
                        UserBL userBL = new UserBL();

                        frmLogin login = frmLogin.GetInstance();

                        userBL._password = txtPassword.Text.Trim();
                        userBL._challengeQuestion = txtChallengeQuestion.Text.Trim();
                        userBL._challengeAnswer = txtAnswer.Text.Trim();
                        userBL._userId = login._loggedInUser._userId;
                        userBL._lastModifiedUserId = login._loggedInUser._userId;
                        UserDAL.updateUserAccountInfo(userBL);
                        MessageBox.Show("Account information updated.");
                    }
                    else
                    {
                        MessageBox.Show("Cannot save empty security question or answer.");
                    }
                }
                else
                {
                    MessageBox.Show("Password did not matched.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error occured contact your administrator.");
            }
        }
    }
}
