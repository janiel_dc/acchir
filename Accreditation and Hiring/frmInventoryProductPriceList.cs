﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Inventory_Management
{
    public partial class frmInventoryProductPriceList : Form
    {
        public frmInventoryProductPriceList()
        {
            InitializeComponent();
        }

        private void frmInventoryProductPriceList_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProductReport.DAL_GET_GetProductPriceList' table. You can move, or remove it, as needed.
            this.dAL_GET_GetProductPriceListTableAdapter.Fill(this.dataSetProductReport.DAL_GET_GetProductPriceList);

            this.rptViewerProductPriceList.RefreshReport();
        }

        private void frmInventoryProductPriceList_FormClosing(object sender, FormClosingEventArgs e)
        {
            //no load error handler
            rptViewerProductPriceList.LocalReport.ReleaseSandboxAppDomain();
        }
    }
}
