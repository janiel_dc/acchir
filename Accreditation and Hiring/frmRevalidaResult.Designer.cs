﻿namespace Accreditation_and_Hiring_System
{
    partial class frmRevalidaResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tcRevalida = new System.Windows.Forms.TabControl();
            this.tpParticipants = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbRevalidaDate = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbMemberName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpInProgress = new System.Windows.Forms.TabPage();
            this.cmbActiveRevalidaDate = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgActive = new System.Windows.Forms.DataGridView();
            this.RevalidaResultId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaForm_Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isPassed_Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Save_Active = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpFinished = new System.Windows.Forms.TabPage();
            this.dtpRevalidaDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dgFinished = new System.Windows.Forms.DataGridView();
            this.RevalidaId_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberId_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name_Finished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RevalidaForm_Finished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isPassed_Finished = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.tcRevalida.SuspendLayout();
            this.tpParticipants.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpInProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).BeginInit();
            this.tpFinished.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFinished)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(677, 33);
            this.label1.TabIndex = 12;
            this.label1.Text = "Revalida Result";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tcRevalida
            // 
            this.tcRevalida.Controls.Add(this.tpParticipants);
            this.tcRevalida.Controls.Add(this.tpInProgress);
            this.tcRevalida.Controls.Add(this.tpFinished);
            this.tcRevalida.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcRevalida.Location = new System.Drawing.Point(0, 33);
            this.tcRevalida.Name = "tcRevalida";
            this.tcRevalida.SelectedIndex = 0;
            this.tcRevalida.Size = new System.Drawing.Size(677, 372);
            this.tcRevalida.TabIndex = 11;
            // 
            // tpParticipants
            // 
            this.tpParticipants.Controls.Add(this.groupBox1);
            this.tpParticipants.Location = new System.Drawing.Point(4, 22);
            this.tpParticipants.Name = "tpParticipants";
            this.tpParticipants.Padding = new System.Windows.Forms.Padding(3);
            this.tpParticipants.Size = new System.Drawing.Size(669, 346);
            this.tpParticipants.TabIndex = 0;
            this.tpParticipants.Text = "Add Participants";
            this.tpParticipants.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbRevalidaDate);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.cmbMemberName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(141, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 136);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // cmbRevalidaDate
            // 
            this.cmbRevalidaDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRevalidaDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRevalidaDate.FormattingEnabled = true;
            this.cmbRevalidaDate.Location = new System.Drawing.Point(95, 45);
            this.cmbRevalidaDate.Name = "cmbRevalidaDate";
            this.cmbRevalidaDate.Size = new System.Drawing.Size(230, 21);
            this.cmbRevalidaDate.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(211, 99);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(114, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Add Participant";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbMemberName
            // 
            this.cmbMemberName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMemberName.FormattingEnabled = true;
            this.cmbMemberName.Location = new System.Drawing.Point(95, 72);
            this.cmbMemberName.Name = "cmbMemberName";
            this.cmbMemberName.Size = new System.Drawing.Size(230, 21);
            this.cmbMemberName.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Revalida Date:";
            // 
            // tpInProgress
            // 
            this.tpInProgress.Controls.Add(this.cmbActiveRevalidaDate);
            this.tpInProgress.Controls.Add(this.label4);
            this.tpInProgress.Controls.Add(this.dgActive);
            this.tpInProgress.Location = new System.Drawing.Point(4, 22);
            this.tpInProgress.Name = "tpInProgress";
            this.tpInProgress.Padding = new System.Windows.Forms.Padding(3);
            this.tpInProgress.Size = new System.Drawing.Size(669, 346);
            this.tpInProgress.TabIndex = 1;
            this.tpInProgress.Text = "In Progress";
            this.tpInProgress.UseVisualStyleBackColor = true;
            // 
            // cmbActiveRevalidaDate
            // 
            this.cmbActiveRevalidaDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActiveRevalidaDate.FormattingEnabled = true;
            this.cmbActiveRevalidaDate.Location = new System.Drawing.Point(489, 6);
            this.cmbActiveRevalidaDate.Name = "cmbActiveRevalidaDate";
            this.cmbActiveRevalidaDate.Size = new System.Drawing.Size(172, 21);
            this.cmbActiveRevalidaDate.TabIndex = 3;
            this.cmbActiveRevalidaDate.SelectionChangeCommitted += new System.EventHandler(this.cmbActiveRevalidaDate_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Revalida Date:";
            // 
            // dgActive
            // 
            this.dgActive.AllowUserToAddRows = false;
            this.dgActive.AllowUserToDeleteRows = false;
            this.dgActive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgActive.BackgroundColor = System.Drawing.Color.White;
            this.dgActive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RevalidaResultId_Active,
            this.RevalidaId_Active,
            this.MemberId_Active,
            this.Name_Active,
            this.RevalidaForm_Active,
            this.isPassed_Active,
            this.Save_Active});
            this.dgActive.Location = new System.Drawing.Point(8, 33);
            this.dgActive.Name = "dgActive";
            this.dgActive.RowHeadersVisible = false;
            this.dgActive.Size = new System.Drawing.Size(653, 307);
            this.dgActive.TabIndex = 0;
            this.dgActive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgActive_CellContentClick);
            // 
            // RevalidaResultId_Active
            // 
            this.RevalidaResultId_Active.DataPropertyName = "RevalidaResultId";
            this.RevalidaResultId_Active.HeaderText = "RevalidaResultId";
            this.RevalidaResultId_Active.Name = "RevalidaResultId_Active";
            this.RevalidaResultId_Active.Visible = false;
            // 
            // RevalidaId_Active
            // 
            this.RevalidaId_Active.DataPropertyName = "RevalidaId";
            this.RevalidaId_Active.HeaderText = "RevalidaId";
            this.RevalidaId_Active.Name = "RevalidaId_Active";
            this.RevalidaId_Active.Visible = false;
            // 
            // MemberId_Active
            // 
            this.MemberId_Active.DataPropertyName = "MemberId";
            this.MemberId_Active.HeaderText = "MemberId";
            this.MemberId_Active.Name = "MemberId_Active";
            this.MemberId_Active.Visible = false;
            // 
            // Name_Active
            // 
            this.Name_Active.DataPropertyName = "FullName";
            this.Name_Active.HeaderText = "Name";
            this.Name_Active.Name = "Name_Active";
            // 
            // RevalidaForm_Active
            // 
            this.RevalidaForm_Active.DataPropertyName = "isSubmittedRevalidaForm";
            this.RevalidaForm_Active.HeaderText = "Revalida Form";
            this.RevalidaForm_Active.Name = "RevalidaForm_Active";
            // 
            // isPassed_Active
            // 
            this.isPassed_Active.DataPropertyName = "isPassedInRevalida";
            this.isPassed_Active.HeaderText = "Passed";
            this.isPassed_Active.Name = "isPassed_Active";
            // 
            // Save_Active
            // 
            this.Save_Active.HeaderText = "";
            this.Save_Active.Name = "Save_Active";
            this.Save_Active.Text = "Save";
            this.Save_Active.UseColumnTextForButtonValue = true;
            // 
            // tpFinished
            // 
            this.tpFinished.Controls.Add(this.dtpRevalidaDate);
            this.tpFinished.Controls.Add(this.label5);
            this.tpFinished.Controls.Add(this.dgFinished);
            this.tpFinished.Location = new System.Drawing.Point(4, 22);
            this.tpFinished.Name = "tpFinished";
            this.tpFinished.Padding = new System.Windows.Forms.Padding(3);
            this.tpFinished.Size = new System.Drawing.Size(669, 346);
            this.tpFinished.TabIndex = 2;
            this.tpFinished.Text = "Finished";
            this.tpFinished.UseVisualStyleBackColor = true;
            // 
            // dtpRevalidaDate
            // 
            this.dtpRevalidaDate.Location = new System.Drawing.Point(478, 7);
            this.dtpRevalidaDate.Name = "dtpRevalidaDate";
            this.dtpRevalidaDate.Size = new System.Drawing.Size(183, 20);
            this.dtpRevalidaDate.TabIndex = 6;
            this.dtpRevalidaDate.ValueChanged += new System.EventHandler(this.dtpRevalidaDate_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(394, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Revalida Date:";
            // 
            // dgFinished
            // 
            this.dgFinished.AllowUserToAddRows = false;
            this.dgFinished.AllowUserToDeleteRows = false;
            this.dgFinished.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFinished.BackgroundColor = System.Drawing.Color.White;
            this.dgFinished.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFinished.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RevalidaId_Finished,
            this.MemberId_Finished,
            this.Name_Finished,
            this.RevalidaForm_Finished,
            this.isPassed_Finished});
            this.dgFinished.Location = new System.Drawing.Point(8, 33);
            this.dgFinished.Name = "dgFinished";
            this.dgFinished.ReadOnly = true;
            this.dgFinished.RowHeadersVisible = false;
            this.dgFinished.Size = new System.Drawing.Size(653, 307);
            this.dgFinished.TabIndex = 4;
            // 
            // RevalidaId_Finished
            // 
            this.RevalidaId_Finished.DataPropertyName = "RevalidaId";
            this.RevalidaId_Finished.HeaderText = "RevalidaId";
            this.RevalidaId_Finished.Name = "RevalidaId_Finished";
            this.RevalidaId_Finished.ReadOnly = true;
            this.RevalidaId_Finished.Visible = false;
            // 
            // MemberId_Finished
            // 
            this.MemberId_Finished.DataPropertyName = "MemberId";
            this.MemberId_Finished.HeaderText = "MemberId";
            this.MemberId_Finished.Name = "MemberId_Finished";
            this.MemberId_Finished.ReadOnly = true;
            this.MemberId_Finished.Visible = false;
            // 
            // Name_Finished
            // 
            this.Name_Finished.DataPropertyName = "FullName";
            this.Name_Finished.HeaderText = "Name";
            this.Name_Finished.Name = "Name_Finished";
            this.Name_Finished.ReadOnly = true;
            // 
            // RevalidaForm_Finished
            // 
            this.RevalidaForm_Finished.DataPropertyName = "isSubmittedRevalidaForm";
            this.RevalidaForm_Finished.HeaderText = "Revalida Form";
            this.RevalidaForm_Finished.Name = "RevalidaForm_Finished";
            this.RevalidaForm_Finished.ReadOnly = true;
            // 
            // isPassed_Finished
            // 
            this.isPassed_Finished.DataPropertyName = "isPassedInRevalida";
            this.isPassed_Finished.HeaderText = "Passed";
            this.isPassed_Finished.Name = "isPassed_Finished";
            this.isPassed_Finished.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 405);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(677, 34);
            this.panel2.TabIndex = 13;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(597, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmRevalidaResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(677, 439);
            this.Controls.Add(this.tcRevalida);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Name = "frmRevalidaResult";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Revalida Result";
            this.Load += new System.EventHandler(this.frmRevalidaResult_Load);
            this.tcRevalida.ResumeLayout(false);
            this.tpParticipants.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpInProgress.ResumeLayout(false);
            this.tpInProgress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).EndInit();
            this.tpFinished.ResumeLayout(false);
            this.tpFinished.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFinished)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tcRevalida;
        private System.Windows.Forms.TabPage tpParticipants;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbRevalidaDate;
        private System.Windows.Forms.ComboBox cmbMemberName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpInProgress;
        private System.Windows.Forms.TabPage tpFinished;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dgActive;
        private System.Windows.Forms.ComboBox cmbActiveRevalidaDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgFinished;
        private System.Windows.Forms.DateTimePicker dtpRevalidaDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaResultId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name_Active;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RevalidaForm_Active;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPassed_Active;
        private System.Windows.Forms.DataGridViewButtonColumn Save_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn RevalidaId_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId_Finished;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name_Finished;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RevalidaForm_Finished;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isPassed_Finished;
    }
}