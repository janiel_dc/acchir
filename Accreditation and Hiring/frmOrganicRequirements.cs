﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmOrganicRequirements : Form
    {
        private int _memberId = 0, _organicDocumentsId = 0;

        public frmOrganicRequirements()
        {
            InitializeComponent();
        }

        private void frmOrganicRequirements_Load(object sender, EventArgs e)
        {
            loadDataGrid();
            if (dgMembersList.Rows.Count > 0)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[0].Cells[0].Value);
            }
            else
            {
                _memberId = 0;
            }
            viewMemberDocumentDetails();
        }

        private void loadDataGrid()
        {
            DataSet dsMembersList = TrainingResultsDAL.getUnhiredList();
            dgMembersList.AutoGenerateColumns = false;
            dgMembersList.DataSource = dsMembersList.Tables[0];
        }

        private void viewMemberDocumentDetails()
        {
            try
            {
                SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                salesPersonnelBL._memberId = _memberId;

                DataSet dsOrganicDocuments = OrganicPersonnelDAL.organicDocumentsByMemberId(salesPersonnelBL);
                _organicDocumentsId = Convert.ToInt32(dsOrganicDocuments.Tables[0].Rows[0]["OrganicDocumentsId"]);
                txtTINNo.Text = dsOrganicDocuments.Tables[0].Rows[0]["TINNo"].ToString();
                txtPagIbigNo.Text = dsOrganicDocuments.Tables[0].Rows[0]["PagIbigNo"].ToString();
                txtPhilHealthNo.Text = dsOrganicDocuments.Tables[0].Rows[0]["PhilhealthNo"].ToString();
                txtSSSNo.Text = dsOrganicDocuments.Tables[0].Rows[0]["SSSNo"].ToString();

                if (dsOrganicDocuments.Tables[0].Rows[0]["DateHired"].ToString() == "")
                {
                    dtpDateHired.Value = DateTime.Now;
                    cbHired.Checked = false;
                }
                else
                {
                    dtpDateHired.Value = Convert.ToDateTime(dsOrganicDocuments.Tables[0].Rows[0]["DateHired"]);
                    cbHired.Checked = true;
                }

                if (Convert.ToBoolean(dsOrganicDocuments.Tables[0].Rows[0]["2x2Pic"]) == true)
                    cb2x2Pic.Checked = true;
                else
                    cb2x2Pic.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator." + ex.ToString());
                throw;
            }
        }

        private void dgMembersList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[e.RowIndex].Cells[0].Value);
                viewMemberDocumentDetails();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbHired_CheckedChanged(object sender, EventArgs e)
        {
            if (cbHired.Checked)
                dtpDateHired.Enabled = true;
            else
                dtpDateHired.Enabled = false;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string searchString = txtSearch.Text.Trim();

            DataSet dsMembersList = OrganicPersonnelDAL.searchOrganicDocuments(searchString);
            dgMembersList.AutoGenerateColumns = false;
            dgMembersList.DataSource = dsMembersList.Tables[0];

            if (dsMembersList.Tables[0].Rows.Count > 0)
            {
                _memberId = Convert.ToInt32(dgMembersList.Rows[0].Cells[0].Value);
                viewMemberDocumentDetails();
                btnSave.Enabled = true;
            }
            else
            {
                _memberId = 0;
                btnSave.Enabled = false;
                txtTINNo.Text = "";
                txtPagIbigNo.Text = "";
                txtPhilHealthNo.Text = "";
                txtSSSNo.Text = "";
                cbHired.Checked = false;
                cb2x2Pic.Checked = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_organicDocumentsId == 0)
            {
                MessageBox.Show("No selected organic document set.");
            }
            else
            {
                updateSalesRequirements();
            }
        }

        private void updateSalesRequirements()
        {
            try
            {
                OrganicDocumentsBL organicDocumentsBL = new OrganicDocumentsBL();
                frmLogin login = frmLogin.GetInstance();

                organicDocumentsBL._SSSNo = txtSSSNo.Text.Trim();
                organicDocumentsBL._pagIbigNo = txtPagIbigNo.Text.Trim();
                organicDocumentsBL._TINNo = txtTINNo.Text.Trim();
                organicDocumentsBL._philhealthNo = txtPhilHealthNo.Text.Trim();
                organicDocumentsBL._lastModifiedUserId = login._loggedInUser._userId;
                organicDocumentsBL._organicDocumentsId = _organicDocumentsId;

                if (cbHired.Checked)
                    organicDocumentsBL._dateHired = dtpDateHired.Value.ToString();
                else
                    organicDocumentsBL._dateHired = null;

                if (cb2x2Pic.Checked)
                    organicDocumentsBL._2x2Pic = true;
                else
                    organicDocumentsBL._2x2Pic = false;

                OrganicPersonnelDAL.updateOrganicDocuments(organicDocumentsBL);
                MessageBox.Show("Organic documents successfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("error occured contact your administrator." + ex.ToString());
                throw;
            }
        }

        #region HELPERS

        private static void phoneValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46 && e.KeyChar != 43)
                e.Handled = true;

            if (e.KeyChar == 43)
            {
                if ((sender as TextBox).Text.Contains("+"))
                    e.Handled = true;
            }
        }

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 32)
                e.Handled = true;
        }

        #endregion

        private void txtTINNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtSSSNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtPagIbigNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtPhilHealthNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

    }
}
