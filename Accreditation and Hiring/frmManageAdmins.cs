﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManageAdmins : Form
    {
        private int _userId = 0;

        private bool _isMale = true;

        public frmManageAdmins()
        {
            InitializeComponent();
        }

        private void tcManageAdmins_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcManageAdmins.SelectedIndex == 1)
            {
                DataSet dsAdminList = UserDAL.getAdminLists();
                dgActive.AutoGenerateColumns = false;
                dgActive.DataSource = dsAdminList.Tables[0];

                btnSave.Visible = false;
                clear();
            }
            else if (tcManageAdmins.SelectedIndex == 2)
            {
                DataSet dsAdminList = UserDAL.getAdminLists();
                dgInactive.AutoGenerateColumns = false;
                dgInactive.DataSource = dsAdminList.Tables[1];

                btnSave.Visible = true;
                btnSave.Text = "Delete";
                btnSave.BackColor = Color.Red;

                clear();
            }
            else if (tcManageAdmins.SelectedIndex == 0)
            {
                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnSave.BackColor = Color.Green;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text.Trim() != "" && dtpBirthDate.Value != null &&
                txtAddress1.Text.Trim() != "" && txtContact1.Text != "" && txtEmailAdd.Text.Trim() !=""
                && txtUserName.Text.Trim() != "" && txtPassword.Text.Trim() != "" 
                && txtChallengeQuestion.Text.Trim() != "" && txtChallengeAnswer.Text.Trim() != "")
            {
                UserBL userBL = new UserBL();
                userBL._userName = txtUserName.Text.ToLower().Trim();

                DataSet dsGetUserDetailsByUserName = UserDAL.getUserDetailsByUserName(userBL);

                    if (_userId == 0 && tcManageAdmins.SelectedIndex != 2)
                    {
                        if (dsGetUserDetailsByUserName.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("Username already exist");
                        }
                        else
                        {
                            savingNewUser();
                        }
                    }
                    else if(_userId !=0 && tcManageAdmins.SelectedIndex != 2)
                    {
                        updateUser();
                    }
                }
            else if (txtFirstName.Text.Trim() == ""&& txtAddress1.Text.Trim() == "" 
                && txtContact1.Text == "" && txtEmailAdd.Text.Trim() ==""
                && txtUserName.Text.Trim() == "" && txtPassword.Text.Trim() == "" 
                && txtChallengeQuestion.Text.Trim() == "" && txtChallengeAnswer.Text.Trim() == ""
                && tcManageAdmins.SelectedIndex == 2)
            {
                deleteUser();
            }
            else
            {
                MessageBox.Show("Please complete information.");
            }
        }

        private void deleteUser()
        {
            UserBL userBL = new UserBL();
            frmLogin login = frmLogin.GetInstance();

            string ans = MessageBox.Show("Are you sure you want to delete this user?", "Delete User", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                try
                {
                    int rowIndex = dgInactive.CurrentCell.RowIndex;
                    userBL._userId = Convert.ToInt32(dgInactive.Rows[rowIndex].Cells["UserId_Inactive"].Value);
                    userBL._lastModifiedUserId = login._loggedInUser._userId;
                    UserDAL.deleteUser(userBL);
                    MessageBox.Show("User deleted successfully");

                    DataSet dsAdminList = UserDAL.getAdminLists();
                    dgInactive.AutoGenerateColumns = false;
                    dgInactive.DataSource = dsAdminList.Tables[1];

                }
                catch (Exception)// ex)
                {
                    MessageBox.Show("An error occured, please contact your Administrator.");
                }
            }
        }

        private void updateUser()
        {
            try
            {
                UserBL userBL = new UserBL();

                frmLogin login = frmLogin.GetInstance();

                userBL._userId = _userId;
                userBL._firstName = txtFirstName.Text.Trim();
                userBL._middleName = txtMiddleName.Text.Trim();
                userBL._lastName = txtLastName.Text.Trim();
                userBL._birthDate = dtpBirthDate.Value.ToString();
                userBL._address1 = txtAddress1.Text.Trim();
                userBL._address2 = txtAddress2.Text.Trim();
                userBL._contactNo1 = txtContact1.Text.Trim();
                userBL._contactNo2 = txtContact2.Text.Trim();
                userBL._emailAddress = txtEmailAdd.Text.Trim();
                userBL._password = txtPassword.Text.Trim();
                userBL._challengeQuestion = txtChallengeQuestion.Text.Trim();
                userBL._challengeAnswer = txtChallengeAnswer.Text.Trim();
                userBL._lastModifiedUserId = login._loggedInUser._userId;
                if (cbActive.Checked)
                {
                    userBL._isActive = true;
                }
                else
                {
                    userBL._isActive = false;
                }

                UserDAL.updateUserGeneralInfo(userBL);
                MessageBox.Show("User updated successfuly");

                if (cbActive.Checked)
                {
                    tcManageAdmins.SelectedIndex = 1;
                }
                else
                {
                    tcManageAdmins.SelectedIndex = 2;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error occured contact your administrator.");
            }
        }

        private void savingNewUser()
        {
            try
            {
                UserBL userBL = new UserBL();

                frmLogin login = frmLogin.GetInstance();

                userBL._firstName = txtFirstName.Text.Trim();
                userBL._middleName = txtMiddleName.Text.Trim();
                userBL._lastName = txtLastName.Text.Trim();
                userBL._gender = _isMale;
                userBL._birthDate = dtpBirthDate.Value.ToString();
                userBL._address1 = txtAddress1.Text.Trim();
                userBL._address2 = txtAddress2.Text.Trim();
                userBL._contactNo1 = txtContact1.Text.Trim();
                userBL._contactNo2 = txtContact2.Text.Trim();
                userBL._emailAddress = txtEmailAdd.Text.Trim();
                userBL._userName = txtUserName.Text.Trim();
                userBL._password = txtPassword.Text.Trim();
                userBL._challengeQuestion = txtChallengeQuestion.Text.Trim();
                userBL._challengeAnswer = txtChallengeAnswer.Text.Trim();
                userBL._lastModifiedUserId = login._loggedInUser._userId;
                if(cbActive.Checked)
                {
                    userBL._isActive = true;
                }
                else
                {
                    userBL._isActive = false;
                }

                UserDAL.insertNewUser(userBL);
                MessageBox.Show("User saved successfuly");

                if (cbActive.Checked)
                {
                    tcManageAdmins.SelectedIndex = 1;
                }
                else
                {
                    tcManageAdmins.SelectedIndex = 2;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error occured contact your administrator.");
            }
        }

        private void rbtnMale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnMale.Checked && !rbtnFemale.Checked)
            {
                _isMale = true;
            }
            else
            {
                _isMale = false;
            }
        }

        private void frmManageAdmins_Load(object sender, EventArgs e)
        {

        }

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgActive.Rows[e.RowIndex].Cells["Edit_Active"] == dgActive.CurrentCell)
            {
                UserBL userBL = new UserBL();
                userBL._userId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["UserId_Active"].Value);

                DataSet dsGetUserDetails = UserDAL.getUserDetailsById(userBL);

                tcManageAdmins.SelectedIndex = 0;

                //Personal Info
                _userId = Convert.ToInt32(dsGetUserDetails.Tables[0].Rows[0]["UserId"]);
                txtFirstName.Text = dsGetUserDetails.Tables[0].Rows[0]["FirstName"].ToString();
                txtMiddleName.Text = dsGetUserDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                txtLastName.Text = dsGetUserDetails.Tables[0].Rows[0]["LastName"].ToString();
                dtpBirthDate.Value = Convert.ToDateTime(dsGetUserDetails.Tables[0].Rows[0]["BirthDate"]);
                txtAddress1.Text = dsGetUserDetails.Tables[0].Rows[0]["Address1"].ToString();
                txtAddress2.Text = dsGetUserDetails.Tables[0].Rows[0]["Address2"].ToString();
                txtContact1.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                txtContact2.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                txtEmailAdd.Text = dsGetUserDetails.Tables[0].Rows[0]["EmailAddress"].ToString();

                if (Convert.ToBoolean(dsGetUserDetails.Tables[0].Rows[0]["Gender"]) == true)
                {
                    rbtnMale.Checked = true;
                    rbtnFemale.Checked = false;
                }
                else
                {
                    rbtnMale.Checked = false;
                    rbtnFemale.Checked = true;
                }

                //Security Info
                txtUserName.Text = dsGetUserDetails.Tables[0].Rows[0]["UserName"].ToString();
                txtPassword.Text = dsGetUserDetails.Tables[0].Rows[0]["Password"].ToString();
                txtChallengeQuestion.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeQuestion"].ToString();
                txtChallengeAnswer.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeAnswer"].ToString();
                cbActive.Checked = true;
                rbtnMale.Enabled = false;
                rbtnFemale.Enabled = false;
                txtUserName.Enabled = false;
            }
        }

        private void dgInactive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgInactive.Rows[e.RowIndex].Cells["Edit_Inactive"] == dgInactive.CurrentCell)
            {
                UserBL userBL = new UserBL();
                userBL._userId = Convert.ToInt32(dgInactive.Rows[e.RowIndex].Cells["UserId_Inactive"].Value);

                DataSet dsGetUserDetails = UserDAL.getUserDetailsById(userBL);

                tcManageAdmins.SelectedIndex = 0;

                //Personal Info
                _userId = Convert.ToInt32(dsGetUserDetails.Tables[0].Rows[0]["UserId"]);
                txtFirstName.Text = dsGetUserDetails.Tables[0].Rows[0]["FirstName"].ToString();
                txtMiddleName.Text = dsGetUserDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                txtLastName.Text = dsGetUserDetails.Tables[0].Rows[0]["LastName"].ToString();
                dtpBirthDate.Value = Convert.ToDateTime(dsGetUserDetails.Tables[0].Rows[0]["BirthDate"]);
                txtAddress1.Text = dsGetUserDetails.Tables[0].Rows[0]["Address1"].ToString();
                txtAddress2.Text = dsGetUserDetails.Tables[0].Rows[0]["Address2"].ToString();
                txtContact1.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                txtContact2.Text = dsGetUserDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                txtEmailAdd.Text = dsGetUserDetails.Tables[0].Rows[0]["EmailAddress"].ToString();

                if (Convert.ToBoolean(dsGetUserDetails.Tables[0].Rows[0]["Gender"]) == true)
                {
                    rbtnMale.Checked = true;
                    rbtnFemale.Checked = false;
                }
                else
                {
                    rbtnMale.Checked = false;
                    rbtnFemale.Checked = true;
                }

                //Security Info
                txtUserName.Text = dsGetUserDetails.Tables[0].Rows[0]["UserName"].ToString();
                txtPassword.Text = dsGetUserDetails.Tables[0].Rows[0]["Password"].ToString();
                txtChallengeQuestion.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeQuestion"].ToString();
                txtChallengeAnswer.Text = dsGetUserDetails.Tables[0].Rows[0]["ChallengeAnswer"].ToString();
                cbActive.Checked = false;
                rbtnMale.Enabled = false;
                rbtnFemale.Enabled = false;
                txtUserName.Enabled = false;
            }
        }

        #region HELPERS

        private static void phoneValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46 && e.KeyChar != 43)
                e.Handled = true;

            if (e.KeyChar == 43)
            {
                if ((sender as TextBox).Text.Contains("+"))
                    e.Handled = true;
            }
        }

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 32)
                e.Handled = true;
        }

        private void clear()
        {
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtLastName.Text = "";
            rbtnMale.Checked = true;
            dtpBirthDate.Value = DateTime.Now;
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtContact1.Text = "";
            txtContact2.Text = "";
            txtEmailAdd.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtChallengeQuestion.Text = "";
            txtChallengeAnswer.Text = "";
            cbActive.Checked = true;
        }

        #endregion

        private void txtContact1_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }

        private void txtContact2_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }
    }
}
