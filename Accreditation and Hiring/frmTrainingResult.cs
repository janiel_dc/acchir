﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmTrainingResult : Form
    {
        private int _trainingId = 0, _finishedTrainingId = 0;

        public frmTrainingResult()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void populateDropDowns()
        {
            //Set DataSet
            DataSet dsGetActiveTrainings = TrainingDAL.getActiveTrainingList();
            DataSet dsGetUnaccreditedSalesPersonnel = TrainingResultsDAL.getUnaccreditedList();

            //Populate DropDownList
            cmbTrainingName.DataSource = dsGetActiveTrainings.Tables[0];
            cmbTrainingName.DisplayMember = "TrainingNameOrRemarks";
            cmbTrainingName.ValueMember = "TrainingId";

            cmbTrainingNameList.DataSource = dsGetActiveTrainings.Tables[0];
            cmbTrainingNameList.DisplayMember = "TrainingNameOrRemarks";
            cmbTrainingNameList.ValueMember = "TrainingId";

            cmbTrainingListFinished.DataSource = dsGetActiveTrainings.Tables[1];
            cmbTrainingListFinished.DisplayMember = "TrainingNameOrRemarks";
            cmbTrainingListFinished.ValueMember = "TrainingId";

            cmbMemberName.DataSource = dsGetUnaccreditedSalesPersonnel.Tables[0];
            cmbMemberName.DisplayMember = "FullName";
            cmbMemberName.ValueMember = "MemberId";

        }

        private void frmTrainingResult_Load(object sender, EventArgs e)
        {
            populateDropDowns();
            _trainingId = Convert.ToInt32(cmbTrainingNameList.SelectedValue);
            _finishedTrainingId = Convert.ToInt32(cmbTrainingListFinished.SelectedValue);
            setDatagrids();
        }

        private void setDatagrids()
        {
            TrainingResultBL trainingResultBL = new TrainingResultBL();
            TrainingResultBL trainingResultFinishedBL = new TrainingResultBL();

            trainingResultBL._trainingId = _trainingId;
            trainingResultFinishedBL._trainingId = _finishedTrainingId;

            DataSet dsTrainingDetails = TrainingResultsDAL.getTrainingDetailsByTrainingId(trainingResultBL);
            dgManageResult.AutoGenerateColumns = false;
            dgManageResult.DataSource = dsTrainingDetails.Tables[0];

            DataSet dsFinishedTrainingDetails = TrainingResultsDAL.getTrainingDetailsByTrainingId(trainingResultFinishedBL);
            dgFinishedTraining.AutoGenerateColumns = false;
            dgFinishedTraining.DataSource = dsFinishedTrainingDetails.Tables[0];
        }

        private void btnAddParticipant_Click(object sender, EventArgs e)
        {
            frmLogin login = frmLogin.GetInstance();

            try
            {
                TrainingResultBL trainingResultBL = new TrainingResultBL();
                trainingResultBL._trainingId = Convert.ToInt32(cmbTrainingName.SelectedValue);
                trainingResultBL._memberId = Convert.ToInt32(cmbMemberName.SelectedValue);
                trainingResultBL._lastModifiedUserId = login._loggedInUser._userId;

                DataSet dsExistingTrainingResultDetails = TrainingResultsDAL.getExistingTrainingDetails(trainingResultBL);
                if (dsExistingTrainingResultDetails.Tables[0].Rows.Count == 0)
                {
                    TrainingResultsDAL.insertNewTrainingResult(trainingResultBL);
                    MessageBox.Show("Participant added.");
                }
                else
                {
                    MessageBox.Show("Entry already saved.");
                }
                
            }
            catch (Exception)// ex)
            {
                MessageBox.Show("Error occured please contact your administrator");
                throw;
            }
        }

        private void tcTrainingResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTrainingResult.SelectedIndex == 1)
            {
                setDatagrids();
            }
        }

        private void cmbTrainingNameList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _trainingId = Convert.ToInt32(cmbTrainingNameList.SelectedValue);
            setDatagrids();
        }

        private void dtgManageResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (Convert.ToInt32(dgManageResult.Rows[e.RowIndex].Cells["ExamScore_Result"].Value) <=
                    Convert.ToInt32(dgManageResult.Rows[e.RowIndex].Cells["ExamItems_Result"].Value))
                {
                    if (dgManageResult.Rows[e.RowIndex].Cells["Save_Result"] == dgManageResult.CurrentCell)
                    {
                        try
                        {
                            TrainingResultBL trainingResultBL = new TrainingResultBL();
                            frmLogin login = frmLogin.GetInstance();

                            trainingResultBL._trainingId = Convert.ToInt32(dgManageResult.Rows[e.RowIndex].Cells["TrainingId_Result"].Value);
                            trainingResultBL._memberId = Convert.ToInt32(dgManageResult.Rows[e.RowIndex].Cells["MemberId_Result"].Value);
                            trainingResultBL._examScore = Convert.ToInt32(dgManageResult.Rows[e.RowIndex].Cells["ExamScore_Result"].Value);
                            trainingResultBL._lastModifiedUserId = login._loggedInUser._userId;

                            if (Convert.ToBoolean(dgManageResult.Rows[e.RowIndex].Cells["Day1_Result"].Value) == true)
                                trainingResultBL._isAttendedDay1 = true;
                            else
                                trainingResultBL._isAttendedDay1 = false;

                            if (Convert.ToBoolean(dgManageResult.Rows[e.RowIndex].Cells["Day2_Result"].Value) == true)
                                trainingResultBL._isAttendedDay2 = true;
                            else
                                trainingResultBL._isAttendedDay2 = false;

                            if (Convert.ToBoolean(dgManageResult.Rows[e.RowIndex].Cells["Pass_Result"].Value) == true)
                                trainingResultBL._isPassed = true;
                            else
                                trainingResultBL._isPassed = false;

                            TrainingResultsDAL.updateTrainingStatus(trainingResultBL);
                            MessageBox.Show("Changes successfully saved.");
                            dgManageResult.Rows[e.RowIndex].Cells["ExamScore_Result"].Style.BackColor = Color.LightGray;
                            setDatagrids();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error occured contact your administrator." + ex.ToString());
                            throw;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Unable to save. Score exceeds exam items.");
                    dgManageResult.Rows[e.RowIndex].Cells["ExamScore_Result"].Style.BackColor = Color.Red;
                }
            }
        }

        private void cmbTrainingListFinished_SelectionChangeCommitted(object sender, EventArgs e)
        {
            _finishedTrainingId = Convert.ToInt32(cmbTrainingListFinished.SelectedValue);
            setDatagrids();
        }
    }
}
