﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;
using BusinessObjects;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManagePersonnel : Form
    {
        private int _organicId = 0;

        public frmManagePersonnel()
        {
            InitializeComponent();
        }

        private void tcManagePersonnel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcManagePersonnel.SelectedIndex == 2)
            {
                DataSet dsMemberList = OrganicPersonnelDAL.getOrganicPersonnelList();
                dgInactive.AutoGenerateColumns = false;
                dgInactive.DataSource = dsMemberList.Tables[1];

                btnSave.Visible = true;
                btnSave.Text = "Delete";
                btnSave.BackColor = Color.Red;
                clear();
            }
            else if (tcManagePersonnel.SelectedIndex == 1)
            {
                DataSet dsMemberList = OrganicPersonnelDAL.getOrganicPersonnelList();
                dgActive.AutoGenerateColumns = false;
                dgActive.DataSource = dsMemberList.Tables[0];

                btnSave.Visible = false;
                clear();
            }
            else
            {
                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnSave.BackColor = Color.Green;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tcManagePersonnel.SelectedIndex != 2 && _organicId > 0)
            {
                if (txtFirstName.Text != "" && txtCitizenship.Text != "" && txtContactNo1.Text != "")
                {
                    updateMember();

                    if (cbActive.Checked)
                        tcManagePersonnel.SelectedIndex = 1;
                    else if (!cbActive.Checked)
                        tcManagePersonnel.SelectedIndex = 2;
                    else
                        tcManagePersonnel.SelectedIndex = 0;
                    clear();
                }
                else
                {
                    MessageBox.Show("Compelete required information.");
                }
            }
            else if (tcManagePersonnel.SelectedIndex != 2 && _organicId == 0)
            {
                if (txtFirstName.Text != "" && txtCitizenship.Text != "" && txtContactNo1.Text != "")
                {
                    insertNewMember();

                    if (cbActive.Checked)
                        tcManagePersonnel.SelectedIndex = 1;
                    else if (!cbActive.Checked)
                        tcManagePersonnel.SelectedIndex = 2;
                    else
                        tcManagePersonnel.SelectedIndex = 0;
                    clear();
                }
                else
                {
                    MessageBox.Show("Compelete required information.");
                }
            }
            else if (tcManagePersonnel.SelectedIndex == 2)
            {
                deleteMember();
            }
        }

        private void deleteMember()
        {
            SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
            frmLogin login = frmLogin.GetInstance();

            string ans = MessageBox.Show("Are you sure you want to delete this personnel?", "Delete Organic Personnel", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                try
                {
                    int rowIndex = dgInactive.CurrentCell.RowIndex;
                    salesPersonnelBL._memberId = Convert.ToInt32(dgInactive.Rows[rowIndex].Cells["MemberId_Inactive"].Value);
                    salesPersonnelBL._lastModifiedUserId = login._loggedInUser._userId;
                    SalesPersonnelDAL.deleteMember(salesPersonnelBL);
                    MessageBox.Show("Member deleted successfully");

                    DataSet dsSalesPersonnelList = OrganicPersonnelDAL.getOrganicPersonnelList();
                    dgInactive.AutoGenerateColumns = false;
                    dgInactive.DataSource = dsSalesPersonnelList.Tables[1];

                }
                catch (Exception)// ex)
                {
                    MessageBox.Show("An error occured, please contact your Administrator.");
                }
            }
        }

        private void updateMember()
        {
            try
            {
                SalesPersonnelBL memberBL = new SalesPersonnelBL();
                frmLogin login = frmLogin.GetInstance();

                memberBL._memberId = _organicId;
                memberBL._memberTypeId = 2;
                memberBL._firstName = txtFirstName.Text.Trim();
                memberBL._middleName = txtMiddleName.Text.Trim();
                memberBL._lastName = txtLastName.Text.Trim();
                memberBL._civilStatus = cmbCivilStatus.Text.Trim();
                memberBL._religion = txtReligion.Text.Trim();
                memberBL._citizenship = txtCitizenship.Text.Trim();
                memberBL._birthDate = dtpBirthdate.Value.ToString(); ;
                memberBL._birthPlace = txtBirthPlace.Text.Trim();
                memberBL._contactNo1 = txtContactNo1.Text.Trim();
                memberBL._contactNo2 = txtContactNo2.Text.Trim();
                memberBL._emailAddress = txtEmailAdd.Text.Trim();
                memberBL._addressNo = txtAddressNo.Text.Trim();
                memberBL._addressStreet = txtStreetNo.Text.Trim();
                memberBL._addressVillageOrSubdivision = txtSubdivision.Text.Trim();
                memberBL._addressBarangayOrDistrict = txtBarangay.Text.Trim();
                memberBL._addressTownOrCity = txtCity.Text.Trim();
                memberBL._addressPostalCode = txtPostalCode.Text.Trim();
                memberBL._lastModifiedUserId = login._loggedInUser._userId;
                memberBL._positionDesired = txtPosition.Text.Trim();
                memberBL._dateOfApplication = dtpDateOfApplication.Value.ToString();

                if (rbtnMale.Checked && !rbtnFemale.Checked)
                    memberBL._gender = true;
                else
                    memberBL._gender = false;

                if (cbActive.Checked)
                    memberBL._isActive = true;
                else
                    memberBL._isActive = false;

                OrganicPersonnelDAL.updateNewMember(memberBL);
                MessageBox.Show("Organic personnel successfully updated.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured contact your administrator." + ex.ToString());
            }

        }

        private void insertNewMember()
        {
            try
            {
                SalesPersonnelBL memberBL = new SalesPersonnelBL();
                OrganicDocumentsBL organicDocumentsBL = new OrganicDocumentsBL();
                frmLogin login = frmLogin.GetInstance();

                memberBL._memberTypeId = 2;
                memberBL._firstName = txtFirstName.Text.Trim();
                memberBL._middleName = txtMiddleName.Text.Trim();
                memberBL._lastName = txtLastName.Text.Trim();
                memberBL._civilStatus = cmbCivilStatus.Text.Trim();
                memberBL._religion = txtReligion.Text.Trim();
                memberBL._citizenship = txtCitizenship.Text.Trim();
                memberBL._birthDate = dtpBirthdate.Value.ToString();;
                memberBL._birthPlace = txtBirthPlace.Text.Trim();
                memberBL._contactNo1 = txtContactNo1.Text.Trim();
                memberBL._contactNo2 = txtContactNo2.Text.Trim();
                memberBL._emailAddress = txtEmailAdd.Text.Trim();
                memberBL._addressNo = txtAddressNo.Text.Trim();
                memberBL._addressStreet = txtStreetNo.Text.Trim();
                memberBL._addressVillageOrSubdivision = txtSubdivision.Text.Trim();
                memberBL._addressBarangayOrDistrict = txtBarangay.Text.Trim();
                memberBL._addressTownOrCity = txtCity.Text.Trim();
                memberBL._addressPostalCode = txtPostalCode.Text.Trim();
                memberBL._createdUserId = login._loggedInUser._userId;
                memberBL._lastModifiedUserId = login._loggedInUser._userId;
                //Parameters for organic documents
                organicDocumentsBL._SSSNo = txtSSSNo.Text.Trim();
                organicDocumentsBL._pagIbigNo = txtPagIbigNo.Text.Trim();
                organicDocumentsBL._TINNo = txtTINNo.Text.Trim();
                organicDocumentsBL._philhealthNo = txtPhilHealthNo.Text.Trim();
                memberBL._positionDesired = txtPosition.Text.Trim();
                memberBL._dateOfApplication = dtpDateOfApplication.Value.ToString();

                if (cbPic.Checked)
                    organicDocumentsBL._2x2Pic = true;
                else
                    organicDocumentsBL._2x2Pic = false;

                if (rbtnMale.Checked && !rbtnFemale.Checked)
                    memberBL._gender = true;
                else
                    memberBL._gender = false;

                if (cbActive.Checked)
                    memberBL._isActive = true;
                else
                    memberBL._isActive = false;

                OrganicPersonnelDAL.insertNewMember(memberBL,organicDocumentsBL);
                MessageBox.Show("New organic personnel successfully added.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured contact your administrator." + ex.ToString());
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region HELPERS

        private static void phoneValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46 && e.KeyChar != 43)
                e.Handled = true;

            if (e.KeyChar == 43)
            {
                if ((sender as TextBox).Text.Contains("+"))
                    e.Handled = true;
            }
        }

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 32)
                e.Handled = true;
        }

        private void cbSSS_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSSS.Checked)
                txtSSSNo.Enabled = true;
            else
                txtSSSNo.Enabled = false;
                txtSSSNo.Text = "";
        }

        private void cbPagIbig_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPagIbig.Checked)
                txtPagIbigNo.Enabled = true;
            else
                txtPagIbigNo.Enabled = false;
                txtPagIbigNo.Text = "";
        }

        private void cbTIN_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTIN.Checked)
                txtTINNo.Enabled = true;
            else
                txtTINNo.Enabled = false;
                txtTINNo.Text = "";
        }

        private void cbPhilHealth_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPhilHealth.Checked)
                txtPhilHealthNo.Enabled = true;
            else
                txtPhilHealthNo.Enabled = false;
                txtPhilHealthNo.Text = "";
        }

        private void clear()
        {
            _organicId = 0;

            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtLastName.Text = "";
            cmbCivilStatus.SelectedItem = "Single";
            txtReligion.Text = "";
            txtCitizenship.Text = "";
            dtpBirthdate.Value = DateTime.Now;
            txtBirthPlace.Text = "";
            txtContactNo1.Text = "";
            txtContactNo2.Text = "";
            txtEmailAdd.Text = "";
            txtAddressNo.Text = "";
            txtStreetNo.Text = "";
            txtSubdivision.Text = "";
            txtBarangay.Text = "";
            txtCity.Text = "";
            txtPostalCode.Text = "";
            txtSSSNo.Text.Trim();
            txtPagIbigNo.Text.Trim();
            txtTINNo.Text.Trim();
            txtPhilHealthNo.Text.Trim();
            txtPosition.Text = "";
            dtpDateOfApplication.Value = DateTime.Now;

            tcManagePersonnel.TabPages[0].Text = "New";
            gbDocments.Enabled = true;
            cbSSS.Checked = false;
            cbPagIbig.Checked = false;
            cbPhilHealth.Checked = false;
            cbTIN.Checked = false;
        }

        #endregion

        private void frmManagePersonnel_Load(object sender, EventArgs e)
        {
            clear();
        }

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgActive.Rows[e.RowIndex].Cells["Edit_Active"] == dgActive.CurrentCell)
                {
                    SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                    salesPersonnelBL._memberId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["MemberId_Active"].Value);

                    DataSet dsGetMemberDetails = OrganicPersonnelDAL.getOrganicPersonnelListById(salesPersonnelBL);

                    if (dsGetMemberDetails.Tables[0].Rows.Count > 0)
                    {
                        //Personal info
                        _organicId = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["MemberId"]);
                        txtFirstName.Text = dsGetMemberDetails.Tables[0].Rows[0]["FirstName"].ToString();
                        txtMiddleName.Text = dsGetMemberDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                        txtLastName.Text = dsGetMemberDetails.Tables[0].Rows[0]["LastName"].ToString();
                        cmbCivilStatus.Text = dsGetMemberDetails.Tables[0].Rows[0]["CivilStatus"].ToString();
                        txtReligion.Text = dsGetMemberDetails.Tables[0].Rows[0]["Religion"].ToString();
                        txtCitizenship.Text = dsGetMemberDetails.Tables[0].Rows[0]["Citizenship"].ToString();
                        dtpBirthdate.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["BirthDate"]);
                        txtBirthPlace.Text = dsGetMemberDetails.Tables[0].Rows[0]["BirthPlace"].ToString();
                        txtAddressNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressNo"].ToString();
                        txtStreetNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressStreet"].ToString();
                        txtSubdivision.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressVillageOrSubdivision"].ToString();
                        txtBarangay.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressBarangayOrDistrict"].ToString();
                        txtCity.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressTownOrCity"].ToString();
                        txtContactNo1.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                        txtContactNo2.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                        txtEmailAdd.Text = dsGetMemberDetails.Tables[0].Rows[0]["EmailAddress"].ToString();
                        txtPostalCode.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressPostalCode"].ToString();

                        //documents
                        txtPosition.Text = dsGetMemberDetails.Tables[0].Rows[0]["PositionDesired"].ToString();
                        dtpDateOfApplication.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["DateOfApplication"]);

                        if (_organicId > 0)
                        {
                            tcManagePersonnel.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManagePersonnel.TabPages[0].Text = "New";
                        }

                        if (Convert.ToBoolean(dsGetMemberDetails.Tables[0].Rows[0]["Gender"]) == true)
                        {
                            rbtnMale.Checked = true;
                            rbtnFemale.Checked = false;
                        }
                        else
                        {
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }

                        gbDocments.Enabled = false;
                        cbActive.Checked = true;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find personnel.");
                    }

                    tcManagePersonnel.SelectedIndex = 0;
                }
            }
        }

        private void dgInactive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgInactive.Rows[e.RowIndex].Cells["Edit_Inactive"] == dgInactive.CurrentCell)
                {
                    SalesPersonnelBL salesPersonnelBL = new SalesPersonnelBL();
                    salesPersonnelBL._memberId = Convert.ToInt32(dgInactive.Rows[e.RowIndex].Cells["MemberId_Inactive"].Value);

                    DataSet dsGetMemberDetails = OrganicPersonnelDAL.getOrganicPersonnelListById(salesPersonnelBL);

                    if (dsGetMemberDetails.Tables[0].Rows.Count > 0)
                    {
                        //Personal info
                        _organicId = Convert.ToInt32(dsGetMemberDetails.Tables[0].Rows[0]["MemberId"]);
                        txtFirstName.Text = dsGetMemberDetails.Tables[0].Rows[0]["FirstName"].ToString();
                        txtMiddleName.Text = dsGetMemberDetails.Tables[0].Rows[0]["MiddleName"].ToString();
                        txtLastName.Text = dsGetMemberDetails.Tables[0].Rows[0]["LastName"].ToString();
                        cmbCivilStatus.Text = dsGetMemberDetails.Tables[0].Rows[0]["CivilStatus"].ToString();
                        txtReligion.Text = dsGetMemberDetails.Tables[0].Rows[0]["Religion"].ToString();
                        txtCitizenship.Text = dsGetMemberDetails.Tables[0].Rows[0]["Citizenship"].ToString();
                        dtpBirthdate.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["BirthDate"]);
                        txtBirthPlace.Text = dsGetMemberDetails.Tables[0].Rows[0]["BirthPlace"].ToString();
                        txtAddressNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressNo"].ToString();
                        txtStreetNo.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressStreet"].ToString();
                        txtSubdivision.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressVillageOrSubdivision"].ToString();
                        txtBarangay.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressBarangayOrDistrict"].ToString();
                        txtCity.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressTownOrCity"].ToString();
                        txtContactNo1.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo1"].ToString();
                        txtContactNo2.Text = dsGetMemberDetails.Tables[0].Rows[0]["ContactNo2"].ToString();
                        txtEmailAdd.Text = dsGetMemberDetails.Tables[0].Rows[0]["EmailAddress"].ToString();
                        txtPostalCode.Text = dsGetMemberDetails.Tables[0].Rows[0]["AddressPostalCode"].ToString();

                        //documents
                        txtPosition.Text = dsGetMemberDetails.Tables[0].Rows[0]["PositionDesired"].ToString();
                        dtpDateOfApplication.Value = Convert.ToDateTime(dsGetMemberDetails.Tables[0].Rows[0]["DateOfApplication"]);

                        if (_organicId > 0)
                        {
                            tcManagePersonnel.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManagePersonnel.TabPages[0].Text = "New";
                        }

                        if (Convert.ToBoolean(dsGetMemberDetails.Tables[0].Rows[0]["Gender"]) == true)
                        {
                            rbtnMale.Checked = true;
                            rbtnFemale.Checked = false;
                        }
                        else
                        {
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }

                        gbDocments.Enabled = false;
                        cbActive.Checked = false;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find personnel.");
                    }

                    tcManagePersonnel.SelectedIndex = 0;
                }
            }
        }

        private void txtSSSNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtPagIbigNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtTINNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtPhilHealthNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }

        private void txtContactNo1_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }

        private void txtContactNo2_KeyPress(object sender, KeyPressEventArgs e)
        {
            phoneValuesChecker(sender, e);
        }
    }
}
