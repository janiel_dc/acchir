﻿namespace Accreditation_And_Hiring
{
    partial class frmNumberOfMSAGenerated
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AccreditationHiringDataSet = new Accreditation_And_Hiring.AccreditationHiringDataSet();
            this.DAL_GET_NumberOfGeneratedMSABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DAL_GET_NumberOfGeneratedMSATableAdapter = new Accreditation_And_Hiring.AccreditationHiringDataSetTableAdapters.DAL_GET_NumberOfGeneratedMSATableAdapter();
            this.dALGETNumberOfGeneratedMSABindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AccreditationHiringDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DAL_GET_NumberOfGeneratedMSABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETNumberOfGeneratedMSABindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsGeneratedMSA";
            reportDataSource1.Value = this.dALGETNumberOfGeneratedMSABindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Accreditation_And_Hiring.Reports.rptGeneratedMSA.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(401, 139);
            this.reportViewer1.TabIndex = 0;
            // 
            // AccreditationHiringDataSet
            // 
            this.AccreditationHiringDataSet.DataSetName = "AccreditationHiringDataSet";
            this.AccreditationHiringDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DAL_GET_NumberOfGeneratedMSABindingSource
            // 
            this.DAL_GET_NumberOfGeneratedMSABindingSource.DataMember = "DAL_GET_NumberOfGeneratedMSA";
            this.DAL_GET_NumberOfGeneratedMSABindingSource.DataSource = this.AccreditationHiringDataSet;
            // 
            // DAL_GET_NumberOfGeneratedMSATableAdapter
            // 
            this.DAL_GET_NumberOfGeneratedMSATableAdapter.ClearBeforeFill = true;
            // 
            // dALGETNumberOfGeneratedMSABindingSource
            // 
            this.dALGETNumberOfGeneratedMSABindingSource.DataMember = "DAL_GET_NumberOfGeneratedMSA";
            this.dALGETNumberOfGeneratedMSABindingSource.DataSource = this.AccreditationHiringDataSet;
            // 
            // frmNumberOfMSAGenerated
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 139);
            this.Controls.Add(this.reportViewer1);
            this.Name = "frmNumberOfMSAGenerated";
            this.Load += new System.EventHandler(this.frmNumberOfMSAGenerated_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AccreditationHiringDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DAL_GET_NumberOfGeneratedMSABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETNumberOfGeneratedMSABindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource DAL_GET_NumberOfGeneratedMSABindingSource;
        private AccreditationHiringDataSet AccreditationHiringDataSet;
        private AccreditationHiringDataSetTableAdapters.DAL_GET_NumberOfGeneratedMSATableAdapter DAL_GET_NumberOfGeneratedMSATableAdapter;
        private System.Windows.Forms.BindingSource dALGETNumberOfGeneratedMSABindingSource;
    }
}