﻿namespace Accreditation_And_Hiring
{
    partial class frmTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.label1 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dtpByMonth = new System.Windows.Forms.DateTimePicker();
            this.dataSetNUmberOfApplicantByMonth = new Accreditation_And_Hiring.DataSource.DataSetNUmberOfApplicantByMonth();
            this.dALGETNumberOfApplicantsByMonthBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dAL_GET_NumberOfApplicantsByMonthTableAdapter = new Accreditation_And_Hiring.DataSource.DataSetNUmberOfApplicantByMonthTableAdapters.DAL_GET_NumberOfApplicantsByMonthTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetNUmberOfApplicantByMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETNumberOfApplicantsByMonthBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(457, 64);
            this.label1.TabIndex = 7;
            this.label1.Text = "Number of applicants by month";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "dsGetNumberOfApplicantsByMonth";
            reportDataSource2.Value = this.dALGETNumberOfApplicantsByMonthBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Accreditation_And_Hiring.Reports.NumberOfApplicantsByMonth.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 64);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(457, 244);
            this.reportViewer1.TabIndex = 8;
            // 
            // dtpByMonth
            // 
            this.dtpByMonth.Location = new System.Drawing.Point(12, 38);
            this.dtpByMonth.Name = "dtpByMonth";
            this.dtpByMonth.Size = new System.Drawing.Size(203, 20);
            this.dtpByMonth.TabIndex = 9;
            this.dtpByMonth.ValueChanged += new System.EventHandler(this.dtpByMonth_ValueChanged);
            // 
            // dataSetNUmberOfApplicantByMonth
            // 
            this.dataSetNUmberOfApplicantByMonth.DataSetName = "DataSetNUmberOfApplicantByMonth";
            this.dataSetNUmberOfApplicantByMonth.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dALGETNumberOfApplicantsByMonthBindingSource
            // 
            this.dALGETNumberOfApplicantsByMonthBindingSource.DataMember = "DAL_GET_NumberOfApplicantsByMonth";
            this.dALGETNumberOfApplicantsByMonthBindingSource.DataSource = this.dataSetNUmberOfApplicantByMonth;
            // 
            // dAL_GET_NumberOfApplicantsByMonthTableAdapter
            // 
            this.dAL_GET_NumberOfApplicantsByMonthTableAdapter.ClearBeforeFill = true;
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 308);
            this.Controls.Add(this.dtpByMonth);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.label1);
            this.Name = "frmTest";
            this.Load += new System.EventHandler(this.frmTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetNUmberOfApplicantByMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETNumberOfApplicantsByMonthBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.DateTimePicker dtpByMonth;
        private System.Windows.Forms.BindingSource dALGETNumberOfApplicantsByMonthBindingSource;
        private DataSource.DataSetNUmberOfApplicantByMonth dataSetNUmberOfApplicantByMonth;
        private DataSource.DataSetNUmberOfApplicantByMonthTableAdapters.DAL_GET_NumberOfApplicantsByMonthTableAdapter dAL_GET_NumberOfApplicantsByMonthTableAdapter;
    }
}