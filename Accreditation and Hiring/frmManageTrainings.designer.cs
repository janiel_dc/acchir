﻿namespace Accreditation_and_Hiring_System
{
    partial class frmManageTrainings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.tcManageTrainings = new System.Windows.Forms.TabControl();
            this.tpNew = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTrainingTitleOrRemarks = new System.Windows.Forms.RichTextBox();
            this.cbInProgress = new System.Windows.Forms.CheckBox();
            this.dtpDateStart = new System.Windows.Forms.DateTimePicker();
            this.txtExamTotalNumber = new System.Windows.Forms.TextBox();
            this.txtVenue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFacilitator = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtOfficerInCharge = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpActive = new System.Windows.Forms.TabPage();
            this.dgActive = new System.Windows.Forms.DataGridView();
            this.TrainingId_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingNameOrRemarks_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDate_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDate_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OfficerIncharge_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Facilitator_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Venue_Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Active = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpInActive = new System.Windows.Forms.TabPage();
            this.dgInactive = new System.Windows.Forms.DataGridView();
            this.TrainingId_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainingNameOrRemarks_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDate_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDate_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OfficerInCharge_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Facilitator_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Venue_Inactive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_Inactive = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel2.SuspendLayout();
            this.tcManageTrainings.SuspendLayout();
            this.tpNew.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).BeginInit();
            this.tpInActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(787, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Manage Training";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(700, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 327);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(787, 28);
            this.panel2.TabIndex = 9;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(619, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tcManageTrainings
            // 
            this.tcManageTrainings.Controls.Add(this.tpNew);
            this.tcManageTrainings.Controls.Add(this.tpActive);
            this.tcManageTrainings.Controls.Add(this.tpInActive);
            this.tcManageTrainings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcManageTrainings.Location = new System.Drawing.Point(0, 33);
            this.tcManageTrainings.Name = "tcManageTrainings";
            this.tcManageTrainings.SelectedIndex = 0;
            this.tcManageTrainings.Size = new System.Drawing.Size(787, 294);
            this.tcManageTrainings.TabIndex = 10;
            this.tcManageTrainings.SelectedIndexChanged += new System.EventHandler(this.tcManageTrainings_SelectedIndexChanged);
            // 
            // tpNew
            // 
            this.tpNew.Controls.Add(this.groupBox1);
            this.tpNew.Location = new System.Drawing.Point(4, 22);
            this.tpNew.Name = "tpNew";
            this.tpNew.Padding = new System.Windows.Forms.Padding(3);
            this.tpNew.Size = new System.Drawing.Size(779, 268);
            this.tpNew.TabIndex = 0;
            this.tpNew.Text = "New";
            this.tpNew.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtTrainingTitleOrRemarks);
            this.groupBox1.Controls.Add(this.cbInProgress);
            this.groupBox1.Controls.Add(this.dtpDateStart);
            this.groupBox1.Controls.Add(this.txtExamTotalNumber);
            this.groupBox1.Controls.Add(this.txtVenue);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtFacilitator);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtOfficerInCharge);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(211, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 247);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Training Info";
            // 
            // txtTrainingTitleOrRemarks
            // 
            this.txtTrainingTitleOrRemarks.Location = new System.Drawing.Point(124, 39);
            this.txtTrainingTitleOrRemarks.MaxLength = 50;
            this.txtTrainingTitleOrRemarks.Name = "txtTrainingTitleOrRemarks";
            this.txtTrainingTitleOrRemarks.Size = new System.Drawing.Size(227, 41);
            this.txtTrainingTitleOrRemarks.TabIndex = 20;
            this.txtTrainingTitleOrRemarks.Text = "";
            // 
            // cbInProgress
            // 
            this.cbInProgress.AutoSize = true;
            this.cbInProgress.Checked = true;
            this.cbInProgress.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbInProgress.Location = new System.Drawing.Point(272, 217);
            this.cbInProgress.Name = "cbInProgress";
            this.cbInProgress.Size = new System.Drawing.Size(79, 17);
            this.cbInProgress.TabIndex = 19;
            this.cbInProgress.Text = "In-Progress";
            this.cbInProgress.UseVisualStyleBackColor = true;
            // 
            // dtpDateStart
            // 
            this.dtpDateStart.Location = new System.Drawing.Point(124, 165);
            this.dtpDateStart.Name = "dtpDateStart";
            this.dtpDateStart.Size = new System.Drawing.Size(227, 20);
            this.dtpDateStart.TabIndex = 18;
            // 
            // txtExamTotalNumber
            // 
            this.txtExamTotalNumber.Location = new System.Drawing.Point(124, 191);
            this.txtExamTotalNumber.MaxLength = 3;
            this.txtExamTotalNumber.Name = "txtExamTotalNumber";
            this.txtExamTotalNumber.Size = new System.Drawing.Size(227, 20);
            this.txtExamTotalNumber.TabIndex = 13;
            this.txtExamTotalNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExamTotalNumber_KeyPress);
            // 
            // txtVenue
            // 
            this.txtVenue.Location = new System.Drawing.Point(124, 138);
            this.txtVenue.MaxLength = 50;
            this.txtVenue.Name = "txtVenue";
            this.txtVenue.Size = new System.Drawing.Size(227, 20);
            this.txtVenue.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Exam Total Number:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Date Start:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Venue:";
            // 
            // txtFacilitator
            // 
            this.txtFacilitator.Location = new System.Drawing.Point(124, 112);
            this.txtFacilitator.MaxLength = 50;
            this.txtFacilitator.Name = "txtFacilitator";
            this.txtFacilitator.Size = new System.Drawing.Size(227, 20);
            this.txtFacilitator.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Facilitator:";
            // 
            // txtOfficerInCharge
            // 
            this.txtOfficerInCharge.Location = new System.Drawing.Point(124, 86);
            this.txtOfficerInCharge.MaxLength = 50;
            this.txtOfficerInCharge.Name = "txtOfficerInCharge";
            this.txtOfficerInCharge.Size = new System.Drawing.Size(227, 20);
            this.txtOfficerInCharge.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Officer In-Charge:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Training Title / Remarks:";
            // 
            // tpActive
            // 
            this.tpActive.Controls.Add(this.dgActive);
            this.tpActive.Location = new System.Drawing.Point(4, 22);
            this.tpActive.Name = "tpActive";
            this.tpActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpActive.Size = new System.Drawing.Size(779, 268);
            this.tpActive.TabIndex = 1;
            this.tpActive.Text = "In-Progress";
            this.tpActive.UseVisualStyleBackColor = true;
            // 
            // dgActive
            // 
            this.dgActive.AllowUserToAddRows = false;
            this.dgActive.AllowUserToDeleteRows = false;
            this.dgActive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgActive.BackgroundColor = System.Drawing.Color.White;
            this.dgActive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgActive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrainingId_Active,
            this.TrainingNameOrRemarks_Active,
            this.StartDate_Active,
            this.EndDate_Active,
            this.OfficerIncharge_Active,
            this.Facilitator_Active,
            this.Venue_Active,
            this.Edit_Active});
            this.dgActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgActive.Location = new System.Drawing.Point(3, 3);
            this.dgActive.Name = "dgActive";
            this.dgActive.ReadOnly = true;
            this.dgActive.RowHeadersVisible = false;
            this.dgActive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgActive.Size = new System.Drawing.Size(773, 262);
            this.dgActive.TabIndex = 0;
            this.dgActive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgActive_CellContentClick);
            // 
            // TrainingId_Active
            // 
            this.TrainingId_Active.DataPropertyName = "TrainingId";
            this.TrainingId_Active.HeaderText = "Training Id";
            this.TrainingId_Active.Name = "TrainingId_Active";
            this.TrainingId_Active.ReadOnly = true;
            this.TrainingId_Active.Visible = false;
            // 
            // TrainingNameOrRemarks_Active
            // 
            this.TrainingNameOrRemarks_Active.DataPropertyName = "TrainingNameOrRemarks";
            this.TrainingNameOrRemarks_Active.HeaderText = "Title / Remarks";
            this.TrainingNameOrRemarks_Active.Name = "TrainingNameOrRemarks_Active";
            this.TrainingNameOrRemarks_Active.ReadOnly = true;
            // 
            // StartDate_Active
            // 
            this.StartDate_Active.DataPropertyName = "TrainingStartDate";
            this.StartDate_Active.HeaderText = "StartDate";
            this.StartDate_Active.Name = "StartDate_Active";
            this.StartDate_Active.ReadOnly = true;
            // 
            // EndDate_Active
            // 
            this.EndDate_Active.DataPropertyName = "TrainingEndDate";
            this.EndDate_Active.HeaderText = "End Date";
            this.EndDate_Active.Name = "EndDate_Active";
            this.EndDate_Active.ReadOnly = true;
            // 
            // OfficerIncharge_Active
            // 
            this.OfficerIncharge_Active.DataPropertyName = "OfficerInCharge";
            this.OfficerIncharge_Active.HeaderText = "Officer In-Charge";
            this.OfficerIncharge_Active.Name = "OfficerIncharge_Active";
            this.OfficerIncharge_Active.ReadOnly = true;
            // 
            // Facilitator_Active
            // 
            this.Facilitator_Active.DataPropertyName = "Facilitator";
            this.Facilitator_Active.HeaderText = "Facilitator";
            this.Facilitator_Active.Name = "Facilitator_Active";
            this.Facilitator_Active.ReadOnly = true;
            // 
            // Venue_Active
            // 
            this.Venue_Active.DataPropertyName = "Venue";
            this.Venue_Active.HeaderText = "Venue";
            this.Venue_Active.Name = "Venue_Active";
            this.Venue_Active.ReadOnly = true;
            // 
            // Edit_Active
            // 
            this.Edit_Active.HeaderText = "";
            this.Edit_Active.Name = "Edit_Active";
            this.Edit_Active.ReadOnly = true;
            this.Edit_Active.Text = "Edit";
            this.Edit_Active.UseColumnTextForButtonValue = true;
            // 
            // tpInActive
            // 
            this.tpInActive.Controls.Add(this.dgInactive);
            this.tpInActive.Location = new System.Drawing.Point(4, 22);
            this.tpInActive.Name = "tpInActive";
            this.tpInActive.Padding = new System.Windows.Forms.Padding(3);
            this.tpInActive.Size = new System.Drawing.Size(779, 268);
            this.tpInActive.TabIndex = 2;
            this.tpInActive.Text = "Finished";
            this.tpInActive.UseVisualStyleBackColor = true;
            // 
            // dgInactive
            // 
            this.dgInactive.AllowUserToAddRows = false;
            this.dgInactive.AllowUserToDeleteRows = false;
            this.dgInactive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInactive.BackgroundColor = System.Drawing.Color.White;
            this.dgInactive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInactive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrainingId_Inactive,
            this.TrainingNameOrRemarks_Inactive,
            this.StartDate_Inactive,
            this.EndDate_Inactive,
            this.OfficerInCharge_Inactive,
            this.Facilitator_Inactive,
            this.Venue_Inactive,
            this.Edit_Inactive});
            this.dgInactive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInactive.Location = new System.Drawing.Point(3, 3);
            this.dgInactive.Name = "dgInactive";
            this.dgInactive.ReadOnly = true;
            this.dgInactive.RowHeadersVisible = false;
            this.dgInactive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInactive.Size = new System.Drawing.Size(773, 262);
            this.dgInactive.TabIndex = 1;
            this.dgInactive.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInactive_CellContentClick);
            // 
            // TrainingId_Inactive
            // 
            this.TrainingId_Inactive.DataPropertyName = "TrainingId";
            this.TrainingId_Inactive.HeaderText = "Training Id";
            this.TrainingId_Inactive.Name = "TrainingId_Inactive";
            this.TrainingId_Inactive.ReadOnly = true;
            this.TrainingId_Inactive.Visible = false;
            // 
            // TrainingNameOrRemarks_Inactive
            // 
            this.TrainingNameOrRemarks_Inactive.DataPropertyName = "TrainingNameOrRemarks";
            this.TrainingNameOrRemarks_Inactive.HeaderText = "Title / Remarks";
            this.TrainingNameOrRemarks_Inactive.Name = "TrainingNameOrRemarks_Inactive";
            this.TrainingNameOrRemarks_Inactive.ReadOnly = true;
            // 
            // StartDate_Inactive
            // 
            this.StartDate_Inactive.DataPropertyName = "TrainingStartDate";
            this.StartDate_Inactive.HeaderText = "StartDate";
            this.StartDate_Inactive.Name = "StartDate_Inactive";
            this.StartDate_Inactive.ReadOnly = true;
            // 
            // EndDate_Inactive
            // 
            this.EndDate_Inactive.DataPropertyName = "TrainingEndDate";
            this.EndDate_Inactive.HeaderText = "End Date";
            this.EndDate_Inactive.Name = "EndDate_Inactive";
            this.EndDate_Inactive.ReadOnly = true;
            // 
            // OfficerInCharge_Inactive
            // 
            this.OfficerInCharge_Inactive.DataPropertyName = "OfficerInCharge";
            this.OfficerInCharge_Inactive.HeaderText = "Officer In-Charge";
            this.OfficerInCharge_Inactive.Name = "OfficerInCharge_Inactive";
            this.OfficerInCharge_Inactive.ReadOnly = true;
            // 
            // Facilitator_Inactive
            // 
            this.Facilitator_Inactive.DataPropertyName = "Facilitator";
            this.Facilitator_Inactive.HeaderText = "Facilitator";
            this.Facilitator_Inactive.Name = "Facilitator_Inactive";
            this.Facilitator_Inactive.ReadOnly = true;
            // 
            // Venue_Inactive
            // 
            this.Venue_Inactive.DataPropertyName = "Venue";
            this.Venue_Inactive.HeaderText = "Venue";
            this.Venue_Inactive.Name = "Venue_Inactive";
            this.Venue_Inactive.ReadOnly = true;
            // 
            // Edit_Inactive
            // 
            this.Edit_Inactive.HeaderText = "";
            this.Edit_Inactive.Name = "Edit_Inactive";
            this.Edit_Inactive.ReadOnly = true;
            this.Edit_Inactive.Text = "Edit";
            this.Edit_Inactive.UseColumnTextForButtonValue = true;
            // 
            // frmManageTrainings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(787, 355);
            this.Controls.Add(this.tcManageTrainings);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Name = "frmManageTrainings";
            this.panel2.ResumeLayout(false);
            this.tcManageTrainings.ResumeLayout(false);
            this.tpNew.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgActive)).EndInit();
            this.tpInActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInactive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tcManageTrainings;
        private System.Windows.Forms.TabPage tpNew;
        private System.Windows.Forms.TabPage tpActive;
        private System.Windows.Forms.TabPage tpInActive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpDateStart;
        private System.Windows.Forms.TextBox txtVenue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFacilitator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtOfficerInCharge;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox cbInProgress;
        private System.Windows.Forms.DataGridView dgActive;
        private System.Windows.Forms.RichTextBox txtTrainingTitleOrRemarks;
        private System.Windows.Forms.DataGridView dgInactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingId_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingNameOrRemarks_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartDate_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndDate_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn OfficerIncharge_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Facilitator_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Venue_Active;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingId_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainingNameOrRemarks_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartDate_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndDate_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn OfficerInCharge_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Facilitator_Inactive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Venue_Inactive;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_Inactive;
        private System.Windows.Forms.TextBox txtExamTotalNumber;
        private System.Windows.Forms.Label label6;
    }
}