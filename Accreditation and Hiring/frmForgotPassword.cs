﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmForgotPassword : Form
    {
        private string _userName = "", _answer = "";

        public frmForgotPassword()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtAnswer.Text.ToLower() == _answer.ToLower())
            {
                frmChangePassword frmChangePassword = new frmChangePassword();
                frmChangePassword.ShowDialog();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Invalid answer,");
            }
        }

        private void frmForgotPassword_Load(object sender, EventArgs e)
        {
            string userName = frmLogin.GetInstance()._cUsername;
            string answer = frmLogin.GetInstance()._cAnswer;
            int userId = frmLogin.GetInstance()._cUserID;
            DataSet getInformation = UserDAL.getSecretQuestion(userName);

            _answer = answer;
            _userName = userName;
            lblQuestion.Text = frmLogin.GetInstance()._cQuestion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
