﻿namespace Inventory_Management
{
    partial class frmInventoryProductPriceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.dALGETGetProductPriceListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetProductReport = new Inventory_Management.DataSource.DataSetProductReport();
            this.rptViewerProductPriceList = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dAL_GET_GetProductPriceListTableAdapter = new Inventory_Management.DataSource.DataSetProductReportTableAdapters.DAL_GET_GetProductPriceListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dALGETGetProductPriceListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProductReport)).BeginInit();
            this.SuspendLayout();
            // 
            // dALGETGetProductPriceListBindingSource
            // 
            this.dALGETGetProductPriceListBindingSource.DataMember = "DAL_GET_GetProductPriceList";
            this.dALGETGetProductPriceListBindingSource.DataSource = this.dataSetProductReport;
            // 
            // dataSetProductReport
            // 
            this.dataSetProductReport.DataSetName = "DataSetProductReport";
            this.dataSetProductReport.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rptViewerProductPriceList
            // 
            this.rptViewerProductPriceList.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsProductPrice";
            reportDataSource1.Value = this.dALGETGetProductPriceListBindingSource;
            this.rptViewerProductPriceList.LocalReport.DataSources.Add(reportDataSource1);
            this.rptViewerProductPriceList.LocalReport.ReportEmbeddedResource = "Inventory_Management.Reports.rptInventoryProductPriceListReport.rdlc";
            this.rptViewerProductPriceList.Location = new System.Drawing.Point(0, 0);
            this.rptViewerProductPriceList.Name = "rptViewerProductPriceList";
            this.rptViewerProductPriceList.Size = new System.Drawing.Size(745, 367);
            this.rptViewerProductPriceList.TabIndex = 0;
            // 
            // dAL_GET_GetProductPriceListTableAdapter
            // 
            this.dAL_GET_GetProductPriceListTableAdapter.ClearBeforeFill = true;
            // 
            // frmInventoryProductPriceList
            // 
            this.ClientSize = new System.Drawing.Size(745, 367);
            this.Controls.Add(this.rptViewerProductPriceList);
            this.Name = "frmInventoryProductPriceList";
            this.Text = "Product Price List";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInventoryProductPriceList_FormClosing);
            this.Load += new System.EventHandler(this.frmInventoryProductPriceList_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.dALGETGetProductPriceListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProductReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bIMSDALGETGetInventoryProductPriceListBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewerProductPriceList;
        private DataSource.DataSetProductReport dataSetProductReport;
        private System.Windows.Forms.BindingSource dALGETGetProductPriceListBindingSource;
        private DataSource.DataSetProductReportTableAdapters.DAL_GET_GetProductPriceListTableAdapter dAL_GET_GetProductPriceListTableAdapter;
    }
}