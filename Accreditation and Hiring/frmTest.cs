﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accreditation_And_Hiring
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();
        }

        private void frmTest_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void loadReport(DateTime dateSelected)
        {
            try
            {
                // TODO: This line of code loads data into the 'dataSetSalesOrderDailyReport.DAL_GET_GetSalesOrderDailyReport' table. You can move, or remove it, as needed.
                this.dAL_GET_NumberOfApplicantsByMonthTableAdapter.Fill(this.dataSetNUmberOfApplicantByMonth.DAL_GET_NumberOfApplicantsByMonth, dateSelected);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured, please contact your Administrator.");
            }

            this.reportViewer1.RefreshReport();
        }

        private void dtpByMonth_ValueChanged(object sender, EventArgs e)
        {
            loadReport(dtpByMonth.Value);
        }
    }
}
