﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessObjects;
using DataAccess;

namespace Accreditation_and_Hiring_System
{
    public partial class frmManageTrainings : Form
    {
        private int _trainingId = 0;

        public frmManageTrainings()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tcManageTrainings.SelectedIndex != 2 && _trainingId > 0)
            {
                if (txtTrainingTitleOrRemarks.Text != "")
                {
                    updateTraining();

                    if (cbInProgress.Checked)
                        tcManageTrainings.SelectedIndex = 1;
                    else if (!cbInProgress.Checked)
                        tcManageTrainings.SelectedIndex = 2;
                    else
                        tcManageTrainings.SelectedIndex = 0;

                    clear();
                }
                else
                {
                    MessageBox.Show("Please compete information");
                }
            }
            else if (tcManageTrainings.SelectedIndex != 2 && _trainingId == 0)
            {
                if (txtTrainingTitleOrRemarks.Text != "")
                {
                    insertNewTraining();

                    if (cbInProgress.Checked)
                        tcManageTrainings.SelectedIndex = 1;
                    else if (!cbInProgress.Checked)
                        tcManageTrainings.SelectedIndex = 2;
                    else
                        tcManageTrainings.SelectedIndex = 0;

                    clear();
                }
                else
                {
                    MessageBox.Show("Please compete information");
                }
            }
            else if (tcManageTrainings.SelectedIndex == 2)
            {
                deleteTraining();
            }
        }

        private void deleteTraining()
        {
            TrainingBL trainingBL = new TrainingBL();
            frmLogin login = frmLogin.GetInstance();

            string ans = MessageBox.Show("Are you sure you want to delete this training?", "Delete Training", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString();

            if (ans.Equals("Yes"))
            {
                try
                {
                    int rowIndex = dgInactive.CurrentCell.RowIndex;
                    trainingBL._trainingId = Convert.ToInt32(dgInactive.Rows[rowIndex].Cells["TrainingId_Inactive"].Value);
                    trainingBL._lastModifiedUserId = login._loggedInUser._userId;
                    TrainingDAL.deleteTraining(trainingBL);
                    MessageBox.Show("Training deleted successfully");

                    DataSet dsTrainingList = TrainingDAL.getTrainingList();
                    dgInactive.AutoGenerateColumns = false;
                    dgInactive.DataSource = dsTrainingList.Tables[1];

                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured, please contact your Administrator." + ex.ToString());
                }
            }
        }

        private void updateTraining()
        {
            try
            {
                TrainingBL trainingBL = new TrainingBL();
                frmLogin login = frmLogin.GetInstance();

                trainingBL._trainingId = _trainingId;
                trainingBL._trainingNameOrRemarks = txtTrainingTitleOrRemarks.Text.Trim();
                trainingBL._trainingStartDate = dtpDateStart.Value.ToString();
                trainingBL._officerInCharge = txtOfficerInCharge.Text.Trim();
                trainingBL._facilitator = txtFacilitator.Text.Trim();
                trainingBL._venue = txtVenue.Text.Trim();
                trainingBL._lastModifiedUserId = login._loggedInUser._userId;
                trainingBL._trainingExamTotalNumber = Convert.ToInt32(txtExamTotalNumber.Text.Trim());

                if (cbInProgress.Checked)
                    trainingBL._isActive = true;
                else
                    trainingBL._isActive = false;

                TrainingDAL.updateTrainingDetails(trainingBL);
                MessageBox.Show("Training successfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator" + ex.ToString());
            }

        }

        private void insertNewTraining()
        {
            try
            {
                TrainingBL trainingBL = new TrainingBL();
                frmLogin login = frmLogin.GetInstance();

                trainingBL._trainingNameOrRemarks = txtTrainingTitleOrRemarks.Text.Trim();
                trainingBL._trainingStartDate = dtpDateStart.Value.ToString();
                trainingBL._officerInCharge = txtOfficerInCharge.Text.Trim();
                trainingBL._facilitator = txtFacilitator.Text.Trim();
                trainingBL._venue = txtVenue.Text.Trim();
                trainingBL._createdUserId = login._loggedInUser._userId;
                trainingBL._lastModifiedUserId = login._loggedInUser._userId;
                trainingBL._trainingExamTotalNumber = Convert.ToInt32(txtExamTotalNumber.Text.Trim());

                if (cbInProgress.Checked)
                    trainingBL._isActive = true;
                else
                    trainingBL._isActive = false;

                TrainingDAL.insertTraining(trainingBL);
                MessageBox.Show("New training successfully added.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured contact your administrator" + ex.ToString());
            }

        }

        private void tcManageTrainings_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcManageTrainings.SelectedIndex == 1)
            {
                DataSet dsTrainingList = TrainingDAL.getTrainingList();
                dgActive.AutoGenerateColumns = false;
                dgActive.DataSource = dsTrainingList.Tables[0];

                btnSave.Visible = false;
                clear();
            }
            else if (tcManageTrainings.SelectedIndex == 2)
            {
                DataSet dsTrainingList = TrainingDAL.getTrainingList();
                dgInactive.AutoGenerateColumns = false;
                dgInactive.DataSource = dsTrainingList.Tables[1];

                btnSave.Visible = true;
                btnSave.Text = "Delete";
                btnSave.BackColor = Color.Red;

                clear();
            }
            else if (tcManageTrainings.SelectedIndex == 0)
            {
                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnSave.BackColor = Color.Green;
            }
        }

        #region HELPERS

        private static void intValuesChecker(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void clear()
        {
            tcManageTrainings.TabPages[0].Text = "New";

            _trainingId = 0;
            txtTrainingTitleOrRemarks.Text = "";
            txtOfficerInCharge.Text = "";
            txtFacilitator.Text = "";
            txtVenue.Text = "";
            dtpDateStart.Value = DateTime.Now;

            cbInProgress.Checked = true;

        }

        #endregion

        private void dgActive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgActive.Rows[e.RowIndex].Cells["Edit_Active"] == dgActive.CurrentCell)
                {
                    TrainingBL trainingBL = new TrainingBL();
                    trainingBL._trainingId = Convert.ToInt32(dgActive.Rows[e.RowIndex].Cells["TrainingId_Active"].Value);

                    DataSet dsGetTraingDetails = TrainingDAL.getTrainingDetailsById(trainingBL);

                    if (dsGetTraingDetails.Tables[0].Rows.Count > 0)
                    {
                        //Training info
                        _trainingId = Convert.ToInt32(dsGetTraingDetails.Tables[0].Rows[0]["TrainingId"]);
                        txtTrainingTitleOrRemarks.Text = dsGetTraingDetails.Tables[0].Rows[0]["TrainingNameOrRemarks"].ToString();
                        txtOfficerInCharge.Text = dsGetTraingDetails.Tables[0].Rows[0]["OfficerInCharge"].ToString();
                        txtFacilitator.Text = dsGetTraingDetails.Tables[0].Rows[0]["Facilitator"].ToString();
                        txtVenue.Text = dsGetTraingDetails.Tables[0].Rows[0]["Venue"].ToString();
                        dtpDateStart.Value = Convert.ToDateTime(dsGetTraingDetails.Tables[0].Rows[0]["TrainingStartDate"]);
                        txtExamTotalNumber.Text = dsGetTraingDetails.Tables[0].Rows[0]["TrainingExamTotalNumber"].ToString();

                        if (_trainingId > 0)
                        {
                            tcManageTrainings.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManageTrainings.TabPages[0].Text = "New";
                        }
                        cbInProgress.Checked = true;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find training.");
                    }

                    tcManageTrainings.SelectedIndex = 0;
                }
            }
        }

        private void dgInactive_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (dgInactive.Rows[e.RowIndex].Cells["Edit_Inactive"] == dgInactive.CurrentCell)
                {
                    TrainingBL trainingBL = new TrainingBL();
                    trainingBL._trainingId = Convert.ToInt32(dgInactive.Rows[e.RowIndex].Cells["TrainingId_Inactive"].Value);

                    DataSet dsGetTraingDetails = TrainingDAL.getTrainingDetailsById(trainingBL);

                    if (dsGetTraingDetails.Tables[0].Rows.Count > 0)
                    {
                        //Training info
                        _trainingId = Convert.ToInt32(dsGetTraingDetails.Tables[0].Rows[0]["TrainingId"]);
                        txtTrainingTitleOrRemarks.Text = dsGetTraingDetails.Tables[0].Rows[0]["TrainingNameOrRemarks"].ToString();
                        txtOfficerInCharge.Text = dsGetTraingDetails.Tables[0].Rows[0]["OfficerInCharge"].ToString();
                        txtFacilitator.Text = dsGetTraingDetails.Tables[0].Rows[0]["Facilitator"].ToString();
                        txtVenue.Text = dsGetTraingDetails.Tables[0].Rows[0]["Venue"].ToString();
                        dtpDateStart.Value = Convert.ToDateTime(dsGetTraingDetails.Tables[0].Rows[0]["TrainingStartDate"]);
                        txtExamTotalNumber.Text = dsGetTraingDetails.Tables[0].Rows[0]["TrainingExamTotalNumber"].ToString();

                        if (_trainingId > 0)
                        {
                            tcManageTrainings.TabPages[0].Text = "Update";
                        }
                        else
                        {
                            tcManageTrainings.TabPages[0].Text = "New";
                        }
                        cbInProgress.Checked = false;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find training.");
                    }

                    tcManageTrainings.SelectedIndex = 0;
                }
            }
        }

        private void txtExamTotalNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            intValuesChecker(sender, e);
        }
    }
}
